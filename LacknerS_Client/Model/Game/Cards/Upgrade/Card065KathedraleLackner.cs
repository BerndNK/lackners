﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card065KathedraleLackner : UpgradeCard
    {
        public Card065KathedraleLackner(int id, string owner) : base(65, id, owner)
        {
            Rarity = -1;
            Coolness = 0;
        }
    }
}
