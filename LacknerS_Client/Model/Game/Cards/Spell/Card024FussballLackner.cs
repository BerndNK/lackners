﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card024FussballLackner : SpellCard
    {
        public Card024FussballLackner(int id, string owner) : base(24, id, owner)
        {
        }
        

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {

            if (activeGame.UserPlayer.Name == forPlayer.Name)
            {
              //  activeGame.AutoPlayNextCards = 1;
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }
            GameLogger.GetInstance().Log($"{Name} wechselte einen Spieler ein und zieht eine grüne Karte.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
