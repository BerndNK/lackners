﻿using System;
using System.ComponentModel;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Host;

namespace LacknerS_Client.Model {
    public class DataService : IDataService {
        public void GetData(Action<DataItem, Exception> callback) {
            // Use this to connect to the actual data service

            var item = new DataItem("Welcome to MVVM Light");
            callback(item, null);
        }

        public void GetRegisteredClients(Action<BindingList<RegisteredClient>, Exception> callback) {
            callback(new BindingList<RegisteredClient>(), null);
        }

        public void GetOpponents(Action<BindingList<Player>, Exception> callback) {
            callback(new BindingList<Player>(), null);
        }

        public void GetPlayerCards(Action<BindingList<GameCard>, Exception> callback) {
            callback(new BindingList<GameCard>(), null);
        }
    }
}