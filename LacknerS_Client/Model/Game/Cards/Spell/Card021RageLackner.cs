﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell {
    public class Card021RageLackner : SpellCard{
        public Card021RageLackner(int id, string owner) : base(21, id, owner)
        {
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.OwnedCard, CardType.AllPlayable, OwnerType.Enemy) };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var cardId = selectionTargets.First().SelectedCard;
            var card = GetCardById(cardId);

            var owner = GetOwner(card, activeGame);
            owner.Cards.Remove(card);
            activeGame.Graveyard.Add(card);

            GameLogger.GetInstance().Log($"{card.Name} wurde Ziel von {Name}.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
