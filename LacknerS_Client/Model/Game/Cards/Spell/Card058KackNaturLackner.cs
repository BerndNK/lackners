﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card058KackNaturLackner : SpellCard
    {

        public Card058KackNaturLackner(int id, string owner) : base(58, id, owner) {
            Rarity = 2;
            Coolness = -1;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.Enemy) };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var upgradeCard = GetCardById(selectionTargets.First().SelectedCard) as UpgradeCard;
            if (upgradeCard == null) throw new Exception("Id of card could not be found, or parsing failed.");

            var rareLackner = RareLacknerCard.GetContainingRareLackner(upgradeCard);
            rareLackner.Upgrades.Remove(upgradeCard);
            forPlayer.PlayedCards.Remove(upgradeCard);
            activeGame.Graveyard.Add(upgradeCard);


            GameLogger.GetInstance().Log(Name + " entfernte " + upgradeCard.Name);
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
