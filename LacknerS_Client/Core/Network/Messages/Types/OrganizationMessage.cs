﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Views;

namespace LacknerS_Client.Core.Network.Messages.Types {
    [Serializable]
    public class OrganizationMessage : Message {

        #region Fields
        private OrganizationType _type;
        private string _user;

        #endregion

        #region Properties

        public OrganizationType Type {
            get { return _type; }
            set {
                _type = value;
                CallPropertyChanged(nameof(Type));
            }
        }

        public string User {
            get { return _user; }
            set {
                _user = value; 
                CallPropertyChanged(nameof(User));
            }
        }

        #endregion

        public OrganizationMessage(OrganizationType type, string user) {
            if(user == null) user = ConnectionPanel.Username;
            User = user;
            Type = type;
        }
    }
}
