﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card028LkwFahrerLackner : UpgradeCard
    {
        public Card028LkwFahrerLackner(int id, string owner) : base(28, id, owner)
        {
            Rarity = 2;
            Coolness = 0;
        }
    }
}
