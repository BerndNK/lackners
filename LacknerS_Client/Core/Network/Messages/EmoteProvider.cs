﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Media;
using LacknerS_Client.Core.Logging;

namespace LacknerS_Client.Core.Network.Messages {
    public static class EmoteProvider {

        public static ImageSource GetImage(string name) {
            string path = null;
            try {
                var files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\Resources\Emotes");
                foreach (var file in from file in files
                                     let fileName = file.Split('\\').Last().Split('.').First()
                                     where fileName == name
                                     select file) {
                    path = file;
                    break;
                }
            } catch (Exception) {
                DebugLogger.GetInstance().LogError("Failed to read Emote Directory");
                throw;
            }
            if (path != null) {
                return GetImageSource(path);
            }
            DebugLogger.GetInstance().LogError("Given name is not an emote. Name: " + name);
            return null;
        }

        private static ImageSource GetImageSource(string path) {
            var baseImage = new ImageSourceConverter().ConvertFromString(path) as ImageSource;

            // if the filename has a BW at the end of it, its a black/white picture. So we color black pixels white
            var fileName = path.Split('\\').Last().Split('.').First();
            throw new NotImplementedException();
            //if (fileName.IndexOf("BW", StringComparison.Ordinal) == fileName.Length - 2 && App.CurrentTheme == "BaseDark") {
            //    return ColorManipulator.ToWhite(baseImage);
            //} else return baseImage;
        }

        public static bool IsEmote(string name) {
            try {
                var files = Directory.GetFiles(Directory.GetCurrentDirectory() + @"\Resources\Emotes");
                if (files.Select(file => file.Split('\\').Last().Split('.').First()).Any(fileName => fileName == name)) {
                    return true;
                }
            } catch (Exception) {
                DebugLogger.GetInstance().LogError("Failed to read Emote Directory");
                throw;
            }
            return false;
        }
    }
}
