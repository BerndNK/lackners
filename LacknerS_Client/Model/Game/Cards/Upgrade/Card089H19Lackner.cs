﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card089H19Lackner : UpgradeCard
    {
        public Card089H19Lackner(int id, string owner) : base(89, id, owner)
        {
            Rarity = 0;
            Coolness = -1;
        }
    }
}
