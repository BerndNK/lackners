﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card084MisstrauischLackner : RareLacknerCard
    {
        public Card084MisstrauischLackner(int id, string owner) : base(84, id, owner)
        {
            Rarity = 4;
            Coolness = 4;
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            foreach (var player in activeGame.AllPlayer)
            {
                player.CanRemoveUpgrades = false;
            }
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            foreach (var player in activeGame.AllPlayer)
            {
                player.CanRemoveUpgrades = true;
            }
            GameLogger.GetInstance().Log($"Misstrauisch - Lackner zerstört! Es herrscht wieder Vertrauen und es können Upgrades entfernt werden.");
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}