﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace LacknerS_Client.Core.Logging {
    /// <summary>
    /// Describes a logger, which can log messages
    /// </summary>
    public interface ILogger {
        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logType">Type of the log message.</param>
        void Log(string message, LogType logType);

        /// <summary>
        /// Gets the logged messages.
        /// </summary>
        /// <returns>The logged messages</returns>
        BindingList<LogMessage> GetLogs();

        /// <summary>
        /// Occurs when a new [message is logged].
        /// </summary>
        event EventHandler<MessageLoggedEventArgs> MessageLogged;
    }
}
