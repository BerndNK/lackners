﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    [Serializable]
    public class Card000SacrificeCard : SpellCard
    {
        private static Card000SacrificeCard _instance;
        public const int Id = -4;

        public static Card000SacrificeCard GetInstance()
        {
            if (_instance != null) return _instance;
            return _instance = new Card000SacrificeCard();
        }

        private Card000SacrificeCard() : base(Id, Id, null)
        {
            NeededPoints = 0;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.OwnedCard, CardType.AllPlayable, OwnerType.Player) };
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var card = GetCardById(selectionTargets.First().SelectedCard);
            var owner = GetOwner(card, activeGame);
            owner.Cards.Remove(card);
            activeGame.Graveyard.Add(card);


            forPlayer.PlayPoints = 0;
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }

            GameLogger.GetInstance().Log($"{forPlayer.Name} hat {card.Name} geopfert um eine grüne Karte zu ziehen.");
        }
    }
}
