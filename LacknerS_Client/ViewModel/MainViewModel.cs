﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Documents;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Model;

namespace LacknerS_Client.ViewModel {
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase {
        private readonly IDataService _dataService;

        /// <summary>
        /// The <see cref="WelcomeTitle" /> property's name.
        /// </summary>
        public const string WelcomeTitlePropertyName = "WelcomeTitle";

        private string _welcomeTitle = string.Empty;

        /// <summary>
        /// Gets the WelcomeTitle property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string WelcomeTitle {
            get {
                return _welcomeTitle;
            }
            set {
                Set(ref _welcomeTitle, value);
            }
        }


        public ICommand SendCommand { get; private set; }
        public ICommand ConnectCommand { get; private set; }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IDataService dataService) {
            _dataService = dataService;
            SendCommand = new RelayCommand(Send);
            ConnectCommand = new RelayCommand(Connect);

            DebugLogger.GetInstance().LogInfo("Started.");
            _dataService.GetData(
                (item, error) => {
                    if (error != null) {
                        // Report error here
                        return;
                    }

                    WelcomeTitle = item.Title;
                });
        }

        private void Connect() {
            throw new NotImplementedException();
        }

        private void Send() {
            DebugLogger.GetInstance().LogInfo("test");
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}