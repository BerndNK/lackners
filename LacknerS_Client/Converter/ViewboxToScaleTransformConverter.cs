﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace LacknerS_Client.Converter {
    public class ViewboxToScaleTransformConverter : IValueConverter{
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var child = VisualTreeHelper.GetChild(value as Viewbox, 0) as ContainerVisual;
            var scale = child.Transform as ScaleTransform;
            return new ScaleTransform(1.0/scale.ScaleX, 1.0/scale.ScaleY);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
