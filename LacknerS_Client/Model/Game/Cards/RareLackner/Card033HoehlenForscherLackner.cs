﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card033HoehlenForscherLackner : RareLacknerCard
    {
        public Card033HoehlenForscherLackner(int id, string owner) : base(33, id, owner)
        {
            Rarity = 4;
            Coolness = 3;
        }
        

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            if (activeGame.UserPlayer == forPlayer)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);
            }
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
