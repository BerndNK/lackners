﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card099AlkoholLackner : UpgradeCard
    {
        public Card099AlkoholLackner(int id, string owner) : base(99, id, owner)
        {
            Rarity = 3;
            Coolness = -4;
        }
    }
}
