#undef UNICODE
#include <iostream>
#include <string>

#ifdef _WIN32
#define _WIN32_WINNT 0x0500
#include <winsock2.h>
#include <windows.h>

#include "SocketHandler.h"

HWND WINAPI GetConsoleWindow(void);
#include "SocketHandlerWindows.h"
#endif

#ifdef __unix__
#include "SocketHandlerUnix.h"
#endif

bool forceQuit = false;

void ConsoleReader() {
	while (1) {
		std::string input;
		std::cin >> input;
		if (input == "exit") {
			forceQuit = true;
#ifdef _WIN32
			WSACleanup();
#endif
			return;
		}
	}
}

int main(int argc, char * argv[]) {
	network::SocketHandler* socketHandler = nullptr;
	if (argc % 2 != 1) {
		std::cout << "Usage: -p Port -h [0|1] Http mode on or off" << std::endl;
		return 1;
	}

	std::string port = DEFAULT_PORT;
	auto httpMode = false;

	// parse arguments
	if (argc != 1) {
		for (auto i = 1; i < argc; i += 2) {
			if (std::string(argv[i]) == "-p") {
				port = argv[i + 1];
			}
			else if (std::string(argv[i]) == "-h") {
				httpMode = std::string(argv[i + 1]) == "1";
			}
		}
	}

	std::cout << "Port: " << port << " Http mode: " << httpMode << std::endl;

#ifdef _WIN32
	// make cmd always on top
	auto hwndMyWnd = GetConsoleWindow();

	::SetWindowPos(hwndMyWnd, HWND_TOPMOST, 400, 0, 0, 0, SWP_DRAWFRAME | SWP_NOMOVE | SWP_NOSIZE |  SWP_SHOWWINDOW);
	::ShowWindow(hwndMyWnd, SW_MINIMIZE);

	socketHandler = new network::SocketHandlerWindows(port);
#endif

#ifdef __unix__
	socketHandler = new network::SocketHandlerUnix(port);
#endif

	if (socketHandler == nullptr) {
		std::cout << "There's no socket implementation for your sick operating system" << std::endl;
		return 1;
	}


	socketHandler->HttpMode = httpMode;
	if (socketHandler->Initialize() != 0) {
		std::cout << "Failed to intialize socket" << std::endl;
		return 1;
	}

	std::thread readThread(ConsoleReader);
	std::cout << "Server running. Type 'exit' to stop" << std::endl;
	while (!forceQuit) {
		while (!socketHandler->WaitingForClient && !forceQuit) {
			socketHandler->CreateListener();
		}
	}

	readThread.join();
	std::cout << "Main Thread end" << std::endl;
	return 0;
}
