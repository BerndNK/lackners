﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card070SightseeingLackner : UpgradeCard
    {
        public Card070SightseeingLackner(int id, string owner) : base(70, id, owner)
        {
            Rarity = -1;
            Coolness = 0;
        }
    }
}
