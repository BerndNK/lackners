﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card098SkiFahrenLackner : UpgradeCard
    {
        public Card098SkiFahrenLackner(int id, string owner) : base(98, id, owner)
        {
            Rarity = 0;
            Coolness = 3;
        }
    }
}
