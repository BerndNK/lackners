﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card051WhatsAppLackner : UpgradeCard
    {
        public Card051WhatsAppLackner(int id, string owner) : base(51, id, owner)
        {
            Rarity = 2;
            Coolness = 0;
        }
    }
}
