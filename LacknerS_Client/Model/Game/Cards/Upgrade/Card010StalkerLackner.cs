﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card010StalkerLackner : UpgradeCard{
        public Card010StalkerLackner(int id, string owner) : base(10, id, owner)
        {
            Rarity = 0;
            Coolness = -2;
            CanBeRegularlyRemoved = false;
        }
    }
}
