﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card057FernsehTeamLackner : UpgradeCard
    {
        public Card057FernsehTeamLackner(int id, string owner) : base(57, id, owner)
        {
            Rarity = 0;
            Coolness = 2;
        }
    }
}
