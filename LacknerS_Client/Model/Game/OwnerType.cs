﻿using System;

namespace LacknerS_Client.Model.Game
{
    [Flags]
    public enum OwnerType
    {
        Enemy = 1,
        Player = 2,
        None = 4,
        All = Enemy | Player
    }
}