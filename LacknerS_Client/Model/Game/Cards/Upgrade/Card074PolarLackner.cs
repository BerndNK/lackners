﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card074PolarLackner : UpgradeCard
    {
        public Card074PolarLackner(int id, string owner) : base(74, id, owner)
        {
            Rarity = -1;
            Coolness = 3;
        }
    }
}
