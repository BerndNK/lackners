#include "SocketHandler.h"
#include <memory.h>
#include <iostream>

// public:
void network::SocketHandler::WaitForThreads() {
	if (!m_isInitalized) return;

	for (auto client : m_clientPool) {
		client->Thread->join();
	}
}

void network::SocketHandler::CreateListener() {
	m_waitingForClient = true;
	auto newClient = new Client(static_cast<std::thread*>(nullptr), ++m_clientCounter);

	// create normal listener or http listener
	if (HttpMode) newClient->Thread = new std::thread(&SocketHandler::ListenerHttp, this, &m_waitingForClient, newClient);
	else newClient->Thread = new std::thread(&SocketHandler::Listener, this, &m_waitingForClient, newClient);

	m_clientMutex.lock();
	m_clientPool.push_back(newClient);
	m_clientMutex.unlock();
}

void network::SocketHandler::QueueMessage(Client* sender, char msg[DEFAULT_BUFLEN], int length) {
	m_clientMutex.lock();
	for (auto client : m_clientPool) {
		if (!client->IsConnected) continue;
		client->ListMutex.lock();
		auto copy = new char[DEFAULT_BUFLEN];
		memcpy(copy, msg, length);
		client->ToPushList.push_back(Message(length, copy));
#ifdef PRINT_INFO
		std::cout << "client " << client->No << " has queued " << client->ToPushList.size() << std::endl;
#endif
		client->ListMutex.unlock();
	}
	m_clientMutex.unlock();
	/*m_clientMutex.lock();
	for (auto client : m_clientPool) {
		if (!client->IsConnected) continue;
		//if (client == sender) continue;
		client->ListMutex.lock();

		std::string str = "<!DOCTYPE html> <html lang='de'> 	<head> 		<title>Holla hop</title> 		<meta charset='UTF-8'> 		<meta name='keywords' content=''> 		<meta name='description' content=''> 		<meta name='robots' content='index, follow'> 	</head> <hr1>shit works</h1> </html> ";
		auto copy = new char[str.size()];
		memcpy(copy, str.c_str(), str.size());
		client->ToPushList.push_back(Message(str.size(), copy));
		client->ListMutex.unlock();
	}
	m_clientMutex.unlock();*/
}

void network::SocketHandler::RemoveClient(Client* client) {
	m_clientMutex.lock();
	auto j = m_clientPool.size();
	for (decltype(j) i = 0; i < j; i++) {
		if (m_clientPool[i] == client) {
			m_clientPool.erase(m_clientPool.begin() + i, m_clientPool.begin() + i + 1);
			m_clientMutex.unlock();
			return;
		}
	}
	m_clientMutex.unlock();
}
