﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Core.Network.Messages {
    [Serializable]
    public class TradeMessage : Message{
        public TradeMessage(TradeMessageType type, int cardId)
        {
            Type = type;
            CardId = cardId;
        }

        public int CardId { get; set; }

        public TradeMessageType Type { get; set; }
    }
}
