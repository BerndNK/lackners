﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card094TopSpeedLackner : SpellCard
    {
        public Card094TopSpeedLackner(int id, string owner) : base(94, id, owner)
        {
            Power = SpellCardPower.High;
        }



        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {

            if (activeGame.UserPlayer.Name == forPlayer.Name)
            {
                activeGame.AutoPlayNextCards = 2;
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }
            GameLogger.GetInstance().Log($"{Name} gibt vollgas und bringt zwei weitere RareLackner aufs Feld.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
