﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards {
    public  class RareLacknerCard : GameCard {

        public BindingList<GameCard> Upgrades { get; private set; }


        public int CombinedRarity => Rarity + Upgrades.Sum(x => x.Rarity);
        public int CombinedCoolness => Coolness + Upgrades.Sum(x => x.Coolness);

        /// <summary>
        /// Gets or sets the maximum amount of upgrades this card may receive.
        /// </summary>
        public int MaxUpgradeCount { get; set; } = Int32.MaxValue;

        public bool OnlyOwnerCanGiveUpgrades { get; set; }

        public int FreeUpgrades { get; set; }

        public bool EveryoneCanRemoveUpgrades { get; set; }

        public RareLacknerCard(int no, int id, string owner) : base(no, CardType.RareLackner, id, owner)
        {

            Upgrades = new BindingList<GameCard>();
            Upgrades.ListChanged += (sender, args) => {
                OnPropertyChanged(nameof(CombinedCoolness));
                OnPropertyChanged(nameof(CombinedRarity));
                foreach (var gameCard in Upgrades) {
                    gameCard.PropertyChanged -= GameCardOnPropertyChanged;
                    gameCard.PropertyChanged += GameCardOnPropertyChanged;
                }
            };

            NeededPoints = 2;
        }


        /// <summary>
        /// Games the card on property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="propertyChangedEventArgs">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void GameCardOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs) {
            if (propertyChangedEventArgs.PropertyName == nameof(Coolness)) OnPropertyChanged(nameof(CombinedCoolness));
            if (propertyChangedEventArgs.PropertyName == nameof(Rarity)) OnPropertyChanged(nameof(CombinedRarity));
        }

        /// <summary>
        /// Activates the specified active game. (Plays it)
        /// </summary>
        /// <param name="activeGame">The active game.</param>
        /// <param name="selectionTargets"></param>
        /// <param name="forPlayer"></param>
        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.Cards.Remove(this);
            forPlayer.PlayedCards.Add(this);
            forPlayer.PlayedRareLackner.Add(this);
        }

        /// <summary>
        /// Gets the selection targets.
        /// </summary>
        /// <returns></returns>
        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget>();
        }

        public override string ToString()
        {
            return $"MaxUpgradeCount: {MaxUpgradeCount} FreeUpgrades: {FreeUpgrades} EveryoneCanRemoveUpgrades: {EveryoneCanRemoveUpgrades} OwnerOnlyUpgrades: {OnlyOwnerCanGiveUpgrades}";
        }
    }
}
