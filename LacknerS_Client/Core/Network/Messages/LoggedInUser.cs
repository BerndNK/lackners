﻿using System;
using System.ComponentModel;

namespace LacknerS_Client.Core.Network.Messages {
    public class LoggedInUser : INotifyPropertyChanged {
        #region Fields
        private string _name;
        private DateTime _lastBeaconReceived;

        #endregion

        #region Properties
        public string Name {
            get { return _name; }
            set {
                _name = value; 
                CallPropertyChanged("Name");
            }
        }

        public DateTime LastBeaconReceived {
            get { return _lastBeaconReceived; }
            set {
                _lastBeaconReceived = value; 
                CallPropertyChanged("LastBeaconReceived");
            }
        }

        #endregion

        #region INotifyPropertyChangedImplementation
        private void CallPropertyChanged(string on) {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
