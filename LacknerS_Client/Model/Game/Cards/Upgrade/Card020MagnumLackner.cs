﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card020MagnumLackner : UpgradeCard{
        public Card020MagnumLackner(int id, string owner) : base(20, id, owner)
        {
            Rarity = -1;
            Coolness = 3;
        }
    }
}
