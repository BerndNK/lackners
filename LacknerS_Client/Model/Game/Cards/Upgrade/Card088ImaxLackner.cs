﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card088ImaxLackner : UpgradeCard
    {
        public Card088ImaxLackner(int id, string owner) : base(88, id, owner)
        {
            Rarity = 0;
            Coolness = 2;
        }
    }
}
