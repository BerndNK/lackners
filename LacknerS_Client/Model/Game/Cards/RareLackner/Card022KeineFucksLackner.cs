﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner {
    public class Card022KeineFucksLackner : RareLacknerCard{
        public Card022KeineFucksLackner(int id, string owner) : base(22, id, owner)
        {
            Rarity = 2;
            Coolness = 6;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            foreach (var player in activeGame.AllPlayer)
            {
                player.RarityLeadNeededForExtraCard = 3;
            }
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            GameLogger.GetInstance().Log("Keine Fucks Lackner zerstört. Jeder Spieler braucht wieder 2 Rarität mehr für das doppelte Karten ziehen.");
            foreach (var player in activeGame.AllPlayer)
            {
                player.RarityLeadNeededForExtraCard = 2;
            }
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
