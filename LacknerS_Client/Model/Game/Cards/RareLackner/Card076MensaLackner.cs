﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card076MensaLackner : RareLacknerCard
    {
        public Card076MensaLackner(int id, string owner) : base(76, id, owner)
        {
            Rarity = 3;
            Coolness = 3;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);
            }
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
