﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Encryption;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.Extensions;
using LacknerS_Client.Model;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Game.Cards;
using LacknerS_Client.Model.Host;
using LacknerS_Client.Views;

namespace LacknerS_Client.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class HostViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the current state of this host
        /// </summary>
        public HostState HostState {
            get { return _hostState; }
            set {
                Set(ref _hostState, value);
                RaisePropertyChanged(nameof(InPollingState));
                RaisePropertyChanged(nameof(InPlayingState));
            }
        }

        private HostState _hostState;

        public bool InPollingState => HostState == HostState.PollingClients;
        public bool InPlayingState => HostState == HostState.RegistrationComplete;

        /// <summary>
        /// Gets or sets the list of registered clients
        /// </summary>
        public BindingList<RegisteredClient> RegisteredClients {
            get { return _registeredClients; }
            set { Set(ref _registeredClients, value); }
        }

        private BindingList<RegisteredClient> _registeredClients;

        public ICommand StartCommand { get; private set; }
        public ICommand RestartCommand { get; private set; }
        public ICommand RequestCommand { get; private set; }

        private List<GameCard> _upgradeAndSpellCards;
        private List<GameCard> _tokenCards;
        private List<GameCard> _rareLacknerCards;
        private int _lastDrawnCard;

        /// <summary>
        /// Initializes a new instance of the HostViewModel class.
        /// </summary>
        public HostViewModel(IDataService dataService)
        {
            StartCommand = new RelayCommand(Start);
            RestartCommand = new RelayCommand(Restart);
            RequestCommand = new RelayCommand(RequestRegistration);

            dataService.GetRegisteredClients((list, exception) =>
            {
                if (exception != null)
                {
                    DebugLogger.GetInstance().LogError(exception.Message);
                    return;
                }

                RegisteredClients = list;
            });
            ConnectionHandler.GetInstance().PropertyChanged += ServerConnectorOnPropertyChanged;
            ConnectionHandler.GetInstance().MessageReceived += ServerConnectorOnMessageReceived;
        }

        private void Restart()
        {
            if (ConnectionHandler.GetInstance().ConnectionStatus != ConnectionHandler.ConnectionStatusEnum.Connected)
                return;
            ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.CloseGame);
        }

        private void RequestRegistration()
        {
            if (SelectionPanel.SelectedMode != ClientMode.Host) return; // only do this if the client is in host mode
            Application.Current.Dispatcher.Invoke(
                () => ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.HostHelloRequest));
            DebugLogger.GetInstance().LogInfo("Polling for client registrations");
        }

        private async void Start()
        {
            if (ConnectionHandler.GetInstance().ConnectionStatus != ConnectionHandler.ConnectionStatusEnum.Connected)
                return;
            if (HostState != HostState.PollingClients) return;
            HostState = HostState.RegistrationComplete;

            ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.StartGame);

            // send all players to each other
            foreach (var registeredClient in RegisteredClients)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.RegisterOpponent, registeredClient.Username);
            }
            _tokenCards = new List<GameCard>();
            _upgradeAndSpellCards = new List<GameCard>();
            _rareLacknerCards = new List<GameCard>();

            GenerateCards();


            var upgradeCardsToDeal = 3;
            var rareLacknerToDeal = 1;
#if DEBUG
            // deal all cards
            //upgradeCardsToDeal = _upgradeAndSpellCards.Count;
            //rareLacknerToDeal = _rareLacknerCards.Count;

#endif

            // deal each player cardsToDeal cards 
            for (var i = 0; i < upgradeCardsToDeal; i++)
            {
                foreach (var registeredClient in RegisteredClients)
                {

                    if (_upgradeAndSpellCards.Count == 0) break;
                    var newCard = _upgradeAndSpellCards.First();
                    _upgradeAndSpellCards.Remove(newCard);
                    _tokenCards.Add(newCard);
                    ConnectionHandler.GetInstance().SendDealCard(newCard.No, registeredClient.Username, newCard.Id);
                }
            }

            await Task.Delay(500);

            // + 1 rare lackner
            var autoPlayCards = new List<Tuple<string, GameCard>>();
            for (var i = 0; i < rareLacknerToDeal; i++)
            {
                foreach (var registeredClient in RegisteredClients)
                {
                    if (_rareLacknerCards.Count == 0) break;
                    var newCard = _rareLacknerCards.First();
                    _rareLacknerCards.Remove(newCard);
                    _tokenCards.Add(newCard);
                    ConnectionHandler.GetInstance().SendDealCard(newCard.No, registeredClient.Username, newCard.Id);
                    autoPlayCards.Add(new Tuple<string, GameCard>(registeredClient.Username, newCard));
                }
            }

            // set the active player to player 1
            ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.SetActivePlayer, RegisteredClients[0].Username);
            foreach (var autoPlayCard in autoPlayCards)
            {
                // automatically play it (for free)
                ConnectionHandler.GetInstance().SendPlayCard(autoPlayCard.Item2.Id, new List<SelectionTarget>(), autoPlayCard.Item1, true);
                await Task.Delay(500);
            }
        }

        private void GenerateCards()
        {
            var allCards = GameCardFactory.GetAllCards();
            _upgradeAndSpellCards = allCards.Where(x => x.CardType != CardType.RareLackner).ToList();
            _rareLacknerCards = allCards.Where(x => x.CardType == CardType.RareLackner).ToList();
            // shuffle
            _upgradeAndSpellCards.Shuffle();
            _rareLacknerCards.Shuffle();
            // give each an id
            var id = 1;
            foreach (var availableCard in _upgradeAndSpellCards)
            {
                availableCard.Id = id++;
            }
            foreach (var availableCard in _rareLacknerCards)
            {
                availableCard.Id = id++;
            }
        }

        private void ServerConnectorOnMessageReceived(object sender, ReceiveMessageEventArgs e)
        {
            // remove listener if the user has changed the mode to game (since this class is for host mode)
            if (SelectionPanel.SelectedMode == ClientMode.Game)
            {
                ConnectionHandler.GetInstance().PropertyChanged -= ServerConnectorOnPropertyChanged;
                ConnectionHandler.GetInstance().MessageReceived -= ServerConnectorOnMessageReceived;
                return;
            }
            if (SelectionPanel.SelectedMode == ClientMode.Waiting) return; // ignore all messages as long as the user has not selected a mode yet

            var msg = e.Message;

            if (msg is TextMessage)
            {
                HandleTextMessage(msg as TextMessage);
            }

            if (msg is OrganizationMessage)
            {
                HandleOrganizationMessage(msg as OrganizationMessage);
            }

            if (msg is GameStateChangedMessage)
            {
                HandleGameStateChangedMessage(msg as GameStateChangedMessage);
            }
        }

        private void HandleGameStateChangedMessage(GameStateChangedMessage gameStateChangedMessage)
        {
            // a client has reported that it's game state has changed. Check if the user is registered
            var user = RegisteredClients.FirstOrDefault(x => x.Username == gameStateChangedMessage.Username);

            DebugLogger.GetInstance().LogInfo("New game state for: " + gameStateChangedMessage.Username + " state: " + gameStateChangedMessage.GameState);

            // if the user was found, update the game state
            if (user != null)
            {
                // if the last time the game state was updated is more recent than the now received message, ignore the information
                if (user.LastGameStateUpdate > gameStateChangedMessage.TimeStamp) return;
                user.LastGameStateUpdate = gameStateChangedMessage.TimeStamp;
                user.GameState = gameStateChangedMessage.GameState;
            }
        }

        private void HandleOrganizationMessage(OrganizationMessage organizationMessage)
        {
            GameCard newCard;
            switch (organizationMessage.Type)
            {
                case OrganizationType.HostHelloRequest:
                    // ignored, this is for clients in game mode only
                    break;
                case OrganizationType.ClientHostHelloResponse:
                    if (!InPollingState) return;
                    DebugLogger.GetInstance().LogInfo("Client response received from: " + organizationMessage.User);
                    // a client has responded. Check if the client is not already registered.
                    if (RegisteredClients.ToList().Exists(x => x.Username == organizationMessage.User)) return;
                    DebugLogger.GetInstance().LogInfo("User has been registered. Sending response for: " + organizationMessage.User);
                    // if not, add the client and send confirmation response
                    RegisteredClients.Add(new RegisteredClient(organizationMessage.User));
                    ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.HostHelloResponseConfirmation, organizationMessage.User);

                    break;
                case OrganizationType.HostHelloResponseConfirmation:
                    // ignored, this is for clients in game mode only
                    break;
                case OrganizationType.EndTurn:
                    // get index of sender
                    var sender = RegisteredClients.First(x => x.Username == organizationMessage.User);
                    var index = RegisteredClients.IndexOf(sender);
                    index++;
                    if (index >= RegisteredClients.Count) index = 0;
                    // deal new card
                    /* newCard = _upgradeAndSpellCards.FirstOrDefault();
                     if (newCard != null)
                     {
                         _upgradeAndSpellCards.Remove(newCard);
                         _tokenCards.Add(newCard);
                         ConnectionHandler.GetInstance().SendDealCard(newCard.No, RegisteredClients[index].Username, newCard.Id);
                     }*/
                    // set move
                    ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.SetActivePlayer, RegisteredClients[index].Username);

                    break;
                case OrganizationType.DrawGreenCard:
                    if (_rareLacknerCards.Count == 0) break;
                    newCard = _rareLacknerCards.First();
                    _rareLacknerCards.Remove(newCard);
                    _tokenCards.Add(newCard);
                    _lastDrawnCard = newCard.Id;
                    ConnectionHandler.GetInstance().SendDealCard(newCard.No, organizationMessage.User, newCard.Id);
                    break;
                case OrganizationType.DrawYellowCard:
                    if (_upgradeAndSpellCards.Count == 0) break;
                    newCard = _upgradeAndSpellCards.First();
                    _upgradeAndSpellCards.Remove(newCard);
                    _tokenCards.Add(newCard);
                    _lastDrawnCard = newCard.Id;
                    ConnectionHandler.GetInstance().SendDealCard(newCard.No, organizationMessage.User, newCard.Id);
                    break;
                case OrganizationType.PlayLastDrawnCard:
                    ConnectionHandler.GetInstance().SendPlayCard(_lastDrawnCard, new List<SelectionTarget>(), organizationMessage.User, true);
                    break;
                case OrganizationType.CloseGame:
                    HostState = HostState.PollingClients;
                    break;
            }
        }

        private void HandleTextMessage(TextMessage msg)
        {
            var text = Crypto.Decrypt(msg.Text, SettingsWindow.GetSetting("cryptopassword").ToString());
            DebugLogger.GetInstance().LogInfo(text);
        }

        private void ServerConnectorOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // remove listener if the user has changed the mode to game (since this class is for host mode)
            if (SelectionPanel.SelectedMode == ClientMode.Game)
            {
                ConnectionHandler.GetInstance().PropertyChanged -= ServerConnectorOnPropertyChanged;
                ConnectionHandler.GetInstance().MessageReceived -= ServerConnectorOnMessageReceived;
                return;
            }
            if (e.PropertyName != "ConnectionStatus") return;

            switch (ConnectionHandler.GetInstance().ConnectionStatus)
            {
                case ConnectionHandler.ConnectionStatusEnum.Disconnected:
                case ConnectionHandler.ConnectionStatusEnum.Connecting:
                    break;
                case ConnectionHandler.ConnectionStatusEnum.Connected:
                    HostState = HostState.PollingClients;
                    ConnectionHandler.GetInstance().StartListener();
                    StartPolling();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void StartPolling()
        {
            Task.Run(() =>
            {
                while (HostState == HostState.PollingClients)
                {
                    Thread.Sleep(6000);
                    RequestRegistration();
                }
            });

        }
    }
}