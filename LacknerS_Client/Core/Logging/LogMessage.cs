using System;

namespace LacknerS_Client.Core.Logging {
    public struct LogMessage {
        /// <summary>
        /// The message
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// The log type
        /// </summary>
        public LogType LogType { get; private set; }

        /// <summary>
        /// The occurrence time of the message
        /// </summary>
        public DateTime OccurenceTime { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogMessage"/> struct.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logType">Type of the log.</param>
        public LogMessage(string message, LogType logType) {
            Message = message;
            LogType = logType;
            OccurenceTime = DateTime.Now;
        }

        public override string ToString() {
            return OccurenceTime.ToString("[hh:mm:ss]") + " " + LogType + " - " + Message;
        }
    }
}