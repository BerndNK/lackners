#ifndef _SOCKET_HANDLER_H_
#define _SOCKET_HANDLER_H_
#include <vector>
#include <thread>
#include <mutex>

#define DEFAULT_BUFLEN 16384
#define DEFAULT_PORT "123"
#define MAX_AMOUNT_FULL_MESSAGES 30

namespace network {
	class SocketHandler {
	public:
		struct Message {
			int Length;
			char* Array;

			explicit Message(int length, char* array) : Length(length), Array(array) {};
		};

		struct FullMessage
		{
			std::vector<Message> Parts;
			int Id;

			explicit FullMessage(int id) : Id(id) {};
		};

		struct Client
		{
			std::thread* Thread;
			std::mutex ListMutex;
			std::vector<Message> ToPushList;
			bool IsConnected;
			int No;

			explicit Client(std::thread* thread, int no) : Thread(thread), No(no), IsConnected(false) {}
		};

		bool HttpMode = false;

		SocketHandler(std::string port = DEFAULT_PORT) : WaitingForClient(m_waitingForClient), m_port(port) {};

		virtual ~SocketHandler() {};

		const bool& WaitingForClient;

		// Creates the socket and sets everything up to start listening
		virtual int Initialize() = 0;

		// Waits for all started threads to terminte
		void WaitForThreads();

		// Creates a new listener
		void CreateListener();

		// queres the given messages for all connected clients except the sender
		void QueueMessage(Client* sender, char msg[DEFAULT_BUFLEN], int length);

		// removes a client from the pool
		void RemoveClient(Client* client);

	protected:
		bool m_isInitalized = false;
		bool m_waitingForClient = false;
		std::vector<Client*> m_clientPool;
		std::mutex m_clientMutex;
		std::mutex m_coutMutex;
		int m_clientCounter = 0;
		std::string m_port;
		std::vector<FullMessage*> m_fullCachedMessaged;
		std::mutex m_messagesMutex;
		int m_fullMsgIdCounter = 0;

		virtual void Listener(bool* waitingForClientBool, Client* thisClientPtr) = 0;

		virtual void Sender(Client* thisClientPtr, void* socket, bool* forceQuitBool) = 0;

		virtual void ListenerHttp(bool* waitingForClientbool, Client* thisClientPtr) = 0;
	};
}
#endif
