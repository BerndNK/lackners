﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LacknerS_Client.Converter {
    public class TextToBoolConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var str = value.ToString();
            var whiteSpace = string.IsNullOrWhiteSpace(str);
            return !whiteSpace;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
