﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card086KonzertVorbeiLackner : SpellCard
    {
        public Card086KonzertVorbeiLackner(int id, string owner) : base(86, id, owner)
        {
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {

            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                foreach (var player in activeGame.AllPlayer)
                {
                    ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, player.Name);
                }
            }
            GameLogger.GetInstance().Log($"{Name} lässt jeden eine gelbe Karte ziehen.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
