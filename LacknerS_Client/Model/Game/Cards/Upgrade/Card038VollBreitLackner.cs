﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card038VollBreitLackner : UpgradeCard
    {
        public Card038VollBreitLackner(int id, string owner) : base(38, id, owner)
        {
            Rarity = 3;
            Coolness = -2;
        }
    }
}
