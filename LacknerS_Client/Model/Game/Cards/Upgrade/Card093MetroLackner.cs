﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card093MetroLackner : UpgradeCard
    {
        public Card093MetroLackner(int id, string owner) : base(93, id, owner)
        {
            Rarity = 1;
            Coolness = 0;
        }
    }
}
