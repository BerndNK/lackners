﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card062BusinessLackner : UpgradeCard
    {
        public Card062BusinessLackner(int id, string owner) : base(62, id, owner)
        {
            Rarity = 0;
            Coolness = 2;
        }
    }
}
