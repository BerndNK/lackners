﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card082KetchupLackner : SpellCard
    {
        public Card082KetchupLackner(int id, string owner) : base(82, id, owner)
        {
            Power = SpellCardPower.High;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget>
            {
                new SelectionTarget(SelectionPlayedType.OwnedCard, CardType.AllPlayable, OwnerType.Enemy),
                new SelectionTarget(SelectionPlayedType.OwnedCard, CardType.AllPlayable, OwnerType.Enemy)
            };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var card = GetCardById(selectionTargets.First().SelectedCard);
            var card2 = GetCardById(selectionTargets.Last().SelectedCard);

            var owner = GetOwner(card, activeGame);
            owner.Cards.Remove(card);
            forPlayer.Cards.Add(card);

            owner = GetOwner(card2, activeGame);
            owner.Cards.Remove(card2);
            forPlayer.Cards.Add(card2);

            card.IsRevealed = activeGame.UserPlayer.Name == forPlayer.Name;
            card2.IsRevealed = activeGame.UserPlayer.Name == forPlayer.Name;

            GameLogger.GetInstance().Log($"{Name} hat {card.Name} und {card2.Name} gestohlen.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override bool IsSpecialRequirementFulfilled(ActiveGameViewModel activeGame)
        {
            // at least two cards to select
            return activeGame.Opponents.Sum(x => x.Cards.Count) > 2;
        }
    }
}
