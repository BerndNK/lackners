﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network.Messages;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Core.Network {
    public class ConnectionHandler : INotifyPropertyChanged {
        #region Fields
        private ConnectionMode _mode;
        private IServerConnector _activeConnection;
        private readonly SocketConnector _socketConnector;

        #endregion

        #region Properties

        public ConnectionMode Mode {
            get { return _mode; }
            set {
                _mode = value;
                switch (value) {
                    case ConnectionMode.Socket:
                        _activeConnection = _socketConnector;
                        break;
                    default:
                        throw new InvalidEnumArgumentException(@"Invalid value given");
                }
                CallPropertyChanged("Mode");
            }
        }

        public string StatusText {
            get { return _activeConnection.StatusText; }
        }

        public ConnectionStatusEnum ConnectionStatus {
            get { return _activeConnection.ConnectionStatus; }
        }

        public string ProxyAdddress {
            get { return _activeConnection.ProxyAddress; }
            set { _activeConnection.ProxyAddress = value; }
        }

        public string ProxyUsername {
            get { return _activeConnection.ProxyUsername; }
            set { _activeConnection.ProxyUsername = value; }
        }

        public string ProxyPassword {
            get { return _activeConnection.ProxyPassword; }
            set { _activeConnection.ProxyPassword = value; }
        }

        public string ProxyDomain {
            get { return _activeConnection.ProxyDomain; }
            set { _activeConnection.ProxyDomain = value; }
        }

        #endregion

        #region Enums
        public enum ConnectionMode {
            Socket = 0,
            Http = 1
        }
        #endregion

        #region ValueTypes
        public enum ConnectionStatusEnum {
            Disconnected = 0,
            Connecting,
            Connected
        }

        #endregion

        #region Events
        public delegate void MessageReceivedHandler(object sender, ReceiveMessageEventArgs e);
        public event MessageReceivedHandler MessageReceived;

        public delegate void BytesReceivedHandler(object sender, BytesReceivedEventArgs e);
        #endregion

        public ConnectionHandler(ConnectionMode mode) {
            // create Socket Connector
            _socketConnector = new SocketConnector();

            _socketConnector.BytesReceived += OnBytesReceived;
            _socketConnector.PropertyChanged += OnPropertyChanged;

            // set initial active connection
            Mode = mode;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs) {
            CallPropertyChanged(propertyChangedEventArgs.PropertyName);
        }

        private void OnBytesReceived(object sender, BytesReceivedEventArgs args) {
            if (MessageReceived != null) {
                var eventArgs = new ReceiveMessageEventArgs(DeserializeMessage(args.Bytes));
                MessageReceived(this, eventArgs);
                DebugLogger.GetInstance().LogInfo("Message received: "+eventArgs.Message.GetType());
            }
                
        }

        #region Connect
        public void Connect(string target, int port) {
            _activeConnection.Connect(target, port);
        }
        #endregion

        #region Data Handling
        private static Message DeserializeMessage(byte[] messageBuffer) {
            try {
                // deserialize the message container
                var container = MessageContainer.GetContainer(messageBuffer);

                // if it worked check the version, then deserialize the data sent
                if (container != null)
                    return container.Version != MessageContainer.CurrentVersion ? new ErrorMessage("Received outdated Message.") : Message.FromContainer(container);

                return new ErrorMessage("Failed to cast received _bytes into Message Container");
            } catch (Exception e) {
                // if something goes wrong return a error message
                return new ErrorMessage(e.Message);
            }
        }
        #endregion

        #region Data Receiving
        public void StartListener() {
            _activeConnection.StartListener();
        }
        #endregion

        #region Data Sending
        public void Send(string message) {
            _activeConnection.Send(message);
        }

        public void Send(BitmapSource image) {
            _activeConnection.Send(image);
        }

        public void SendFile(string path) {
            _activeConnection.SendFile(path);
        }

        public void Send(GameState state) {
            _activeConnection.Send(state);
        }

        public void SendOrgaMessage(OrganizationType type, string user = null) {
            _activeConnection.Send(type, user);
        }

        public void SendDealCard(int type, string forPlayer, int id) {
            _activeConnection.SendCard(type, forPlayer, id);
        }

        public void SendPlayCard(int id, List<SelectionTarget> onIds, string forPlayer = null, bool forFree = false) {
            _activeConnection.SendPlayCard(id, onIds, forPlayer, forFree);
        }

        public void SendTrade(TradeMessageType type, int forCardId)
        {
            _activeConnection.SendTrade(type, forCardId);
        }

        public void SendSwap(int firstCardId, int secondCardId)
        {
            _activeConnection.SendSwap(firstCardId, secondCardId);
        }
        #endregion

        #region INotifyPropertyChangedImplementation
        private void CallPropertyChanged(string on) {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        private static ConnectionHandler _instance;
        public static ConnectionHandler GetInstance() {
            if (_instance == null) _instance = new ConnectionHandler(ConnectionMode.Socket);
            return _instance;
        }
    }
}
