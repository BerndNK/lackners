﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card030TafelWischLackner : SpellCard
    {

        public Card030TafelWischLackner(int id, string owner) : base(30, id, owner) {
            Rarity = 2;
            Coolness = -1;
        }


        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.All) };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var id = selectionTargets.First().SelectedCard;
            var card = GetCardById(id);
            var owner = GetOwner(card, activeGame);
            var rareLackner = GetContainingRareLackner(card as UpgradeCard);
            rareLackner.Upgrades.Remove(card);
            owner.PlayedCards.Remove(card);
            owner.Cards.Add(card);

            activeGame.ResetEventListener(card);
            
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
