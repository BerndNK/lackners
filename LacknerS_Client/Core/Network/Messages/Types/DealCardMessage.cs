﻿using System;

namespace LacknerS_Client.Core.Network.Messages.Types {
    [Serializable]
    public class DealCardMessage : Message {

        public int Type { get; set; }

        public string ForPlayer { get; set; }

        public int Id { get; set; }

        public DealCardMessage(int type, string forPlayer, int id) {
            Type = type;
            ForPlayer = forPlayer;
            Id = id;
        }

    }
}
