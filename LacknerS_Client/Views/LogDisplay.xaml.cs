﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using LacknerS_Client.Core.Logging;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Description for LogDisplay.
    /// </summary>
    public partial class LogDisplay : UserControl, INotifyPropertyChanged {
        private BindingList<LogMessage> _messages;

        public BindingList<LogMessage> Messages {
            get { return _messages; }
            set {
                _messages = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the LogDisplay class.
        /// </summary>
        public LogDisplay() {
            InitializeComponent();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}