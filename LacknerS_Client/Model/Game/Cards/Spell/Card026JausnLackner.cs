﻿using System.Collections.Generic;
using System.Linq;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card026JausnLackner : SpellCard
    {
        public Card026JausnLackner(int id, string owner) : base(26, id, owner)
        {
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets,
            Player forPlayer)
        {
            if (activeGame.UserPlayer.Name == forPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }
            GameLogger.GetInstance().Log($"{Name} macht mit {forPlayer.Name} eine Jausn.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
        
    }
}