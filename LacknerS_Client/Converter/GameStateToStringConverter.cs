﻿using System;
using System.Globalization;
using System.Windows.Data;
using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Converter {
    public class GameStateToStringConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var asEnum = (GameState) value;
            switch (asEnum) {
                case GameState.WaitingForHostRequest:
                    return "Waiting for the host to request registration.";
                case GameState.SentResponse:
                    return "Request received. Sent registration.";
                case GameState.HostHasConfirmed:
                    return "Registration confirmed.";
                    case GameState.GameClosed:
                        return "The host closed the game.";
                case GameState.Lost:
                        return "You lost the last game. Better luck next time!";
                case GameState.Won:
                        return "You won the last game! Congratulations.";
                default:
                    return "Error";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
