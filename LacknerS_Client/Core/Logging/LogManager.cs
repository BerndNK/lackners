﻿using System.Collections.Generic;
using System.Linq;

namespace LacknerS_Client.Core.Logging {
    /// <summary>
    /// Manages ILogger and provides methods to get specified log messages
    /// </summary>
    public static class LogManager {
        public static IList<LogMessage> GetMessages(ILogger logger, LogType ofLogType) => logger.GetLogs().Where(x => (x.LogType & ofLogType) == x.LogType).ToList();
    }
}
