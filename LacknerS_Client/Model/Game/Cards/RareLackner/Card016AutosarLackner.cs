﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner {
    public class Card016AutosarLackner : RareLacknerCard{
        public Card016AutosarLackner(int id, string owner) : base(16, id, owner)
        {
            Rarity = 4;
            Coolness = 4;
            OnlyOwnerCanGiveUpgrades = true;
        }
        
    }
}
