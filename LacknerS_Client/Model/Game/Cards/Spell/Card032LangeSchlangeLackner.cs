﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card032LangeSchlangeLackner : SpellCard
    {

        public Card032LangeSchlangeLackner(int id, string owner) : base(32, id, owner) {
            Rarity = 2;
            Coolness = -1;
            Power = SpellCardPower.Middle;
        }


        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionType.Player, OwnerType.Enemy) };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // points for next turn decreased by one
            var targetedPlayer = activeGame.AllPlayer.First(x => x.Name == selectionTargets.First().SelectedPlayer);
            targetedPlayer.TurnSkips++;
            GameLogger.GetInstance().Log($"{Name} steckt mit {targetedPlayer.Name} in einer langen Schlange!");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
