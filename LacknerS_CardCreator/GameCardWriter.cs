﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Game.Cards;
using PS = Photoshop;

namespace LacknerS_CardCreator
{
    class GameCardWriter
    {

        private string MasterTemplatePath = @"C:\Users\Bernd\OneDrive\Projects\LacknerS\MasterTemplate2.psd";
        private string FaceCardsPath = @"C:\Users\Bernd\OneDrive\Projects\LacknerS\Cards\FaceOnly";
        private List<string> _boldWords { get; } = new List<string> { "UPGRADE:", "CAST:", "EFFEKT:" };

        public void CreateCards(IEnumerable<GameCard> cards, string destination)
        {
            Console.WriteLine("Starting Photoshop");
            var app = new PS.Application();
            PS.Document document = null;
            try
            {
                Directory.CreateDirectory(destination);

                Console.WriteLine("Open master template");
                document = app.Open(MasterTemplatePath);
                foreach (var gameCard in cards)
                {
                    Console.WriteLine($"Creating card {gameCard.SetName}");
                    var pastedLayers = InsertFace(app, document, gameCard);
                    SetTemplate(document, gameCard);
                    app.Refresh();
                    SetSpellStrength(document, gameCard);
                    SetDescriptionColor(document, gameCard);
                    SetTexts(document, app, gameCard);
                    app.Refresh();

                    var outputPath = Path.Combine(destination, $"{gameCard.No:D3}_{NameToFileName(gameCard)}.png");
                    Persist(document, outputPath);

                    RemoveLayers(document, pastedLayers);
                }
            }

            catch (Exception e)
            {
                Console.WriteLine($"Exception thrown {e}");
                Console.ReadKey();
            }
            finally
            {
                document?.Close(PS.PsSaveOptions.psDoNotSaveChanges);
                app.Quit();
            }
        }

        private void SetDescriptionColor(PS.Document document, GameCard gameCard)
        {
            foreach (var artLayer in document.ArtLayers().Where(layer => layer.Name.StartsWith("Description")))
            {
                string cardTypeLayerName;
                switch (gameCard.CardType)
                {
                    case CardType.Upgrade:
                        cardTypeLayerName = "Upgrade";
                        break;
                    case CardType.RareLackner:
                        cardTypeLayerName = "Rare";
                        break;
                    case CardType.Spell:
                        cardTypeLayerName = "Spell";
                        break;
                    case CardType.Special:
                        cardTypeLayerName = "Special";
                        break;
                    default:
                        document.ActiveLayer = artLayer;
                        artLayer.Visible = false;
                        continue;
                }

                if (gameCard.IsUltraRare)
                    cardTypeLayerName = "Ultra";
                document.ActiveLayer = artLayer;
                artLayer.Visible = artLayer.Name.Contains(cardTypeLayerName);
            }
        }



        private int GetBoldIndex(GameCard gameCard)
        {
            foreach (var boldWord in _boldWords)
            {
                var index = gameCard.Description.IndexOf(boldWord, StringComparison.InvariantCultureIgnoreCase);
                if (index != -1)
                    return index;
            }

            return -1;
        }

        private int GetBoldLength(GameCard gameCard)
        {
            foreach (var boldWord in _boldWords)
            {
                var index = gameCard.Description.IndexOf(boldWord, StringComparison.InvariantCultureIgnoreCase);
                if (index != -1)
                    return boldWord.Length;
            }

            return -1;
        }

        private void SetSpellStrength(PS.Document document, GameCard gameCard)
        {
            var documentArtLayers = document.ArtLayers().ToList();
            var artLayers = documentArtLayers.Where(layer => !layer.Name.Contains("Template") && layer.Name.Contains("Spell")).ToList();
            foreach (var artLayer in artLayers)
            {
                if (gameCard is SpellCard spellCard)
                {
                    var spellLayerName = SpellPowerToString(spellCard);

                    if(string.IsNullOrEmpty(spellLayerName))
                        continue;
                    
                    document.ActiveLayer = artLayer;
                    artLayer.Visible = artLayer.Name.Contains(spellLayerName);
                }
                else
                {
                    document.ActiveLayer = artLayer;
                    artLayer.Visible = false;
                }
            }
        }

        private string SpellPowerToString(SpellCard spellCard)
        {
            switch (spellCard.Power)
            {
                case SpellCardPower.Low:
                    return "Weak";
                    break;
                case SpellCardPower.Middle:
                    return "Medium";
                    break;
                case SpellCardPower.High:
                    return "Strong";
                    break;
                default:
                    return null;
            }
        }

        private void RemoveLayers(PS.Document document, List<PS.ArtLayer> pastedLayers)
        {
            foreach (var pastedLayer in pastedLayers)
            {
                document.ArtLayers.Remove(pastedLayer);
            }
        }

        private void SetTexts(PS.Document document, PS.Application app, GameCard gameCard)
        {
            SetText(document, "Title", gameCard.SetName);
            SetText(document, "Rarity", gameCard.Rarity == 0 ? "" : (gameCard.CardType == CardType.Upgrade && gameCard.Rarity > 0 ? "+" + gameCard.Rarity.ToString() : gameCard.Rarity.ToString()));
            if(gameCard is SpellCard spellCard)
            {
                SetText(document, "Coolness", ">" + (spellCard.NeededCoolness - 1));
            }else
                SetText(document, "Coolness", gameCard.Coolness == 0 ? "" : (gameCard.CardType == CardType.Upgrade && gameCard.Coolness > 0 ? "+" + gameCard.Coolness.ToString() : gameCard.Coolness.ToString()));
            SetText(document, "Description", gameCard.Description);
            SetTextStyle(app, "Bold", GetBoldIndex(gameCard), GetBoldLength(gameCard));
            SetText(document, "Number", gameCard.No.ToString("D3"));
        }

        private void SetText(PS.Document document, string textFieldName, string content)
        {
            var textLayers = document.ArtLayers().Where(layer => layer.Kind == PS.PsLayerKind.psTextLayer && layer.Name.Contains(textFieldName)).ToList();
            var minHorizontalScale = 100;
            foreach (var textLayer in textLayers)
            {
                if (!textLayer.Visible)
                    continue;
                var textItem = textLayer.TextItem;
                document.ActiveLayer = textLayer;
                textItem.Contents = content.Replace("\n", "\r");
                textItem.HorizontalScale = minHorizontalScale;
                if (textFieldName != "Title")
                    continue;
                while (WidthOfTextLayer(textLayer, document) >= 1000 && textItem.HorizontalScale > 20)
                {
                    textItem.HorizontalScale -= 4;
                    if (textItem.HorizontalScale < minHorizontalScale)
                        minHorizontalScale = textItem.HorizontalScale;
                }
            }

            if (textFieldName != "Title")
                return;

            foreach (var textLayer in textLayers)
            {
                if (!textLayer.Visible)
                    continue;
                var textItem = textLayer.TextItem;
                textItem.HorizontalScale = minHorizontalScale;
            }
        }

        private double WidthOfTextLayer(PS.ArtLayer textLayer, PS.Document doc)
        {
            PS.ArtLayer duplicate = textLayer.Duplicate();
            duplicate.Rasterize(PS.PsRasterizeType.psEntireLayer);
            double width = duplicate.Bounds[2] - duplicate.Bounds[0];
            doc.Layers.Remove(duplicate);
            return width;
        }


        private void SetTemplate(PS.Document document, GameCard gameCard)
        {
            var targetName = gameCard.CardType.ToString();

            if (gameCard is SpellCard spellCard)
                targetName = targetName + SpellPowerToString(spellCard);

            if(gameCard is UpgradeCard upgradeCard)
            {
                var modifiesRarity = upgradeCard.Rarity != 0;
                var modifiesCoolness = upgradeCard.Coolness != 0;

                if (modifiesCoolness && modifiesRarity)
                    targetName = targetName + "Both";
                else if (modifiesCoolness)
                    targetName = targetName + "CoolOnly";
                else if (modifiesRarity)
                    targetName = targetName + "RareOnly";
            }

            if (gameCard.IsUltraRare)
                targetName = "Ultra";

            foreach (PS.ArtLayer layer in document.ArtLayers().Where(layer => layer.Name.Contains("Template")))
            {
                document.ActiveLayer = layer;
                layer.Visible = layer.Name.Contains(targetName);
            }

        }

        private void Persist(PS.Document document, string outputPath)
        {
            var options = new PS.PNGSaveOptions();
            document.SaveAs(outputPath, options, true);
        }

        private List<PS.ArtLayer> InsertFace(PS.Application app, PS.Document document, GameCard gameCard)
        {
            var path = GetFaceCardPath(gameCard);
            var faceDoc = app.Open(path);
            var targetIndexLayer = GetFaceLayer(document);
            var pastedLayers = new List<PS.ArtLayer>();
            app.Refresh();
            faceDoc.ResizeCanvas(document.Width * document.Resolution / faceDoc.Resolution, document.Height * document.Resolution / faceDoc.Resolution, PS.PsAnchorPosition.psMiddleCenter);

            app.Refresh();

            foreach (PS.ArtLayer artLayer in faceDoc.ArtLayers)
            {
                var newLayer = artLayer.Duplicate(document);
                pastedLayers.Add(newLayer);
            }
            app.Refresh();
            faceDoc.Close(PS.PsSaveOptions.psDoNotSaveChanges);
            app.Refresh();
            app.ActiveDocument = document;

            app.Refresh();
            foreach (var pastedLayer in pastedLayers)
            {
                pastedLayer.Move(targetIndexLayer, PS.PsElementPlacement.psPlaceAfter);
            }
            app.Refresh();

            return pastedLayers;
        }

        private PS.ArtLayer GetFaceLayer(PS.Document document)
        {
            foreach (PS.ArtLayer layer in document.ArtLayers)
            {
                if (layer.Name == "FaceLayer")
                    return layer;
            }

            return null;
        }

        private string GetFaceCardPath(GameCard gameCard)
        {
            var filePath = Directory
                .EnumerateFiles(FaceCardsPath)
                .FirstOrDefault(file => file.IndexOf(NameToFileName(gameCard), StringComparison.InvariantCultureIgnoreCase) != -1);
            if (filePath == null)
                Console.WriteLine();
            return filePath;
        }

        private string NameToFileName(GameCard gameCard)
        {
            return gameCard.SetName.Replace(" ", "").Replace("-", "");
        }

        private void SetTextStyle(PS.Application app, string fontStyle, int start, int length)
        {
            if (start <= 0 || length <= 0)
                return;
            var idsetd = app.CharIDToTypeID("setd");
            var action = new PS.ActionDescriptor();
            var idnull = app.CharIDToTypeID("null");
            //The action reference specifies the active text layer.
            var reference = new PS.ActionReference();
            var idTxLr = app.CharIDToTypeID("TxLr");
            var idOrdn = app.CharIDToTypeID("Ordn");
            var idTrgt = app.CharIDToTypeID("Trgt");
            reference.PutEnumerated(idTxLr, idOrdn, idTrgt);
            action.PutReference(idnull, reference);
            var idT = app.CharIDToTypeID("T   ");
            var textAction = new PS.ActionDescriptor();
            var idTxtt = app.CharIDToTypeID("Txtt");
            //actionList contains the sequence of formatting actions.
            var actionList = new PS.ActionList();
            //textRange sets the range of characters to format.
            var textRange = new PS.ActionDescriptor();
            var idFrom = app.CharIDToTypeID("From");
            textRange.PutInteger(idFrom, start);
            textRange.PutInteger(idT, start + length);
            var idTxtS = app.CharIDToTypeID("TxtS");
            //The "formatting" ActionDescriptor holds the formatting. It should be clear that you can
            //add other attributes here--just get the relevant lines (usually 2) from the Script Listener 
            //output and graft them into this section.
            var formatting = new PS.ActionDescriptor();

            formatting.PutString(app.CharIDToTypeID("FntS"), fontStyle);
            formatting.PutString(app.CharIDToTypeID("FntN"), "Arial");
            formatting.PutUnitDouble(app.CharIDToTypeID("Sz  "), app.CharIDToTypeID("#Pnt"), 8.68);

            textRange.PutObject(idTxtS, idTxtS, formatting);
            actionList.PutObject(idTxtt, textRange);
            textAction.PutList(idTxtt, actionList);
            action.PutObject(idT, idTxLr, textAction);
            app.ExecuteAction(idsetd, action, PS.PsDialogModes.psDisplayNoDialogs);
        }
    }


    public static class DocumentExtension
    {
        public static IEnumerable<PS.ArtLayer> ArtLayers(this PS.Document doc)
        {
            var list = new List<PS.ArtLayer>();

            foreach (PS.ArtLayer artLayer in doc.ArtLayers)
            {
                list.Add(artLayer);
            }

            return list;
        }
    }
}
