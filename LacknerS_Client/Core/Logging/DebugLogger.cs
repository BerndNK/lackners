﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LacknerS_Client.Core.Logging {
    public class DebugLogger : ILogger {
        /// <summary>
        /// The logged messages
        /// </summary>
        private readonly BindingList<LogMessage> _messages;

        #region Singleton pattern
        /// <summary>
        /// The singleton instance
        /// </summary>
        private static DebugLogger _instance;

        /// <summary>
        /// Gets the instance of the DebugLogger.
        /// </summary>
        /// <returns>DebugLogger instance</returns>
        public static DebugLogger GetInstance() {
            return _instance ?? (_instance = new DebugLogger());
        }

        #endregion

        /// <summary>
        /// Occurs when a new [message is logged].
        /// </summary>
        public event EventHandler<MessageLoggedEventArgs> MessageLogged;

        /// <summary>
        /// Prevents a default instance of the <see cref="DebugLogger"/> class from being created.
        /// </summary>
        private DebugLogger() {
            _messages = new BindingList<LogMessage>();
        }

        /// <summary>
        /// Logs the specified message as error and also gives the CallMemberName.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="memberName"></param>
        public void LogError(string message,  [CallerMemberName] string memberName = null) {
            // log where the error occurred (calling method)
            message = "Caller: " + memberName+"\n" + message;
            Log(message, LogType.Error);
        }

        /// <summary>
        /// Logs the specified message and also gives the CallMemberName.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="memberName"></param>
        public void LogInfo(string message, [CallerMemberName] string memberName = null) {
            // log where the error occurred (calling method)
            message = "In: " + memberName + "\n" + message;
            Log(message);
        }

        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logType">Type of the log message.</param>
        public void Log(string message, LogType logType = LogType.Info) {
            // assert, that nobody calls this method with logType = Error, as the LogError method is meant for that
            if(logType == LogType.Error) Debug.WriteLine("Log method was used with LogType = Error. Use LogError method instead");
            var logMessage = new LogMessage(message, logType);
            Application.Current.Dispatcher.Invoke(() => {
                _messages.Add(logMessage);
                MessageLogged?.Invoke(this, new MessageLoggedEventArgs(logMessage));
            });
            
        }

        /// <summary>
        /// Gets the logged messages.
        /// </summary>
        /// <returns>
        /// The logged messages
        /// </returns>
        public BindingList<LogMessage> GetLogs() => _messages;
    }
}
