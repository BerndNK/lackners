﻿using System;
using System.Windows;
using LacknerS_Client.ViewModel;
using LacknerS_Client.Views;
using MahApps.Metro;
using MahApps.Metro.Controls;

namespace LacknerS_Client {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>

        public static int WindowX;
        public static int WindowY;
        public static int WindowW = 600;
        public static int WindowH = 400;

        public MainWindow() {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
            try
            {
                ThemeManager.ChangeAppStyle(Application.Current,
                    ThemeManager.GetAccent(SettingsWindow.GetSetting("applicationAccent").ToString()),
                    ThemeManager.GetAppTheme(SettingsWindow.GetSetting("applicationTheme").ToString()));
            }
            catch (Exception)
            {
                // ignored
            }

        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.Left = WindowX;
            this.Top = WindowY;
            this.Height = WindowH;
            this.Width = WindowW;
        }
    }
}