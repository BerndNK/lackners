﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Controls;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages;
using LacknerS_Client.Model;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Views;

namespace LacknerS_Client.ViewModel {
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class TradeViewModel : ViewModelBase {
        private GameCard _selectedCard;
        private string _status;
        private bool _controlsEnabled;
        private TradeState _state;
        public BindingList<GameCard> AvailableCards { get; set; }

        public BindingList<TradeOffer> TradeOffers { get; set; }

        public ICommand OnCardClickCommand { get; set; }
        public ICommand OfferCommand { get; set; }
        public ICommand OnClickedOfferedCard { get; set; }
        public ICommand OnCloseCommand { get; set; }

        public bool SelectedACard => SelectedCard != null;

        public string Status {
            get { return _status; }
            set { Set(ref _status, value); }
        }

        public bool ControlsEnabled {
            get { return _controlsEnabled; }
            set { Set(ref _controlsEnabled, value); }
        }

        public GameCard SelectedCard {
            get { return _selectedCard; }
            set {
                Set(ref _selectedCard, value);
                PropertyChangedHandler(this, new PropertyChangedEventArgs(nameof(SelectedACard)));
            }
        }

        public TradeState State {
            get { return _state; }
            set { Set(ref _state, value); }
        }

        /// <summary>
        /// Initializes a new instance of the TradeViewModel class.
        /// </summary>
        public TradeViewModel() {
            OnCardClickCommand = new RelayCommand<object>(OnCardClick);
            OfferCommand = new RelayCommand(Offer);
            OnClickedOfferedCard = new RelayCommand<object>(ClickedOfferedCard);
            OnCloseCommand = new RelayCommand(OnClose);

            Status = "Select a card";
            ControlsEnabled = true;
            TradeOffers = new BindingList<TradeOffer>();
            AvailableCards = new BindingList<GameCard>();
        }

        private void OnClose() {
            if (ActiveGameViewModel.Current.IsTrading) {
                if (ActiveGameViewModel.Current.HasMove) {
                    ConnectionHandler.GetInstance().SendTrade(TradeMessageType.CancelOffer, -1);
                } else {
                    ConnectionHandler.GetInstance().SendTrade(TradeMessageType.RefuseTrade, -1);
                }

                Clear();
                ActiveGameViewModel.Current.IsTrading = false;
            }


        }

        public void OnOpen() {
            Clear();
            var playerCards = ActiveGameViewModel.Current.UserPlayer.Cards;
            foreach (var playerCard in playerCards) {
                AvailableCards.Add(playerCard);
            }
        }

        private void Clear() {
            SelectedCard = null;
            Status = "Select a card";
            State = TradeState.Idle;
            TradeOffers.Clear();
            AvailableCards.Clear();
            ControlsEnabled = true;
        }

        private void ClickedOfferedCard(object obj) {
            if (!ActiveGameViewModel.Current.HasMove) return;
            var dataContext = (obj as PictureViewer)?.DataContext;
            if (dataContext is TradeOffer) {
                var tradeOffer = (TradeOffer)dataContext;
                
                var offeredCard = tradeOffer.OfferedCard;
                offeredCard.IsRevealed = true;

                ConnectionHandler.GetInstance().SendTrade(TradeMessageType.AcceptTrade, offeredCard.Id);
                ConnectionHandler.GetInstance().SendSwap(SelectedCard.Id, offeredCard.Id);
            }

            OnClose();
        }

        private void Offer() {
            if (SelectedCard == null) return;
            if (State == TradeState.ReceivedOffer) {
                ConnectionHandler.GetInstance().SendTrade(TradeMessageType.OfferResponse, SelectedCard.Id);
                Status = "Waiting for response...";
                ControlsEnabled = false;
                return;
            }
            ConnectionHandler.GetInstance().SendTrade(TradeMessageType.Offer, SelectedCard.Id);
            Status = "Waiting for responses...";
            State = TradeState.WaitingForResponse;
            ControlsEnabled = false;
        }

        private void OnCardClick(object obj) {
            SelectedCard = (obj as PictureViewer)?.DataContext as GameCard;
        }

        public void HandleMessage(TradeMessage message) {
            // ignore messages that came from ourself
            if (message.Username == ConnectionPanel.Username) return;
            if (TradeOffers.Any(x => x.Username == message.Username))
                TradeOffers.Remove(TradeOffers.First(x => x.Username == message.Username));

            switch (message.Type) {
                case TradeMessageType.Offer:
                    if (State == TradeState.Idle) {
                        ActiveGameViewModel.Current.IsTrading = true;
                        OnOpen();
                        var card = GameCard.GetCardById(message.CardId);
                        card.IsRevealed = true;
                        TradeOffers.Add(new TradeOffer(message.Username, card));
                        State = TradeState.ReceivedOffer;
                        Status = message.Username + " offered a trade";
                    }
                    break;
                case TradeMessageType.OfferResponse:
                    if (State == TradeState.WaitingForResponse && ActiveGameViewModel.Current.HasMove)
                    {
                        var card = GameCard.GetCardById(message.CardId);
                        card.IsRevealed = true;
                        TradeOffers.Add(new TradeOffer(message.Username, card));
                    }
                    break;
                case TradeMessageType.AcceptTrade:
                    if (State == TradeState.ReceivedOffer) {
                        if (SelectedCard != null && message.CardId == SelectedCard.Id) {
                            GameLogger.GetInstance().Log(message.Username + " accepted your offer.");
                        } else {
                            GameLogger.GetInstance().Log("The trade was accepted for another player.");
                        }
                        OnClose();
                    }
                    break;
                case TradeMessageType.RefuseTrade:
                    if (State == TradeState.WaitingForResponse) {
                        TradeOffers.Add(new TradeOffer(message.Username));
                    }
                    break;
                case TradeMessageType.CancelOffer:
                    if (State != TradeState.Idle) {
                        GameLogger.GetInstance().Log(message.Username + " canceled the trade.");
                        OnClose();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }



        }

    }
}