﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card043DesktopLackner : UpgradeCard
    {
        public Card043DesktopLackner(int id, string owner) : base(42, id, owner)
        {
            Rarity = 1;
            Coolness = 2;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // the player who played this card has to skip next round
            forPlayer.TurnSkips += 1;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
