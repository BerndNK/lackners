﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card067KillerLackner : RareLacknerCard
    {
        public Card067KillerLackner(int id, string owner) : base(67, id, owner)
        {
            Rarity = 3;
            Coolness = 4;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.CanSacrifice = true;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            GameLogger.GetInstance().Log($"Killer - Lackner gekillt! {forPlayer.Name} kann keine Karten mehr opfern!");
            forPlayer.CanSacrifice = false;
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
