﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card092AlchemistLackner : RareLacknerCard
    {
        public Card092AlchemistLackner(int id, string owner) : base(92, id, owner)
        {
            Rarity = 2;
            Coolness = 6;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.CanPlayRareLacknerInSpellPhase = true;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
        
        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            forPlayer.CanPlayRareLacknerInSpellPhase = false;
            GameLogger.GetInstance().Log($"Alchemist - Lackner zerstört. {forPlayer.Name} kann keine RareLackner mehr anstatt einer Zauberkarte spielen.");
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
