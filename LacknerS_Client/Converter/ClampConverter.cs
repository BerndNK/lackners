﻿using System;
using System.Windows.Data;

namespace ASSM.shared.Converter.Arithmetic {
    /// <summary>
    /// Takes a value and clamps it into a given range
    /// </summary>
    public class ClampConverter : IValueConverter{
        public double Min { get; set; }
        public double Max { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            var min = GetAsDouble(parameter, Min);
            var max = GetAsDouble(parameter, Max);

            var x = GetAsDouble(value, 0.0);

            if (x < min) return min;
            if (x > max) return max;
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            // impossible to convert back
            return value;
        }

        #endregion

        private static double GetAsDouble(object parameter, double defaultValue) {
            double a;
            if (parameter != null)
                try {
                    a = System.Convert.ToDouble(parameter);
                } catch {
                    a = defaultValue;
                } else
                a = defaultValue;
            return a;
        }
    }
}
