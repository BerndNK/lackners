﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Core.Network;

namespace LacknerS_Client.ViewModel {
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ConnectionFrameViewModel : ViewModelBase {

        /// <summary>
        /// Gets or sets the ConnectionHandler.
        /// </summary>
        public ConnectionHandler ConnectionHandler {
            get { return _connectionHandler; }
            set { Set(ref _connectionHandler, value); }
        }

        private ConnectionHandler _connectionHandler;
        

        /// <summary>
        /// Initializes a new instance of the ConnectionFrameViewModel class.
        /// </summary>
        public ConnectionFrameViewModel() {
            ConnectionHandler = new ConnectionHandler(ConnectionHandler.ConnectionMode.Socket);
        }
        
    }
}