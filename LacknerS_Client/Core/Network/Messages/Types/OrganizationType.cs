﻿using System;

namespace LacknerS_Client.Core.Network.Messages.Types {
    [Serializable]
    public enum OrganizationType {
        HostHelloRequest, // message sent from host, to invoke clients to register themselves
        ClientHostHelloResponse, // message sent from client to register itself
        HostHelloResponseConfirmation, // message sent from host to confirm registration
        StartGame, // sent from host, that the game is now starting
        RegisterOpponent, // sent from host, that a client shall register an opponent
        
        SetActivePlayer, // sent from the host to tell which player is taking action
        DrawGreenCard, // draws a green card for the given player
        DrawYellowCard, // draws a yellow/blue card for the given player
        EndTurn,
        PlayLastDrawnCard,
        CloseGame,
        WinGame
    }
}