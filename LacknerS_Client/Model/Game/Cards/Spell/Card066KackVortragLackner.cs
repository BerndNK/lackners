﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card066KackVortragLackner : SpellCard
    {
        public Card066KackVortragLackner(int id, string owner) : base(66, id, owner)
        {
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var card = activeGame.Graveyard.Last();
            activeGame.Graveyard.Remove(card);
            forPlayer.Cards.Add(card);
            card.IsRevealed = activeGame.UserPlayer.Name == forPlayer.Name;

            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                card.ClickedCommmand = new RelayCommand<object>(x => GameViewModel.Current.PlayerCardOnClick(x));
            }
            else
            {
                card.ClickedCommmand = new RelayCommand<object>(x => GameViewModel.Current.OpponentCardOnClick(x));
            }


            GameLogger.GetInstance().Log($"{Name} belebt die Toten für einen kack Vortrag.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override bool IsSpecialRequirementFulfilled(ActiveGameViewModel activeGame)
        {
            return activeGame.Graveyard.Count > 0;
        }
    }
}
