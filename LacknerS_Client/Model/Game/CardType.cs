﻿using System;

namespace LacknerS_Client.Model.Game {
    [Flags]
    public enum CardType {
        Upgrade = 1,
        RareLackner = 2,
        Spell = 4, 
        Special = 8,
        AllPlayable = Upgrade | RareLackner | Spell,
        All = Upgrade | RareLackner | Spell | Special
    }
}