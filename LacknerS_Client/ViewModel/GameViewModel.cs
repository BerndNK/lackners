﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Converters;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Controls;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Encryption;
using LacknerS_Client.Core.Network.Messages;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.Model;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Game.Cards;
using LacknerS_Client.Views;
using MahApps.Metro;

namespace LacknerS_Client.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class GameViewModel : ViewModelBase
    {
        /// <summary>
        /// Gets or sets the current state of this GameViewModel
        /// </summary>
        public GameState GameState {
            get { return _gameState; }
            set {
                Set(ref _gameState, value);
                if (SelectionPanel.SelectedMode == ClientMode.Game)
                {
                    DebugLogger.GetInstance().LogInfo("Send new game state: " + value);
                    if (ConnectionHandler.GetInstance().ConnectionStatus == ConnectionHandler.ConnectionStatusEnum.Connected)
                        ConnectionHandler.GetInstance().Send(value);
                }
            }
        }

        /// <summary>
        /// Gets the active game view model.
        /// </summary>
        /// <value>
        /// The active game view model.
        /// </value>
        private ActiveGameViewModel ActiveGameViewModel => (ActiveGameViewModel)_activeGameWindow?.DataContext;

        public static GameViewModel Current;

        private GameState _gameState;
        /// <summary>
        /// The active game window
        /// </summary>
        private ActiveGame _activeGameWindow;

        public ICommand StartCommand { get; private set; }



        /// <summary>
        /// Initializes a new instance of the GameViewModel class.
        /// </summary>
        public GameViewModel()
        {
            StartCommand = new RelayCommand(Start);
            ConnectionHandler.GetInstance().PropertyChanged += ServerConnectorOnPropertyChanged;
            ConnectionHandler.GetInstance().MessageReceived += ServerConnectorOnMessageReceived;
            Current = this;
        }


        private void Start()
        {
            if (ConnectionHandler.GetInstance().ConnectionStatus != ConnectionHandler.ConnectionStatusEnum.Connected)
                return;
            //InWaitingState = false;
        }

        private void ServerConnectorOnMessageReceived(object sender, ReceiveMessageEventArgs e)
        {
            // abort and remove listener if the user has selected the client to be in host mode (since this class is for game mode)
            if (SelectionPanel.SelectedMode == ClientMode.Host)
            {
                ConnectionHandler.GetInstance().PropertyChanged -= ServerConnectorOnPropertyChanged;
                ConnectionHandler.GetInstance().MessageReceived -= ServerConnectorOnMessageReceived;
                return;
            }
            if (SelectionPanel.SelectedMode == ClientMode.Waiting) return; // ignore all messages as long as the user has not selected a mode yet

            var msg = e.Message;

            if (msg is TextMessage)
            {
                HandleTextMessage(msg as TextMessage);
            }

            if (msg is OrganizationMessage)
            {
                HandleOrganizationMessage(msg as OrganizationMessage);
            }

            if (msg is DealCardMessage)
            {
                HandleDealCardMessage(msg as DealCardMessage);
            }

            if (msg is PlayCardMessage)
            {
                HandlePlayCardMessage(msg as PlayCardMessage);
            }

            if (msg is TradeMessage)
            {
                HandleTradeMessage(msg as TradeMessage);
            }

            if (msg is SwapCardMessage)
            {
                HandleSwapMessage(msg as SwapCardMessage);
            }
        }

        private void HandleSwapMessage(SwapCardMessage swapCardMessage)
        {
            ActiveGameViewModel.SwapCards(swapCardMessage.FirstCard, swapCardMessage.SecondCard, this);
        }

        private void HandleTradeMessage(TradeMessage tradeMessage)
        {
            TradeView.Current.HandleMessage(tradeMessage);
        }

        private void HandlePlayCardMessage(PlayCardMessage playCardMessage)
        {
            ActiveGameViewModel.ActivateCard(playCardMessage);
        }

        private void HandleDealCardMessage(DealCardMessage dealCardMessage)
        {
            if (_activeGameWindow == null) return;
            DebugLogger.GetInstance().LogInfo("Received deal card event for player: " + dealCardMessage.ForPlayer);
            var activeGameVm = ActiveGameViewModel;

            var newCard = GameCardFactory.Produce(dealCardMessage.Type, dealCardMessage.Id, dealCardMessage.ForPlayer);

            if (dealCardMessage.ForPlayer == ConnectionPanel.Username)
            {
                // the card is for this player
                newCard.ClickedCommmand = new RelayCommand<object>(PlayerCardOnClick);
                activeGameVm.AddCard(newCard);
            }
            else
            {
                // the card is for an opponent
                var opp = activeGameVm.Opponents.First(x => x.Name == dealCardMessage.ForPlayer);
                newCard.ClickedCommmand = new RelayCommand<object>(OpponentCardOnClick);
                opp.AddCard(newCard);
            }
        }

        /// <summary>
        /// The user clicked on one of it's own cards
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void PlayerCardOnClick(object sender)
        {
            if (GameState != GameState.HasMove) return;
            if (sender is GameCard) ActiveGameViewModel.PlayerCardOnClick(sender as GameCard);
            else
            {
                var card = (sender as PictureViewer)?.DataContext as GameCard;
                if (card == null) return;
                ActiveGameViewModel.PlayerCardOnClick(card);
            }
        }

        /// <summary>
        /// The user clicked on a card of one of the opponents
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        public void OpponentCardOnClick(object sender)
        {
            if (GameState != GameState.HasMove) return;
            if (sender is GameCard) ActiveGameViewModel.OpponentCardOnClick(sender as GameCard);
            else
            {
                var card = (sender as PictureViewer)?.DataContext as GameCard;

                if (card == null) return;
                ActiveGameViewModel.OpponentCardOnClick(card);
            }
        }

        private void HandleOrganizationMessage(OrganizationMessage organizationMessage)
        {
            switch (organizationMessage.Type)
            {
                case OrganizationType.HostHelloRequest:
                    // host requested hello. Send response, IF we were waiting for a request
                    if (GameState != GameState.WaitingForHostRequest) return;

                    DebugLogger.GetInstance().LogInfo("Received hello poll from host. Sending registration request for " + ConnectionPanel.Username);
                    ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.ClientHostHelloResponse);
                    GameState = GameState.SentResponse;
                    break;
                case OrganizationType.ClientHostHelloResponse:
                    // ignored
                    break;
                case OrganizationType.HostHelloResponseConfirmation:
                    if (GameState == GameState.GameStarted) return;
                    // host has confirmed hello. Check if the confirmation is for us (same user name)
                    DebugLogger.GetInstance().LogInfo("Confirmation received for user \"" + organizationMessage.User + "\"");
                    DebugLogger.GetInstance().LogInfo("Active user is: " + ConnectionPanel.Username);
                    if (organizationMessage.User == ConnectionPanel.Username)
                    {
                        GameState = GameState.HostHasConfirmed;
                    }
                    break;
                case OrganizationType.StartGame:
                    if (GameState == GameState.GameStarted) return;
                    _activeGameWindow = GameView.CurrentActiveGame;
                    _activeGameWindow.Visibility = Visibility.Visible;
                    GameState = GameState.GameStarted;
                    break;
                case OrganizationType.CloseGame:
                    GameView.CurrentActiveGame.DataContext = new ActiveGameViewModel(new DataService());
                    _activeGameWindow.Visibility = Visibility.Collapsed;

                    if (GameState != GameState.Won && GameState != GameState.Lost)
                        GameState = GameState.GameClosed;
                    break;
                case OrganizationType.RegisterOpponent:
                    if (organizationMessage.User != ConnectionPanel.Username)
                    {
                        DebugLogger.GetInstance().LogInfo("Received register opponent message for player: " + organizationMessage.User);
                        ActiveGameViewModel?.RegisterOpponent(organizationMessage.User);
                    }
                    break;
                case OrganizationType.SetActivePlayer:
                    foreach (var currentOpponent in ActiveGameViewModel.Current.Opponents)
                    {
                        currentOpponent.HasTurn = false;
                    }
                    ActiveGameViewModel.Current.UserPlayer.HasTurn = false;

                    if (organizationMessage.User != ConnectionPanel.Username)
                    {
                        GameState = GameState.WaitingForOtherPlayer;
                        ActiveGameViewModel.HasMove = false;
                        var opponent = ActiveGameViewModel.Current.Opponents.First(x => x.Name == organizationMessage.User);
                        ActiveGameViewModel.ReceiveTurn(opponent);
                    }
                    else
                    {
                        GameState = GameState.HasMove; // set that this player currently has the move
                        ActiveGameViewModel.ReceiveTurn();
                    }
                    ActiveGameViewModel.UpdateClickableCards();
                    break;
                case OrganizationType.WinGame:
                    if (ConnectionPanel.Username == organizationMessage.User)
                    {
                        GameState = GameState.Won;
                    }
                    else
                    {
                        GameState = GameState.Lost;
                    }
                    break;
                case OrganizationType.EndTurn:
                case OrganizationType.DrawYellowCard:
                case OrganizationType.DrawGreenCard:
                case OrganizationType.PlayLastDrawnCard:
                    // ignored
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }



        private void HandleTextMessage(TextMessage msg)
        {
            var text = Crypto.Decrypt(msg.Text, SettingsWindow.GetSetting("cryptopassword").ToString());
            DebugLogger.GetInstance().LogInfo(text);
        }

        private void ServerConnectorOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // abort and remove listener if the user has selected the client to be in host mode (since this class is for game mode)
            if (SelectionPanel.SelectedMode == ClientMode.Host)
            {
                ConnectionHandler.GetInstance().PropertyChanged -= ServerConnectorOnPropertyChanged;
                ConnectionHandler.GetInstance().MessageReceived -= ServerConnectorOnMessageReceived;
                return;
            }
            if (e.PropertyName != "ConnectionStatus") return;

            switch (ConnectionHandler.GetInstance().ConnectionStatus)
            {
                case ConnectionHandler.ConnectionStatusEnum.Disconnected:
                case ConnectionHandler.ConnectionStatusEnum.Connecting:
                    break;
                case ConnectionHandler.ConnectionStatusEnum.Connected:
                    ConnectionHandler.GetInstance().StartListener();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}