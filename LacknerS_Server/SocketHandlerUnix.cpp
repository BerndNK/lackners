#include "SocketHandlerUnix.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

network::SocketHandlerUnix::SocketHandlerUnix(std::string port) : SocketHandler(port) {
		
}

network::SocketHandlerUnix::~SocketHandlerUnix() {
	if (!m_isInitalized) return;
	close(sockfd);
}

int network::SocketHandlerUnix::Initialize() {
	socklen_t clilen;
        struct sockaddr_in serv_addr;
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) {
                perror("ERROR opening socket");
                return 1;
        }

        bzero((char*) &serv_addr, sizeof(serv_addr));
        int portno = atoi(m_port.c_str());
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if(bind(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("ERROR on binding");
		return 1;
	}
	
	listen(sockfd, 5);
	clilen = sizeof(cli_addr);
	m_isInitalized  = true;
	return 0;
}

void network::SocketHandlerUnix::Listener(bool* waitingForClientBool, Client* thisClientPtr) {
	m_coutMutex.lock();
	std::cout << "Client: " << thisClientPtr->No << " - Wait for connect" << std::endl;
	m_coutMutex.unlock();

	
	auto clientSocket = accept(sockfd, (struct sockaddr*) &cli_addr, &clilen);
	*waitingForClientBool = false;
	if (clientSocket < 0) {
		return;
	}
	
	// start sender
	auto forceQuitSenderThread = false;
	std::thread senderThread(&SocketHandlerUnix::Sender, this, thisClientPtr, &clientSocket, &forceQuitSenderThread);
	thisClientPtr->IsConnected = true;
	
	// listen
	int iResult;
	char recvbuf[DEFAULT_BUFLEN];
	auto recbuflen = DEFAULT_BUFLEN;

	// Receive until the peer shuts down the connection
	
	m_coutMutex.lock();
	std::cout << "Client: " << thisClientPtr->No << " - Listening..." << std::endl;
	m_coutMutex.unlock();
	
	do {
		iResult = read(clientSocket, recvbuf, recbuflen);
		if (iResult > 0) {
			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Bytes received:" << iResult << std::endl;
			m_coutMutex.unlock();

			// Echo the buffer back to the sender
			QueueMessage(thisClientPtr, recvbuf, iResult);
		} else if (iResult == 0) {
			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Connection closing..." << std::endl;
			m_coutMutex.unlock();

			forceQuitSenderThread = true;
			senderThread.join();
			close(clientSocket);

			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Terminated" << std::endl;
			m_coutMutex.unlock();
			RemoveClient(thisClientPtr);
			return;
		} else {
			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Recv failed with error: " << iResult << std::endl;
			m_coutMutex.unlock();

			forceQuitSenderThread = true;
			senderThread.join();
			close(clientSocket);
			RemoveClient(thisClientPtr);
			return;
		}

	} while(iResult > 0);

	// shutdown the connection since we're done
	close(clientSocket);
}

void network::SocketHandlerUnix::Sender(Client* thisClientPtr, void* socket, bool* forceQuitBool) {
	while (!*forceQuitBool) {
		sleep(0.5);
		int* clientSocket = nullptr;
		try {
			clientSocket = reinterpret_cast<int*>(socket);
		} catch (...) {
			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Failed to cast socket into int" << std::endl;
			m_coutMutex.unlock();
			return;
		}
		thisClientPtr->ListMutex.lock();
		while (thisClientPtr->ToPushList.size() != 0) {
			// get first element
			auto toSend = thisClientPtr->ToPushList[0];
			//unsigned char(*asArr)[DEFAULT_BUFLEN] = reinterpret_cast<unsigned char(*)[DEFAULT_BUFLEN]>(toSend.Array);
			auto iSendResult = write(*clientSocket, toSend.Array, toSend.Length);

			// remove first element
			delete[] toSend.Array;
			thisClientPtr->ToPushList.erase(thisClientPtr->ToPushList.begin(), thisClientPtr->ToPushList.begin() + 1);

			if (iSendResult < 0) {
				m_coutMutex.lock();
				std::cout << "Client: " << thisClientPtr->No << " - Send failed. Code: \n" << iSendResult << std::endl;
				m_coutMutex.unlock();
			} else {
				m_coutMutex.lock();
				std::cout << "Client: " << thisClientPtr->No << " - Sent: " << iSendResult << std::endl;
				m_coutMutex.unlock();
			}
		}
		thisClientPtr->ListMutex.unlock();
	}
}


