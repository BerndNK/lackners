﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card013KaffeeLackner : UpgradeCard{
        public Card013KaffeeLackner(int id, string owner) : base(13, id, owner)
        {
            Rarity = -2;
            Coolness = 0;
        }
    }
}
