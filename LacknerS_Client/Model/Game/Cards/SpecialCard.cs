﻿using System;
using System.Collections.Generic;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards
{
    public class SpecialCard : GameCard
    {
        public SpecialCard(int no, int id, string owner) : base(no, CardType.Special, id, owner)
        {
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            throw new NotImplementedException();
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            throw new NotImplementedException();
        }
    }
}
