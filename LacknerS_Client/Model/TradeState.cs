﻿namespace LacknerS_Client.Model
{
    public enum TradeState
    {
        Idle,
        WaitingForResponse,
        ReceivedOffer
    }
}