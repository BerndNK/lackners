using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using LacknerS_Client.Views;

namespace LacknerS_Client.Core.Network.Messages {
    [Serializable]
    public abstract class Message : INotifyPropertyChanged {
        private double _baseOpacity = 1;

        #region Properties

        public bool IsSentByUser {
            get { return Username == ConnectionPanel.Username; }
        }

        public string Username { get; set; }

        public HorizontalAlignment HorizontalAlignment {
            get { return IsSentByUser ? HorizontalAlignment.Right : HorizontalAlignment.Left; }
        }

        public SolidColorBrush Background {
            get {
                if (IsSentByUser) return (SolidColorBrush)Application.Current.FindResource("AccentColorBrush");
                else return (SolidColorBrush)Application.Current.FindResource("AccentColorBrush3");
            }
        }

        protected double BaseOpacity {
            get { return _baseOpacity; }
            set { _baseOpacity = value; }
        }

        public double BackgroundOpacity {
            get { return IsSentByUser ? BaseOpacity * 0.5 : BaseOpacity; }
        }


        public Dock PlaceholderDocking {
            get { return IsSentByUser ? System.Windows.Controls.Dock.Left : System.Windows.Controls.Dock.Right; }
        }

        public Visibility NameVisibility {
            get { return IsSentByUser ? Visibility.Collapsed : Visibility.Visible; }
        }

        #endregion

        public static Message FromContainer(MessageContainer container) {
            return GetMessage(container.Data);
        }

        private static Message GetMessage(byte[] bytes) {
            var formatter = new BinaryFormatter();
            Stream stream = new MemoryStream(bytes);
            return formatter.Deserialize(stream) as Message;
        }

        public static byte[] GetBytes(Message data) {
            var m = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(m, data);
            return m.ToArray();
        }

        protected Message() {
            TimeStamp = DateTime.Now;
            Username = ConnectionPanel.Username;
        }

        public DateTime TimeStamp { get; set; }

        #region INotifyPropertyChangedImplementation
        protected void CallPropertyChanged(string on) {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}