﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card083DateLackner : UpgradeCard
    {
        public Card083DateLackner(int id, string owner) : base(83, id, owner)
        {
            Rarity = 2;
            Coolness = 0;
        }
    }
}
