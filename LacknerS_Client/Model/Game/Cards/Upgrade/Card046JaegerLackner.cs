﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card046JaegerLackner : UpgradeCard
    {
        public Card046JaegerLackner(int id, string owner) : base(46, id, owner)
        {
            Rarity = 0;
            Coolness = 4;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.TurnSkips++;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
