﻿using System;
using LacknerS_Client.Core.Network.Messages;

namespace LacknerS_Client.Core.Network {
    public class ReceiveMessageEventArgs : EventArgs {
        #region Properties
        public Message Message { get; private set; }
        #endregion

        public ReceiveMessageEventArgs(Message message) {
            Message = message;
        }
    }
}
