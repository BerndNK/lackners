﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card080KonzertLackner : RareLacknerCard
    {
        public Card080KonzertLackner(int id, string owner) : base(80, id, owner)
        {
            Rarity = 5;
            Coolness = 5;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // each spell costs 1 more coolness for every player
            activeGame.UserPlayer.CoolnessNeededModifier += 1;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            GameLogger.GetInstance().Log($"Konzert - Lackner zerstört. Jeder Zauber kostet wieder 1 Coolness weniger.");
            activeGame.UserPlayer.CoolnessNeededModifier -= 1;
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
