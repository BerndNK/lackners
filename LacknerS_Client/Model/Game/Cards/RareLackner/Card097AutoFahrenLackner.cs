﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card097AutoFahrenLackner : RareLacknerCard
    {
        public Card097AutoFahrenLackner(int id, string owner) : base(97, id, owner)
        {
            Rarity = 2;
            Coolness = 5;

        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.FreeOvercharges += 1;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
