﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card023IstGespanntLackner : UpgradeCard
    {
        public Card023IstGespanntLackner(int id, string owner) : base(23, id, owner)
        {
            Rarity = 0;
            Coolness = 1;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var rareLackner = GetCardById(selectionTargets.First().SelectedCard) as RareLacknerCard;
            if (rareLackner == null) return;

            forPlayer.DrawGreenCardNextTurn = true;

            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
