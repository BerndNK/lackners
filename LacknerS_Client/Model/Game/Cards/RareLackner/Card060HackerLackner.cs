﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card060HackerLackner : RareLacknerCard
    {
        public Card060HackerLackner(int id, string owner) : base(60, id, owner)
        {
            Rarity = 3;
            Coolness = 4;
            FreeUpgrades = 1;
        }
    }
}
