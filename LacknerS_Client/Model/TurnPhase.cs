namespace LacknerS_Client.Model
{
    public enum TurnPhase
    {
        UpgradePhase = 1,
        SpellPhase = 2,
        TurnOver = 3
    }
}