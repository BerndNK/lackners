﻿namespace LacknerS_Client.Core.Network.Messages {
    public class ErrorMessage : Message {
        #region Fields
        private string _text;
        #endregion

        #region Properties
        public string Text {
            get { return _text; }
            set {
                _text = value; 
                CallPropertyChanged("Text");
            }
        }

        #endregion

        public ErrorMessage(string text) {
            Text = text;
        }
    }
}
