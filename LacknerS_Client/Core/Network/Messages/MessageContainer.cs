﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace LacknerS_Client.Core.Network.Messages {
    [Serializable]
    public class MessageContainer {
        public const int CurrentVersion = 1;

        #region Enums
        public enum MessageType {
            Text = 0,
            Picture = 1,
            File = 2,
            Organization,
            GameState,
            DealCard,
            PlayCard,
            Trade,
            SwapCard
        }
        #endregion

        #region Properties
        public int Version { get; private set; }
        private MessageType Type { get; set; }
        public byte[] Data { get; private set; }
        #endregion

        public MessageContainer(MessageType type, byte[] data) {
            Type = type;
            Version = CurrentVersion;
            Data = data;
        }

        public static byte[] GetBytes(MessageContainer container) {
            var m = new MemoryStream();
            var formatter = new BinaryFormatter();
            formatter.Serialize(m, container);
            return m.ToArray();
        }

        public static MessageContainer GetContainer(byte[] bytes) {
            var formatter = new BinaryFormatter();
            Stream stream = new MemoryStream(bytes);
            return formatter.Deserialize(stream) as MessageContainer;
        }

        public override string ToString() {
            return "Version: " + Version + " Type: " + Type.ToString();

        }
    }
}
