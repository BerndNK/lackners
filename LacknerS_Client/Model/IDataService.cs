﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Data;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Host;

namespace LacknerS_Client.Model {
    public interface IDataService {
        void GetData(Action<DataItem, Exception> callback);

        void GetRegisteredClients(Action<BindingList<RegisteredClient>, Exception> callback);


        void GetOpponents(Action<BindingList<Player>, Exception> callback);

        void GetPlayerCards(Action<BindingList<GameCard>, Exception> callback);
    }
}
