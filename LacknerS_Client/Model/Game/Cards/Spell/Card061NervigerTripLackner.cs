﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card061NervigerTripLackner : SpellCard
    {
        public Card061NervigerTripLackner(int id, string owner) : base(61, id, owner)
        {
            Power = SpellCardPower.Middle;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            // needs a rareLackner with a combined coolness of 6 or lower and which HAS at least one upgrade.
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.All)};
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var targetCard = GetCardById(selectionTargets.First().SelectedCard) as RareLacknerCard;
            if (targetCard == null) throw new Exception("Could not find card");
            targetCard.MaxUpgradeCount = 0;

            GameLogger.GetInstance().Log($"{Name} versetzt {targetCard?.Name} auf einen nervigen Trip und blockiert weitere Upgrades.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
