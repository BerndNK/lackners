﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SimpleTalkClient.Shared.Utils {
    public static class ColorManipulator {
        public static ImageSource ChangeColor(ImageSource of, Color toColor) {
            // Copy pixel colour values from existing image.
            // (This loads them from an embedded resource. BitmapDecoder can work with any Stream, though.)
            var image = (BitmapFrame)of;

            var pixels = new byte[image.PixelWidth * image.PixelHeight * 4];
            image.CopyPixels(pixels, image.PixelWidth * 4, 0);

            // Modify the white pixels
            for (var i = 0; i < pixels.Length / 4; ++i) {
                var b = pixels[i * 4];
                var g = pixels[i * 4 + 1];
                var r = pixels[i * 4 + 2];
                var a = pixels[i * 4 + 3];

                // if the pixel has alpha 0 or is not exactly white, skip this pixel
                if (a == 0) continue;

                // set the color
                /* r = toColor.R;
                 g = toColor.G;
                 b = toColor.B;
                 var alphaMult = a / 255.0;
                 pixels[i * 4] = (byte)(b * alphaMult);
                 pixels[i * 4 + 1] = (byte)(g * alphaMult);
                 pixels[i * 4 + 2] = (byte)(r * alphaMult);
                 pixels[i * 4 + 3] *= (byte)(toColor.A / 255.0);*/
                var toColorArr = new[] {toColor.B, toColor.G, toColor.R, toColor.A};
                for (var j = 0; j < 4; j++) {
                    var sourceDouble = Convert.ToDouble(pixels[i * 4 + j]);
                    var targetDouble = Convert.ToDouble(toColorArr[j]);
                    var result = (byte)(sourceDouble / 255 * targetDouble);
                    pixels[i*4 + j] = Convert.ToByte(result);
                }
            }

            // Write the modified pixels into a new bitmap and use that as the source of an Image
            var bmp = new WriteableBitmap(image.PixelWidth, image.PixelHeight, image.DpiX, image.DpiY, PixelFormats.Pbgra32, null);
            bmp.WritePixels(new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight), pixels, image.PixelWidth * 4, 0);
            return ConvertWriteableBitmapToBitmapImage(bmp);

        }

        private static BitmapImage ConvertWriteableBitmapToBitmapImage(WriteableBitmap wbm) {
            var bmImage = new BitmapImage();
            using (var stream = new MemoryStream()) {
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(wbm));
                encoder.Save(stream);
                bmImage.BeginInit();
                bmImage.CacheOption = BitmapCacheOption.OnLoad;
                bmImage.StreamSource = stream;
                bmImage.EndInit();
                bmImage.Freeze();
            }
            return bmImage;
        }

        public static ImageSource ToWhite(ImageSource baseImage) {
            // Copy pixel colour values from existing image.
            // (This loads them from an embedded resource. BitmapDecoder can work with any Stream, though.)
            var image = (BitmapFrame)baseImage;

            var pixels = new byte[image.PixelWidth * image.PixelHeight * 4];
            image.CopyPixels(pixels, image.PixelWidth * 4, 0);

            // Modify the white pixels
            for (var i = 0; i < pixels.Length / 4; ++i) {
                // make the entire image white
                pixels[i * 4] = 255;
                pixels[i * 4 + 1] = 255;
                pixels[i * 4 + 2] = 255;
            }

            // Write the modified pixels into a new bitmap and use that as the source of an Image
            var bmp = new WriteableBitmap(image.PixelWidth, image.PixelHeight, image.DpiX, image.DpiY, PixelFormats.Pbgra32, null);
            bmp.WritePixels(new Int32Rect(0, 0, image.PixelWidth, image.PixelHeight), pixels, image.PixelWidth * 4, 0);
            return ConvertWriteableBitmapToBitmapImage(bmp);
        }
    }
}
