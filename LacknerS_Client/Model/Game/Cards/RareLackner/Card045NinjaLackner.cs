﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card045NinjaLackner : RareLacknerCard
    {
        public Card045NinjaLackner(int id, string owner) : base(45, id, owner)
        {
            Rarity = 5;
            Coolness = 7;
            MaxUpgradeCount = 0;
        }
    }
}
