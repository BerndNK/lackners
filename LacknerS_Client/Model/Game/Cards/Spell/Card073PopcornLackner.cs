﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card073PopcornLackner : SpellCard
    {
        public Card073PopcornLackner(int id, string owner) : base(73, id, owner)
        {
            Power = SpellCardPower.Middle;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.GraveyardCard, CardType.AllPlayable, OwnerType.None) };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var card = GetCardById(selectionTargets.First().SelectedCard);
            activeGame.Graveyard.Remove(card);
            forPlayer.Cards.Add(card);

            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                card.ClickedCommmand = new RelayCommand<object>(x => GameViewModel.Current.PlayerCardOnClick(x));
            }
            else
            {
                card.ClickedCommmand = new RelayCommand<object>(x => GameViewModel.Current.OpponentCardOnClick(x));
            }

            GameLogger.GetInstance().Log($"{Name} teilt sich Popcorn mit einem Verstorbenen.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
