﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card037BettLackner : SpellCard
    {
        public Card037BettLackner(int id, string owner) : base(37, id, owner)
        {
            Power = SpellCardPower.High;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            var selectionTarget = new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.Enemy);
            // make sure the player has at least 2 rare lackner
            selectionTarget.SpecialGameCondition =
                game => game.Opponents.Sum(y => y.PlayedCards.Select(x => x.CardType == CardType.RareLackner).Count()) >= 2;
            return new List<SelectionTarget>
            {
                selectionTarget,
                new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.Enemy)
            };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            void Remove(RareLacknerCard rareLackner)
            {
                if (rareLackner == null) return;
                foreach (var upgrade in rareLackner.Upgrades)
                {
                    activeGame.Graveyard.Add(upgrade);
                }
                rareLackner.Upgrades.Clear();

                var owner = GetOwner(rareLackner, activeGame);
                owner.PlayedRareLackner.Remove(rareLackner);
                owner.PlayedCards.Remove(rareLackner);
            }

            Remove(GetCardById(selectionTargets.First().SelectedCard) as RareLacknerCard);
            Remove(GetCardById(selectionTargets.Last().SelectedCard) as RareLacknerCard);
            
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
