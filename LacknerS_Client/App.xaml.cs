﻿using System;
using System.Windows;
using GalaSoft.MvvmLight.Threading;

namespace LacknerS_Client {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        static App() {
            DispatcherHelper.Initialize();
        }
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            /* MainWindow wnd = new MainWindow();
             if (e.Args.Length == 1)
                 MessageBox.Show("Now opening file: \n\n" + e.Args[0]);
             wnd.Show();*/
            LacknerS_Client.MainWindow.WindowX = e.Args.Length > 0 ? Convert.ToInt32(e.Args[0]) : 0;
            LacknerS_Client.MainWindow.WindowY = e.Args.Length > 1 ? Convert.ToInt32(e.Args[1]) : 0;
            LacknerS_Client.MainWindow.WindowW = e.Args.Length > 2 ? Convert.ToInt32(e.Args[2]) : 600;
            LacknerS_Client.MainWindow.WindowH = e.Args.Length > 3 ? Convert.ToInt32(e.Args[3]) : 400;
        }
    }
}
