﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.Model;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Game.Cards;
using LacknerS_Client.Model.Game.Cards.RareLackner;
using LacknerS_Client.Model.Game.Cards.Spell;
using LacknerS_Client.Model.Game.Cards.Upgrade;
using LacknerS_Client.Views;

namespace LacknerS_Client.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ActiveGameViewModel : ViewModelBase
    {
        public static ActiveGameViewModel Current;

        /// <summary>
        /// Gets or sets the player of this client instance
        /// </summary>
        public Player UserPlayer {
            get { return _userPlayer; }
            set { Set(ref _userPlayer, value); }
        }

        /// <summary>
        /// Gets or sets the opponents
        /// </summary>
        public BindingList<Player> Opponents {
            get { return _opponents; }
            set { Set(ref _opponents, value); }
        }

        /// <summary>
        /// Gets or sets the flag whether the player currently has the move or not
        /// </summary>
        public bool HasMove {
            get { return _hasMove; }
            set {
                Set(ref _hasMove, value);
                PropertyChangedHandler(this, new PropertyChangedEventArgs(nameof(AbortPossible)));
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this instance is trading.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is trading; otherwise, <c>false</c>.
        /// </value>
        public bool IsTrading {
            get { return _isTrading; }
            set {
                Set(ref _isTrading, value);
                PropertyChangedHandler(this, new PropertyChangedEventArgs(nameof(IsTradingVisibility)));
            }
        }

        public BindingList<LogMessage> ActiveMessages {
            get { return _activeMessages; }
            set { Set(ref _activeMessages, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [received trade offer].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [received trade offer]; otherwise, <c>false</c>.
        /// </value>
        public bool ReceivedTradeOffer {
            get { return _receivedTradeOffer; }
            set { Set(ref _receivedTradeOffer, value); }
        }

        public List<SelectionTarget> SelectionTargets => _currentNeededSelections;
        public int AutoPlayNextCards { get; set; }

        public ICommand EndTurnCommand { get; set; }
        public ICommand TradeCommand { get; set; }
        public ICommand EndTradeCommand { get; set; }
        public ICommand EndTradeOfferCommand { get; set; }

        public ICommand DrawGreenCardCommand { get; set; }

        public ICommand RefreshClickablesCommand { get; set; }

        public Visibility IsTradingVisibility => IsTrading ? Visibility.Visible : Visibility.Collapsed;

        /// <summary>
        /// Gets a list of all player.
        /// </summary>
        public List<Player> AllPlayer {
            get {
                var toReturn = new List<Player> { UserPlayer };
                toReturn.AddRange(Opponents);
                return toReturn;
            }
        }



        private bool _hasMove;
        private BindingList<Player> _opponents;
        private Player _userPlayer;

        private List<SelectionTarget> _currentNeededSelections;
        private GameCard _currentPlayedCard;
        private int _currentSelectionIndex;
        private bool _receivedTradeOffer;
        private bool _isTrading;
        private BindingList<LogMessage> _activeMessages;
        private bool _playNextCardForFree;
        private bool _graveyardVisibility;

        public ObservableCollection<GameCard> Graveyard { get; set; }

        public bool GraveyardVisibility {
            get { return _graveyardVisibility; }
            set { Set(ref _graveyardVisibility, value); }
        }

        public bool InDebug {
            get {
                var debug = false;
#if DEBUG
                debug = true;
#endif
                return debug;
            }
        }

        public bool CanDrawGreenCard => HasMove && (UserPlayer.IsSuperCharged || UserPlayer.PlayedRareLackner.Count == 0); // if the player has no RareLackner on the field, he may always draw a green card

        public string GraveyardText => $"Graveyard ({Graveyard.Count})";

        public ICommand RemoveUpgradeCommand { get; set; }

        public ICommand OnKeyDownCommand { get; set; }
        public ICommand AbortCommand { get; set; }

        public bool AbortPossible => HasMove && SelectionTargets != null && SelectionTargets.Count != 0;

        public ICommand SacrificeCommand { get; set; }

        public ObservableCollection<GameCard> LastPlayedCards { get; set; }

        /// <summary>
        /// Initializes a new instance of the ActiveGameViewModel class.
        /// </summary>
        public ActiveGameViewModel(IDataService dataService)
        {

            dataService.GetPlayerCards((list, exception) =>
            {
                UserPlayer = new Player(ConnectionPanel.Username, dataService) { OnClickCommand = new RelayCommand<object>(OnPlayerClick) };
            });
            dataService.GetOpponents((list, exception) =>
            {
                Opponents = list;
            });
            EndTurnCommand = new RelayCommand(EndTurn);
            TradeCommand = new RelayCommand(Trade);
            DrawGreenCardCommand = new RelayCommand(DrawGreenCard);
            RefreshClickablesCommand = new RelayCommand(UpdateClickableCards);
            RemoveUpgradeCommand = new RelayCommand(OnRemoveUpgrade);
            OnKeyDownCommand = new RelayCommand<object>(OnKeyDown);
            AbortCommand = new RelayCommand(AbortCard);
            SacrificeCommand = new RelayCommand(Sacrifice);
            ActiveMessages = new BindingList<LogMessage>();
            Graveyard = new ObservableCollection<GameCard>();
            Graveyard.CollectionChanged += GraveyardOnCollectionChanged;
            GraveyardVisibility = false;
            GameLogger.GetInstance().MessageLogged += OnMessageLogged;
            LastPlayedCards = new ObservableCollection<GameCard>();
            LastPlayedCards.CollectionChanged += LastPlayedCardsOnCollectionChanged;
            if (Current == null) Current = this;
        }

        private void LastPlayedCardsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            if (notifyCollectionChangedEventArgs.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (var newItem in notifyCollectionChangedEventArgs.NewItems)
                {
                    var remove = new Action<GameCard>(x =>
                    {
                        LastPlayedCards.Remove(x);

                    });
                    Task.Run(() =>
                    {
                        Task.Delay(3500).Wait();
                        Application.Current.Dispatcher.Invoke(() => { remove((GameCard)newItem); });
                    });
                }
            }
        }

        private void Sacrifice()
        {
            if (!UserPlayer.CanSacrifice || !HasMove) return;
            PlayCard(Card000SacrificeCard.GetInstance());
        }

        private void OnKeyDown(object obj)
        {
            var asKeyEventArgs = obj as KeyEventArgs;
            if (asKeyEventArgs == null) return;

            if (asKeyEventArgs.Key == Key.Escape)
            {
                AbortCard();
            }
        }

        private void AbortCard()
        {
            if (_currentPlayedCard == null) return;
            GameLogger.GetInstance().Log("Aborted");
            _currentPlayedCard = null;
            _currentNeededSelections = null;
            _currentSelectionIndex = 0;
            _playNextCardForFree = false;
            UpdateClickableCards();
        }

        private void OnRemoveUpgrade()
        {
            var card = Card000RemoveUpgrade.GetInstance();
            foreach (var selectionTarget in card.GetSelectionTargets(this))
            {
                if (!selectionTarget.Exists(this))
                {
                    GameLogger.GetInstance().Log("There are no Upgrades that can be removed.");
                    return;
                }
            }
            PlayCard(card);
        }

        private void GraveyardOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            // each item that gets added to the graveyard is automatically revealed
            foreach (var card in Graveyard)
            {
                card.IsRevealed = true;
            }

            PropertyChangedHandler(this, new PropertyChangedEventArgs(nameof(GraveyardText)));
        }

        private void DrawGreenCard()
        {
            if (!CanDrawGreenCard)
            {
                GameLogger.GetInstance().Log("You need an Overcharged - Lackner to draw a green card.");
                return;
            }

            PlayCard(Card000DrawGreenCard.GetInstance());
            UserPlayer.TurnPhase = TurnPhase.SpellPhase; // activating the card will increment the TurnPhase by one, and then automatically end it
        }

        private void OnMessageLogged(object sender, MessageLoggedEventArgs messageLoggedEventArgs)
        {
            ActiveMessages.Add(messageLoggedEventArgs.Message);
            Task.Run(() =>
            {
                Thread.Sleep(6000);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    ActiveMessages.Remove(messageLoggedEventArgs.Message);
                });
            });
        }


        private void Trade()
        {
            IsTrading = true;
            TradeView.Current.OnOpen();
        }

        private void EndTurn()
        {
            if (UserPlayer.TurnPhase == TurnPhase.UpgradePhase)
            {
                UserPlayer.TurnPhase++;
                UpdateClickableCards();
                return;
            }
            if (ConnectionHandler.GetInstance().ConnectionStatus == ConnectionHandler.ConnectionStatusEnum.Connected)
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.EndTurn);
        }

        public void RegisterOpponent(string name)
        {
            Opponents.Add(new Player(name) { OnClickCommand = new RelayCommand<object>(OnPlayerClick) });
        }

        private void OnPlayerClick(object clickedPlayer)
        {
            var asPlayer = clickedPlayer as Player;
            if (asPlayer == null) return;
            if (_currentNeededSelections == null || _currentSelectionIndex >= _currentNeededSelections.Count) return;

            _currentNeededSelections[_currentSelectionIndex].SelectedPlayer = asPlayer.Name;
            _currentSelectionIndex++;
            UpdateClickableCards();
        }

        public void AddCard(GameCard gameCard)
        {
            UserPlayer.Cards.Add(gameCard);
            gameCard.IsRevealed = true;
            if (AutoPlayNextCards > 0)
            {
                AutoPlayNextCards--;
                ConnectionHandler.GetInstance().SendPlayCard(gameCard.Id, new List<SelectionTarget>(), UserPlayer.Name, true);
            }
            UpdateClickableCards();
        }

        public void UpdateClickableCards()
        {

            foreach (var player in AllPlayer)
            {
                player.UpdateHasEnoughRarityToDrawExtraCard(this);
            }

            PropertyChangedHandler(this, new PropertyChangedEventArgs(nameof(AbortPossible)));
            var allCards = GetAllCards();
            foreach (var gameCard in allCards)
            {
                gameCard.IsClickable = false;
            }
            foreach (var player in AllPlayer)
            {
                player.IsClickable = false;
            }

            if (!HasMove) return;// if the player does not have the move, he is not allowed to click on any card
            if (_currentNeededSelections == null)
            {
                foreach (var userPlayerCard in UserPlayer.Cards)
                {
                    if (userPlayerCard.CardType == CardType.Upgrade)
                    {
                        var canPlayInSpell = UserPlayer.TurnPhase == TurnPhase.SpellPhase &&
                                      UserPlayer.CanPlayUpgradesInSpellPhase;
                        var isUpgradePhase = UserPlayer.TurnPhase == TurnPhase.UpgradePhase;

                        if (!(isUpgradePhase || canPlayInSpell)) continue;
                    }


                    if (userPlayerCard.CardType == CardType.Spell && UserPlayer.TurnPhase != TurnPhase.SpellPhase) continue;
                    if (userPlayerCard.CardType == CardType.RareLackner)
                    {
                        var isSuperCharged = UserPlayer.IsSuperCharged || UserPlayer.FreeOvercharges > 0;
                        var hasReachedMax = UserPlayer.PlayedRareLackner.Count >= UserPlayer.MaxRareLackner;
                        var mayPlayInSpell = UserPlayer.CanPlayRareLacknerInSpellPhase && UserPlayer.TurnPhase == TurnPhase.SpellPhase;
                        var hasNoRareLackner = UserPlayer.PlayedRareLackner.Count == 0;
                        if (!hasNoRareLackner) // if the user has no RareLackner, he may ALWAYS play one
                            if (!(isSuperCharged || mayPlayInSpell) || hasReachedMax) continue;
                    }

                    if (userPlayerCard.CardType == CardType.Spell)
                    {
                        var asSpell = userPlayerCard as SpellCard;
                        if (asSpell != null)
                        {
                            var neededCoolness = asSpell.NeededCoolness + UserPlayer.CoolnessNeededModifier;
                            if (!UserPlayer.PlayedRareLackner.Any(x => x.CombinedCoolness >= neededCoolness)) continue;
                        }
                    }

                    var selectionTargets = userPlayerCard.GetSelectionTargets(this);
                    var canBePlayed = true;

                    if (!userPlayerCard.IsSpecialRequirementFulfilled(this)) canBePlayed = false;
                    else
                    {
                        foreach (var selectionTarget in selectionTargets)
                        {
                            if (!selectionTarget.Exists(this)) canBePlayed = false;
                        }
                    }
                    userPlayerCard.IsClickable = canBePlayed;
                }
            }
            else
            {
                // check if the player has selected all cards
                if (_currentSelectionIndex >= _currentNeededSelections.Count)
                {
                    // if so play the card and set the needed selection to null
                    ActicateCard();
                    _currentNeededSelections = null;
                    return;
                }


                var currentSelectionTarget = _currentNeededSelections[_currentSelectionIndex];
                if (currentSelectionTarget.SelectionPlayedType == SelectionPlayedType.GraveyardCard) GraveyardVisibility = true;
                // show message


                if (currentSelectionTarget.SelectionType == SelectionType.Card)
                {
                    var fromWhom = currentSelectionTarget.OwnerType == OwnerType.All
                        ? ""
                        : " from " + currentSelectionTarget.OwnerType;

                    GameLogger.GetInstance().Log("Select a " + currentSelectionTarget.SelectionCardType + fromWhom);
                    var cards = currentSelectionTarget.GetCards(this);
                    foreach (var gameCard in cards)
                    {
                        gameCard.IsClickable = true;
                    }
                }
                else
                {
                    GameLogger.GetInstance().Log("Select a player");
                    var players = currentSelectionTarget.GetPlayer(this);
                    foreach (var player in players)
                    {
                        player.IsClickable = true;
                    }
                }

            }


        }

        public void OpponentCardOnClick(GameCard card)
        {
            if (_currentNeededSelections == null) return;

            var cards = _currentNeededSelections[_currentSelectionIndex].GetCards(this);
            if (cards.Contains(card))
            {
                _currentNeededSelections[_currentSelectionIndex].SelectedCard = card.Id;
                _currentSelectionIndex++;
                UpdateClickableCards();
            }
        }

        public void PlayerCardOnClick(GameCard card)
        {
            if (_currentNeededSelections == null || _currentNeededSelections.Count == 0)
            {
                PlayCard(card);
                return;
            }
            var cards = _currentNeededSelections[_currentSelectionIndex].GetCards(this);
            if (cards.Contains(card))
            {
                _currentNeededSelections[_currentSelectionIndex].SelectedCard = card.Id;
                _currentSelectionIndex++;
                UpdateClickableCards();
            }
        }

        public void PlayCard(GameCard card, bool forFree = false)
        {
            if (!HasMove) return;
            _currentPlayedCard = card;
            _currentNeededSelections = card.GetSelectionTargets(this);
            _currentSelectionIndex = 0;
            _playNextCardForFree = forFree;

            UpdateClickableCards();
        }

        private void ActicateCard()
        {
            if (ConnectionHandler.GetInstance().ConnectionStatus == ConnectionHandler.ConnectionStatusEnum.Connected)
                ConnectionHandler.GetInstance().SendPlayCard(_currentPlayedCard.Id, _currentNeededSelections.ToList(), null, _playNextCardForFree);
            _playNextCardForFree = false;
        }

        public void ActivateCard(PlayCardMessage playCardMessage)
        {
            var playedCard = GetCard(playCardMessage.PlayedCardId);
            
            // abort if the selection targets do not match up
            if (playCardMessage.OnCards.Count != playedCard.GetSelectionTargets(this).Count) return;

            var forPlayer = ConnectionPanel.Username == playCardMessage.ForPlayer
                ? UserPlayer
                : Opponents.First(x => x.Name == playCardMessage.ForPlayer);
            forPlayer.CardsPlayedLastRound.Add(playedCard);

            playedCard.LastPlayedBy = forPlayer.Name;
            if (forPlayer.Name != UserPlayer.Name)
                LastPlayedCards.Add(playedCard);

            if (playedCard.GetsRevealed) playedCard.IsRevealed = true;

            GameLogger.GetInstance().Log($"{forPlayer.Name} played {playedCard.Name}");
            try
            {
                playedCard.Activate(this, playCardMessage.OnCards.ToList(), forPlayer);
            }
            catch (NotImplementedException)
            {
                DebugLogger.GetInstance().LogError("Card is not implemented");
            }

            if (!playCardMessage.ForFree)
            {
                forPlayer.TurnPhase++;
                if (playCardMessage.OnCards.Count == 1 && playedCard.CardType == CardType.Upgrade)
                {
                    var targetCard = GetCard(playCardMessage.OnCards.First().SelectedCard);
                    var rareLackner = targetCard as RareLacknerCard;
                    if (rareLackner != null && rareLackner.FreeUpgrades > 0)
                    {
                        rareLackner.FreeUpgrades--;
                        forPlayer.TurnPhase--;
                    }
                }

                if (forPlayer.PlayPoints != 2) forPlayer.PlayPoints = 0;

                if (playedCard.NeededPoints == 2)
                {
                    if (!forPlayer.IsSuperCharged)
                    {
                        forPlayer.FreeOvercharges--;
                    }
                    else forPlayer.PlayPoints = 0;
                }
                if (forPlayer.Name == UserPlayer.Name && forPlayer.TurnPhase == TurnPhase.TurnOver) EndTurn();
            }


            UpdateClickableCards();
        }



        private GameCard GetCard(int id)
        {
            var allCards = GetAllCards();

            return allCards.FirstOrDefault(gameCard => gameCard.Id == id);
        }

        private List<GameCard> GetAllCards()
        {
            var players = new List<Player> { UserPlayer };
            players.AddRange(Opponents);

            var cards = new List<GameCard>();
            foreach (var player in players)
            {
                cards.AddRange(player.PlayedCards);
                cards.AddRange(player.Cards);
            }
            // add system cards
            cards.Add(Card000RemoveUpgrade.GetInstance());
            cards.Add(Card000DrawGreenCard.GetInstance());
            cards.Add(Card000SacrificeCard.GetInstance());
            return cards;
        }

        public void SwapCards(int firstCard, int secondCard, GameViewModel gameViewModel)
        {
            var players = new List<Player>();
            players.AddRange(Opponents);
            players.Add(UserPlayer);

            var firstPlayer = players.First(x => x.Cards.Any(y => y.Id == firstCard));
            var secondPlayer = players.First(x => x.Cards.Any(y => y.Id == secondCard));

            var firstAsCard = GameCard.GetCardById(firstCard);
            var secondAsCard = GameCard.GetCardById(secondCard);

            firstPlayer.Cards.Remove(firstAsCard);
            secondPlayer.Cards.Remove(secondAsCard);

            firstPlayer.Cards.Add(secondAsCard);
            secondPlayer.Cards.Add(firstAsCard);

            if (firstPlayer.Name == UserPlayer.Name)
            {
                secondAsCard.ClickedCommmand = new RelayCommand<object>(gameViewModel.PlayerCardOnClick);
            }
            if (secondPlayer.Name == UserPlayer.Name)
            {
                firstAsCard.ClickedCommmand = new RelayCommand<object>(gameViewModel.PlayerCardOnClick);
            }

            if (UserPlayer.HasTurn)
                UpdateClickableCards();
        }

        /// <summary>
        /// Indicates that the UserPlayer receives the turn.
        /// </summary>
        public void ReceiveTurn(Player forPlayer = null)
        {
            // forPlayer is THIS UserPlayer
            if (forPlayer == null)
            {
                forPlayer = UserPlayer;
                forPlayer.Turn++;
                HasMove = true;
                forPlayer.CardsPlayedLastRound.Clear();
                // if the current userPlayer has turns to skip, auto end the turn
                if (forPlayer.TurnSkips > 0)
                {
                    forPlayer.TurnSkips -= 1;
                    GameLogger.GetInstance().Log("You have to skip a turn. Turn skips left: " + forPlayer.TurnSkips);
                    EndTurn();
                    return;
                }
                else
                {
                    if (forPlayer.HasEnoughRarityToWin)
                    {
                        ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.WinGame, forPlayer.Name);
                        ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.CloseGame, forPlayer.Name);
                        return;
                    }
                    // draw a card
                    if (forPlayer.DrawGreenCardNextTurn)
                    {
                        ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
                    }
                    else ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);

                    if (forPlayer.HasEnoughRarityToDrawExtraCard && forPlayer.Turn > 1) ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);
                }
            }

            // forPlayer is another player (not this client)
            forPlayer.Turn++;
            forPlayer.CardsPlayedLastRound.Clear();
            forPlayer.HasTurn = true;
            forPlayer.TurnPhase = TurnPhase.UpgradePhase;

            if (forPlayer.TurnSkips > 0)
            {
                forPlayer.TurnSkips--;
            }
            else
            {

                forPlayer.PlayPoints += 1;
                if (forPlayer.PlayPoints > 2) forPlayer.PlayPoints = 2;

            }

        }

        public void ResetEventListener(GameCard card)
        {
            var owner = GameCard.GetOwner(card, this);
            if (owner.Name == UserPlayer.Name) card.ClickedCommmand = new RelayCommand<object>(x => GameViewModel.Current.PlayerCardOnClick(x));
            else card.ClickedCommmand = new RelayCommand<object>(x => GameViewModel.Current.OpponentCardOnClick(x));
        }
    }
}
