﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card087ZuNahLackner : SpellCard
    {
        public Card087ZuNahLackner(int id, string owner) : base(87, id, owner)
        {
            Power = SpellCardPower.Middle;
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }
            GameLogger.GetInstance().Log($"{Name}'s Freunde wurden {forPlayer.Name} Hand hinzugefügt.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
