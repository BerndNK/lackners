﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Model.Host {
    public class RegisteredClient : INotifyPropertyChanged{
        private GameState _gameState;
        private string _username;

        public string Username {
            get { return _username; }
            set {
                _username = value; 
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the state of the game.
        /// </summary>
        /// <value>
        /// The state of the game.
        /// </value>
        public GameState GameState {
            get { return _gameState; }
            set {
                _gameState = value;
                OnPropertyChanged();
            }
        }

        public DateTime LastGameStateUpdate { get; set; }

        public RegisteredClient(string username) {
            Username = username;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
