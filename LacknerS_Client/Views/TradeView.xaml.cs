﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using LacknerS_Client.Core.Network.Messages;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Description for TradeView.
    /// </summary>
    public partial class TradeView : UserControl
    {

        public static TradeView Current;

        /// <summary>
        /// Initializes a new instance of the TradeView class.
        /// </summary>
        public TradeView() {
            InitializeComponent();
            Current = this;
        }


        public void HandleMessage(TradeMessage message) {
            (DataContext as TradeViewModel)?.HandleMessage(message);
        }

        public void OnOpen()
        {
            (DataContext as TradeViewModel)?.OnOpen();
        }
    }
}