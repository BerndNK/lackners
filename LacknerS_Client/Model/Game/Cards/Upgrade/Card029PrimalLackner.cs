﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card029PrimalLackner : UpgradeCard
    {
        public Card029PrimalLackner(int id, string owner) : base(29, id, owner)
        {
            Rarity = 2;
            Coolness = -1;
        }
    }
}
