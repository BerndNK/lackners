﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Model.Game.Cards.RareLackner;
using LacknerS_Client.Model.Game.Cards.Spell;
using LacknerS_Client.Model.Game.Cards.Upgrade;

namespace LacknerS_Client.Model.Game.Cards {
    public static class GameCardFactory
    {
        private static List<GameCard> _cards;

        /// <summary>
        /// List of instances of all GameCards.
        /// </summary>
        /// <value>
        /// The cards.
        /// </value>
        public static List<GameCard> Cards
        {
            get
            {
                if (_cards == null) _cards = GetAllCards();
                return _cards;
                
            }
            private set
            {
                _cards = value;   
            }
        }

        public static List<GameCard> GetAllCards()
        {
            var toReturn = new List<GameCard>
            {
                
                new Card001UrsprungsLackner(-1, null),
                new Card002LongArmLackner(-1, null),
                new Card003SurfaceNeuAufgesetzt(-1, null),
                new Card004KrasseGitarreLackner(-1, null),
                new Card005SchickerHutLackner(-1, null),
                new Card006CrazyLackner(-1, null),
                new Card007JackeKaputtLackner(-1, null),
                new Card008NervigeFahrtLackner(-1, null),
                new Card009StandardLackner(-1, null),
                new Card010StalkerLackner(-1, null),
                new Card011VenedigLackner(-1, null),
                new Card012OrgasmusLackner(-1, null),
                new Card013KaffeeLackner(-1, null),
                new Card014HotChocolateLackner(-1, null),
                new Card015GiesskannenLackner(-1, null),
                new Card016AutosarLackner(-1, null),
                new Card017DatenzentrumLackner(-1, null),
                new Card018LangeweileLackner(-1, null),
                new Card019ZeitverschiebungsLackner(-1, null),
                new Card020MagnumLackner(-1, null),
                new Card021RageLackner(-1, null),
                new Card022KeineFucksLackner(-1, null),
                new Card023IstGespanntLackner(-1, null),
                new Card024FussballLackner(-1, null),
                new Card025FalscherGottLackner(-1, null),
                new Card026JausnLackner(-1, null),
                new Card027DealerLackner(-1, null),
                new Card028LkwFahrerLackner(-1, null),
                new Card029PrimalLackner(-1, null),
                new Card030TafelWischLackner(-1, null),
                new Card031PauseLackner(-1, null),
                new Card032LangeSchlangeLackner(-1, null),
                new Card033HoehlenForscherLackner(-1, null),
                new Card034BergsteigerLackner(-1, null),
                new Card035FastFoodLackner(-1, null),
                new Card036NasenblutenLackner(-1, null),
                new Card037BettLackner(-1, null),
                new Card038VollBreitLackner(-1, null),
                new Card039EremitLackner(-1, null),
                new Card040SchadenfreudeLackner(-1, null),
                new Card041TopDownLackner(-1, null),
                new Card042RomantikLackner(-1, null),
                new Card043DesktopLackner(-1, null),
                new Card044AlmLackner(-1, null),
                new Card045NinjaLackner(-1, null),
                new Card046JaegerLackner(-1, null),
                new Card047BiergartenLackner(-1, null),
                new Card048FastGestorbenLackner(-1, null),
                new Card049SniperLackner(-1, null),
                new Card050OrangenGottLackner(-1, null),
                new Card051WhatsAppLackner(-1, null),
                new Card052WildlingLackner(-1, null),
                new Card053SpookedLackner(-1, null),
                new Card054VersuchNichtZuLachenLackner(-1, null),
                new Card055GondelfahrerLackner(-1, null),
                new Card056SchlafBereitLackner(-1, null),
                new Card057FernsehTeamLackner(-1, null),
                new Card058KackNaturLackner(-1, null),
                new Card059WeihnachtsfeierLackner(-1, null),
                new Card061NervigerTripLackner(-1, null),
                new Card062BusinessLackner(-1, null),
                new Card063TjaLackner(-1, null),
                new Card064SonnenscheinLackner(-1, null),
                new Card065KathedraleLackner(-1, null),
                new Card066KackVortragLackner(-1, null),
                new Card067KillerLackner(-1, null),
                new Card068KlempnerLackner(-1, null),
                new Card069FettLackner(-1, null),
                new Card070SightseeingLackner(-1, null),
                new Card071VerlaufenLackner(-1, null),
                new Card072ReueLackner(-1, null),
                new Card073PopcornLackner(-1, null),
                new Card074PolarLackner(-1, null),
                new Card075DaemonLackner(-1, null),
                new Card076MensaLackner(-1, null),
                new Card077ZugFahrenLackner(-1, null),
                new Card078ZwingMichNichtLackner(-1, null),
                new Card079HotLackner(-1, null),
                new Card080KonzertLackner(-1, null),
                new Card081PisaLackner(-1, null),
                new Card082KetchupLackner(-1, null),
                new Card083DateLackner(-1, null),
                new Card084MisstrauischLackner(-1, null),
                new Card085VorlesungLackner(-1, null),
                new Card086KonzertVorbeiLackner(-1, null),
                new Card087ZuNahLackner(-1, null),
                new Card088ImaxLackner(-1, null),
                new Card089H19Lackner(-1, null),
                new Card090SexyLackner(-1, null),
                new Card091CooleBusfahrtLackner(-1, null),
                new Card092AlchemistLackner(-1, null),
                new Card093MetroLackner(-1, null),
                new Card094TopSpeedLackner(-1, null),
                new Card095RapeLackner(-1, null),
                new Card096SonnenbrandLackner(-1, null),
                new Card097AutoFahrenLackner(-1, null),
                new Card098SkiFahrenLackner(-1, null),
                new Card099AlkoholLackner(-1, null),
                new Card100ZerstörerDerWelten(-1, null)
            };
            return toReturn;
        }

        /// <summary>
        /// Produces the specified card via the given card no.
        /// </summary>
        /// <param name="cardNo">The card no.</param>
        /// <param name="cardId">The card identifier.</param>
        /// <param name="owner">The owner.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">The given card no did not exist.</exception>
        public static GameCard Produce(int cardNo, int cardId, string owner)
        {
            // look into the Cards list and see which card has the same no; get it's type
            var targetType = Cards.FirstOrDefault(x => x.No == cardNo)?.GetType();
            if(targetType == null) throw new ArgumentException("The given card no did not exist.");

            // then create a new instance of this type, with the given parameter id and owner
            var cardInstance = (GameCard) Activator.CreateInstance(targetType, args:new object[] {cardId, owner});
            return cardInstance; // finally return it
        }


    }
}
