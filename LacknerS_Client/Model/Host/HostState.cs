﻿namespace LacknerS_Client.Model.Host {
    public enum HostState {
        PollingClients, // initial state where the host is sending out requests to register
        RegistrationComplete // when the registration phase has been ended
    }
}