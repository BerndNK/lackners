﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card025FalscherGottLackner : SpellCard
    {
        public Card025FalscherGottLackner(int id, string owner) : base(25, id, owner)
        {
            Power = SpellCardPower.High;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            var selectionTarget = new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.Player);
            // make sure the player has at least 2 rare lackner
            selectionTarget.SpecialGameCondition =
                game => game.UserPlayer.PlayedCards.Count(x => x.CardType == CardType.RareLackner) >= 2;
            return new List<SelectionTarget>
            {
                selectionTarget,
                new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.Enemy),
                new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.Player)
            };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var cardToDestroy = (RareLacknerCard) GetCardById(selectionTargets[0].SelectedCard);
            var cardToSteal = (RareLacknerCard) GetCardById(selectionTargets[1].SelectedCard);
            var cardToGive = (RareLacknerCard) GetCardById(selectionTargets[2].SelectedCard);

            var ownerToDestroy = GetOwner(cardToDestroy, activeGame);
            ownerToDestroy.PlayedRareLackner.Remove(cardToDestroy);
            ownerToDestroy.PlayedCards.Remove(cardToDestroy);
            activeGame.Graveyard.Add(cardToDestroy);

            var stolenUpgrades = cardToSteal.Upgrades.ToList();
            cardToSteal.Upgrades.Clear();
            foreach (var stolenUpgrade in stolenUpgrades)
            {
                cardToGive.Upgrades.Add(stolenUpgrade);
            }

            GameLogger.GetInstance().Log($"{Name} opfert {cardToDestroy.Name} um alle Upgrades von {cardToSteal.Name} zu stehlen und sie {cardToGive.Name} zu schenken.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
