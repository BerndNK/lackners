﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.RareLackner {
   public class Card012OrgasmusLackner : RareLacknerCard
   {
       public new int CombinedCoolness => Coolness; // coolness upgrades have no effect on this lackner

       public Card012OrgasmusLackner(int id, string owner) : base(11, id, owner)
       {
           Rarity = 7;
           Coolness = 3;
       }
   }
}
