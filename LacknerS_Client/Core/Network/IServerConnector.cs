﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;
using LacknerS_Client.Core.Network.Messages;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Core.Network {
    public interface IServerConnector {
        #region Events
        /// <summary>
        /// Event for when this Connector has received bytes that shall be handled
        /// </summary>
        event ConnectionHandler.BytesReceivedHandler BytesReceived;
        #endregion

        #region Properties
        /// <summary>
        /// Status of the connection
        /// </summary>
        ConnectionHandler.ConnectionStatusEnum ConnectionStatus { get; }

        /// <summary>
        /// Text to represent the current connection status
        /// </summary>
        string StatusText { get; }

        /// <summary>
        /// Decides whether to use proxy or not
        /// </summary>
        string ProxyAddress { get; set; }
        string ProxyUsername { get; set; }
        string ProxyPassword { get; set; }
        string ProxyDomain { get; set; }

        #endregion

        #region Connect
        /// <summary>
        /// Connets to the specified target.
        /// </summary>
        /// <param name="target">IP to connect to. Allowed to be a DNS</param>
        /// <param name="port">Port to connect to</param>
        void Connect(string target, int port);
        #endregion

        #region Data Sending
        /// <summary>
        /// Sends a string to the server
        /// </summary>
        /// <param name="message">String to send</param>
        void Send(string message);

        /// <summary>
        /// Sends a message to inform the host that the game state has changed.
        /// </summary>
        /// <param name="to">The new game state.</param>
        void Send(GameState to);

        /// <summary>
        /// Sends a BitmapSource to Server
        /// </summary>
        /// <param name="image"></param>
        void Send(BitmapSource image);

        /// <summary>
        /// Sends the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="user">The user.</param>
        void Send(OrganizationType type, string user);

        /// <summary>
        /// Sends the file pehind the path to the Server
        /// </summary>
        /// <param name="path">Path to the file which shall be sended</param>
        void SendFile(string path);

        void SendCard(int type, string forPlayer, int id);

        void SendPlayCard(int id, List<SelectionTarget> onId, string forPlayer, bool forFree = false);

        void SendTrade(TradeMessageType type, int forCardId);

        void SendSwap(int firstCardId, int secondCardId);
        #endregion

        #region Data Receiving
        /// <summary>
        /// Starts a listener thread, which fires BytesReceived events
        /// </summary>
        void StartListener();

        #endregion
    }
}
