﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card044AlmLackner : UpgradeCard
    {
        public Card044AlmLackner(int id, string owner) : base(44, id, owner)
        {
            Rarity = 0;
            Coolness = -2;
        }
    }
}
