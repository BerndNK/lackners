#ifndef _SOCKET_HANDLER_UNIX_H_
#define _SOCKET_HANDLER_UNIX_H_

#include "SocketHandler.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

namespace network {
	class SocketHandlerUnix : public SocketHandler {
	public:
		SocketHandlerUnix(std::string port = DEFAULT_PORT);
		SocketHandlerUnix(const SocketHandlerUnix& socketHandler) = delete;
		~SocketHandlerUnix(); // do WSACleanup equivalent and close the socket created a initialized

		// Creates the socket and sets everything up to start listening
		// SET m_isInitialized to true !!
		int Initialize() override;

	private:
		int sockfd;
		int newsockfd;
		socklen_t clilen;
		struct sockaddr_in cli_addr;

	protected:
		// receive thread. When a message is received call "QueueMessage"
		// After a connection is build (in other words a client connected,
		// START a senderThread! (which is the Sender function below)
		void Listener(bool* waitingForClientBool, Client* thisClientPtr) override;

		// echo thread. Make a loop which quits when "forceQuitBool" is true
		// check if the "ToPushList" Property of the Client is not empty
		// to do this, lock the "ListMutex" first.
		// empty the List by sending the "Array" of the first element through the given socket
		// Then remove the first element and delete[] "Array"
		void Sender(Client* thisClientPtr, void* socket, bool* forceQuitBool) override;
	};
}
#endif
