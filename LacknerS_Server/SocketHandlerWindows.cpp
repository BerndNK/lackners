#include <string>
#ifdef _WIN32
#include "SocketHandlerWindows.h"
#include <iostream>
// #define PRINT_INFO

// public
network::SocketHandlerWindows::SocketHandlerWindows(std::string port) : SocketHandler(port) {

}

network::SocketHandlerWindows::~SocketHandlerWindows() {
	if (!m_isInitalized) return;
	// close server socket
	closesocket(m_listenSocket);
	WSACleanup();
	WaitForThreads();
}

int network::SocketHandlerWindows::Initialize() {
	// server setup
	int iResult;

	struct addrinfo *result = nullptr;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &m_wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(nullptr, m_port.c_str(), &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}


	// Create a SOCKET for connecting to server
	m_listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (m_listenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}


	// Setup the TCP listening socket
	iResult = bind(m_listenSocket, result->ai_addr, static_cast<int>(result->ai_addrlen));
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(m_listenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iResult = listen(m_listenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(m_listenSocket);
		WSACleanup();
		return 1;
	}

	m_isInitalized = true;
	return 0;
}

void network::SocketHandlerWindows::Listener(bool* waitingForClientBool, Client* thisClientPtr) {
	// TODO use the non blocking accept
	m_coutMutex.lock();
#ifdef PRINT_INFO
	std::cout << "Client: " << thisClientPtr->No << " - Wait for connect" << std::endl;
#endif
	m_coutMutex.unlock();

	auto clientSocket = accept(m_listenSocket, nullptr, nullptr);
	*waitingForClientBool = false;
	if (clientSocket == INVALID_SOCKET) {
		return;
	}


	// start sender
	auto forceQuitSenderThread = false;
	std::thread senderThread(&SocketHandlerWindows::Sender, this, thisClientPtr, &clientSocket, &forceQuitSenderThread);
	thisClientPtr->IsConnected = true;

	// listen
	int iResult;
	char recvbuf[DEFAULT_BUFLEN];
	auto recvbuflen = DEFAULT_BUFLEN;

	// Receive until the peer shuts down the connection
	m_coutMutex.lock();
#ifdef PRINT_INFO
	std::cout << "Client: " << thisClientPtr->No << " - Listening..." << std::endl;
#endif
	m_coutMutex.unlock();

	do {
		iResult = recv(clientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {
			m_coutMutex.lock();
#ifdef PRINT_INFO
			std::cout << "Client: " << thisClientPtr->No << " - Bytes received:" << iResult << std::endl;
#endif
			m_coutMutex.unlock();

			// Echo the buffer back to the sender

			QueueMessage(thisClientPtr, recvbuf, iResult);
		}
		else if (iResult == 0) {
			m_coutMutex.lock();
#ifdef PRINT_INFO
			std::cout << "Client: " << thisClientPtr->No << " - Connection closing..." << std::endl;
#endif
			m_coutMutex.unlock();
		}
		else {
			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Recv failed with error: " << WSAGetLastError() << std::endl;
			m_coutMutex.unlock();
			forceQuitSenderThread = true;
			senderThread.join();
			closesocket(clientSocket);
			RemoveClient(thisClientPtr);
			return;
		}

	} while (iResult > 0);

	// shutdown the connection since we're done
	iResult = shutdown(clientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		m_coutMutex.lock();
		std::cout << "Client: " << thisClientPtr->No << "- Shutdown failed with error: " << WSAGetLastError() << std::endl;
		m_coutMutex.unlock();
		forceQuitSenderThread = true;
		senderThread.join();
		closesocket(clientSocket);
		RemoveClient(thisClientPtr);
		return;
	}

	// cleanup
	forceQuitSenderThread = true;
	senderThread.join();
	closesocket(clientSocket);
	RemoveClient(thisClientPtr);
	//WSACleanup();

	return;
}

void network::SocketHandlerWindows::Sender(Client* thisClientPtr, void* socket, bool* forceQuitBool) {
	while (!*forceQuitBool) {
		Sleep(100);
		SOCKET* clientSocket = nullptr;
		try {
			clientSocket = reinterpret_cast<SOCKET*>(socket);
		}
		catch (...) {
			m_coutMutex.lock();
			std::cout << "Client: " << thisClientPtr->No << " - Failed to cast socket into winsocket" << std::endl;
			m_coutMutex.unlock();
			return;
		}
		thisClientPtr->ListMutex.lock();
		while (thisClientPtr->ToPushList.size() != 0) {
			// get first element
			auto toSend = thisClientPtr->ToPushList[0];
			//unsigned char(*asArr)[DEFAULT_BUFLEN] = reinterpret_cast<unsigned char(*)[DEFAULT_BUFLEN]>(toSend.Array);
			auto iSendResult = send(*clientSocket, toSend.Array, toSend.Length, 0);

			// remove first element
			delete[] toSend.Array;
			thisClientPtr->ToPushList.erase(thisClientPtr->ToPushList.begin(), thisClientPtr->ToPushList.begin() + 1);
			if (iSendResult == SOCKET_ERROR) {
				m_coutMutex.lock();
				std::cout << "Client: " << thisClientPtr->No << " - Send failed with error: %d\n" << WSAGetLastError() << std::endl;
				m_coutMutex.unlock();
			}
			else {
				m_coutMutex.lock();
#ifdef PRINT_INFO
				std::cout << "Client: " << thisClientPtr->No << " - Sent: " << iSendResult << std::endl;
#endif
				m_coutMutex.unlock();
			}
		}
		thisClientPtr->ListMutex.unlock();
	}
}

void network::SocketHandlerWindows::ListenerHttp(bool* waitingForClientBool, Client* thisClientPtr) {
	auto clientSocket = accept(m_listenSocket, nullptr, nullptr);
	*waitingForClientBool = false;
	if (clientSocket == INVALID_SOCKET) {
		return;
	}

	// listen
	int iResult;
	char recvbuf[DEFAULT_BUFLEN];
	auto recvbuflen = DEFAULT_BUFLEN;

	auto expectedByte = -1;
	FullMessage* crntFullMessage = nullptr;
	do {
		iResult = recv(clientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0) {

			auto asStr = std::string(recvbuf);

			// if expected bytes = -1 we either set the expected bytes here (with a PUT, or only receive a get and break out of the loop)
			if (expectedByte == -1) {
				if (recvbuf[0] == 'P') {
					// PUT request
					std::string contentLengthString = "Content-Length: ";
					auto messageLengthIndex = asStr.find(contentLengthString) + contentLengthString.size();
					// get first non numeric char
					int i;
					for (i = messageLengthIndex; (asStr[i] >= '0' && asStr[i] <= '9') || asStr[i] == '-'; i++);
					auto messageCount = asStr.substr(messageLengthIndex, i - messageLengthIndex);
					expectedByte = std::stoi(messageCount);
					if (crntFullMessage != nullptr) delete(crntFullMessage);
					crntFullMessage = new FullMessage(m_fullMsgIdCounter);

					int messageStart = asStr.find("\r\n\r\n") + 4;
					if (messageStart == -1 || messageStart == iResult) continue;
					asStr = asStr.substr(messageStart, iResult - messageStart);
					iResult = asStr.size();
				}
				else {
					// GET request
					__int32 lastReceivedMsgIndex = -1;
					auto lmrtsIndex = asStr.find("LMRTS[") + 6; // + 6 to have the index point to the first char after the '['
					if (lmrtsIndex - 6 != -1) {
						auto lastMsgReceivedString = asStr.substr(lmrtsIndex, asStr.find(']', lmrtsIndex) - lmrtsIndex);
						lastReceivedMsgIndex = stoi(lastMsgReceivedString);
						// send messages after timestamp
					}

					// get length of content
					long contentLength = 0;
					auto i = lastReceivedMsgIndex;
					m_messagesMutex.lock();
					for (auto msg : m_fullCachedMessaged) {
						// only send messaged newer than the lastReceivedMsgIndex
						if (msg->Id <= i) continue;
						i = msg->Id;
						for (auto part : msg->Parts) {
							contentLength += part.Length;
							contentLength += 4;
						}
					}
					m_messagesMutex.unlock();

					// intialize contentStart and End message
					std::string contentStart = "<!DOCTYPE html> <html lang='de'> 	<head> 		<title>Holla hop</title> 		<meta charset='UTF-8'> 		<meta name='keywords' content=''> 		<meta name='description' content=''> 		<meta name='robots' content='index, follow'>MSGS::";
					std::string contentEnd = "LMRTS[" + std::to_string(i) + "]</head></html>";

					// send HTTP header
					auto header = "HTTP/1.1 200 OK\r\nContent-Length: " + std::to_string(contentStart.size() + contentEnd.size() + contentLength) + "\r\n\r\n";
					send(clientSocket, header.c_str(), header.size(), 0);

					// send content start
					send(clientSocket, contentStart.c_str(), contentStart.size(), 0);

					// send messages
					m_messagesMutex.lock();
					for (auto msg : m_fullCachedMessaged) {
						// only send messaged newer than the lastReceivedMsgIndex
						if (msg->Id <= lastReceivedMsgIndex) continue;
						lastReceivedMsgIndex = msg->Id;

						for (auto part : msg->Parts) {
							send(clientSocket, part.Array, part.Length, 0);
						}
						if (msg != m_fullCachedMessaged[m_fullCachedMessaged.size() - 1])
							send(clientSocket, "####", 4, 0); // Split string between messages
					}
					m_messagesMutex.unlock();
					send(clientSocket, contentEnd.c_str(), contentEnd.size(), 0);
					// close connection
					break;
				}
			}


			// queue the expected bytes into the crnt full message
			// copy message
			auto copy = new char[asStr.size()];
			memcpy(copy, asStr.c_str(), asStr.size());

			// create Message part and push it into the Parts vector
			crntFullMessage->Parts.push_back(Message(iResult, copy));

			// subtract the received bytes from the expected ones and check if we received everything
			expectedByte -= iResult;
			if (expectedByte <= 0) {
				m_messagesMutex.lock();
				crntFullMessage->Id = m_fullMsgIdCounter++;
				m_fullCachedMessaged.push_back(crntFullMessage);

				// check if we reached the max. amount of cached messages
				while (m_fullCachedMessaged.size() > MAX_AMOUNT_FULL_MESSAGES) {
					m_fullCachedMessaged.erase(m_fullCachedMessaged.begin(), m_fullCachedMessaged.begin() + 1);
				}
				m_messagesMutex.unlock();

				// send response
				std::string response = "HTTP/1.1 200 OK\r\nConnection: close\r\n\r\n";
				auto iSendResult = send(clientSocket, response.c_str(), response.size(), 0);
				if (iSendResult == SOCKET_ERROR) {
					m_coutMutex.lock();
					std::cout << "Client: " << thisClientPtr->No << " - Send failed with error: %d\n" << WSAGetLastError() << std::endl;
					m_coutMutex.unlock();
				}

				long cachedBytes = 0;
				for (auto msg : m_fullCachedMessaged) {
					for (auto part : msg->Parts) {
						cachedBytes += part.Length;
					}
				}

				m_coutMutex.lock();
#ifdef PRINT_INFO
				std::cout << "Received message. Current cached bytes: " << cachedBytes << " in " << m_fullCachedMessaged.size() << " packet(s)" << std::endl;
#endif
				m_coutMutex.unlock();


				// close connection
				break;
			}

			// QueueMessage(thisClientPtr, recvbuf, iResult);
		}
		else if (iResult == 0) {

		}
		else {

			closesocket(clientSocket);
			RemoveClient(thisClientPtr);
			return;
		}

	} while (iResult > 0);

	// shutdown the connection since we're done
	iResult = shutdown(clientSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		m_coutMutex.lock();
		std::cout << "Client: " << thisClientPtr->No << "- Shutdown failed with error: " << WSAGetLastError() << std::endl;
		m_coutMutex.unlock();

		closesocket(clientSocket);
		RemoveClient(thisClientPtr);
		return;
	}

	// cleanup
	closesocket(clientSocket);
	RemoveClient(thisClientPtr);
	//WSACleanup();
}

#endif
