﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner {
    public class Card006CrazyLackner : RareLacknerCard {
        public Card006CrazyLackner(int id, string owner) : base(6, id, owner)
        {
            Rarity = 3;
            Coolness = 5;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.CanRemoveOtherPlayersUpgrades = true;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            forPlayer.CanRemoveOtherPlayersUpgrades = false;
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
