﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;

namespace LacknerS_Client.Converter
{
    public class WidthToColumnCountConverter : MarkupExtension, IValueConverter
    {
        public double ColumnWidth { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var width = (double) value;
            if (Math.Abs(ColumnWidth) < 0.0001) ColumnWidth = 250;
            return (int)(width / ColumnWidth);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider) => this;
    }
}
