﻿namespace LacknerS_Client.Model.Game {
    public enum GameState {
        WaitingForHostRequest = 0, // initial state, when the game client is waiting for a host to request it's invoke
        SentResponse, // the client has sent a response but has not been confirmed it's registration yet
        HostHasConfirmed, // when the host as confirmed this clients invoke
        GameStarted,
        HasMove,
        WaitingForOtherPlayer,
        GameClosed,
        Lost,
        Won
    }
}