using System;

namespace LacknerS_Client.Core.Logging {
    /// <summary>
    /// Describes the type of a log message
    /// </summary>
    [Flags]
    public enum LogType {
        Info = 1,
        Error = 2,
        Network = 4,
        All = Info | Error | Network
    }
}