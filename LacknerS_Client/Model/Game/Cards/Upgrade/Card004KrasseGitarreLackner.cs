﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card004KrasseGitarreLackner : UpgradeCard{
        public Card004KrasseGitarreLackner(int id, string owner) : base(4, id, owner)
        {
            Rarity = 1;
            Coolness = 2;
        }
    }
}
