﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card069FettLackner : RareLacknerCard
    {
        public Card069FettLackner(int id, string owner) : base(69, id, owner)
        {
            Rarity = 6;
            Coolness = 4;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.MaxRareLackner = forPlayer.PlayedRareLackner.Count;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            GameLogger.GetInstance().Log($"Fett - Lackner zerstört. {forPlayer.Name} kann wieder mehr als 1 RareLackner spielen.");
            forPlayer.MaxRareLackner = Int32.MaxValue;
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
