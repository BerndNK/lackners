﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card002LongArmLackner : UpgradeCard{
        public Card002LongArmLackner(int id, string owner) : base(2, id, owner)
        {
            Rarity = 2;
            Coolness = -1;
        }
    }
}

