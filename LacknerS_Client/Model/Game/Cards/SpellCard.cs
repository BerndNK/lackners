﻿using System.Collections.Generic;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards {
    public class SpellCard : GameCard
    {
        public int NeededCoolness => Power == SpellCardPower.Low ? 3 : Power == SpellCardPower.Middle ? 7 : 11;

        public SpellCardPower Power { get; set; } = SpellCardPower.Low;

        public SpellCard(int no, int id, string owner) : base(no, CardType.Spell, id, owner)
        {
            NeededPoints = 1;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget>();
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.PlayedCards.Add(this);
            forPlayer.Cards.Remove(this);
            activeGame.Graveyard.Add(this);
        }

        public override string ToString()
        {
            return $"Needed Coolness: {NeededCoolness}";
        }
    }
}
