﻿using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace LacknerS_Client.Templates {
    public class SettingFormTemplateSelector : DataTemplateSelector {
        public DataTemplate StringTemplate { get; set; }
        public DataTemplate UnkownTemplate { get; set; }
        public DataTemplate IntegerTemplate { get; set; }
        public DataTemplate CheckBoxTemplate { get; set; }
        public DataTemplate ErrorTemplate { get; set; }
        public DataTemplate HeadlineTemplate { get; set; }
        public DataTemplate SeperatorTemplate { get; set; }
        public DataTemplate ComboBoxTemplate { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            var containerElement = container as FrameworkElement;
            var element = item as XmlElement;
            if (element == null) return null;
            if (element.Attributes["Type"] == null) return ErrorTemplate;
            switch (element.Attributes["Type"].Value.ToLower()) {
                case "string":
                    return StringTemplate;
                case "integer":
                    return IntegerTemplate;
                case "cb":
                    return CheckBoxTemplate;
                case "headline":
                    return HeadlineTemplate;
                case "seperator":
                    return SeperatorTemplate;
                case "combo":
                    return ComboBoxTemplate;
                default:
                    return UnkownTemplate;
            }
        }
    }
}
