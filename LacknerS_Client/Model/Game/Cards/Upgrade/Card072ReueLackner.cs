﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card072ReueLackner : UpgradeCard
    {
        public Card072ReueLackner(int id, string owner) : base(72, id, owner)
        {
            Rarity = 1;
            Coolness = -1;
        }
    }
}