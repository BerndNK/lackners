﻿using System;
using System.ComponentModel;
using LacknerS_Client.Model;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Game.Cards;
using LacknerS_Client.Model.Host;

namespace LacknerS_Client.Design {
    public class DesignDataService : IDataService {
        public void GetData(Action<DataItem, Exception> callback) {
            // Use this to create design time data

            var item = new DataItem("Welcome to MVVM Light [design]");
            callback(item, null);
        }

        public void GetRegisteredClients(Action<BindingList<RegisteredClient>, Exception> callback) {
            var list = new BindingList<RegisteredClient>();
            list.Add(new RegisteredClient("Hans"));
            list.Add(new RegisteredClient("Dieter"));
            list.Add(new RegisteredClient("Marcel Davis"));
            callback(list, null);
        }


        public void GetOpponents(Action<BindingList<Player>, Exception> callback) {
            var list = new BindingList<Player>();
            list.Add(new Player("Hans", this));
            list.Add(new Player("Dieter", this));
            list.Add(new Player("Marcel Davis", this));
            callback(list, null);
        }

        public void GetPlayerCards(Action<BindingList<GameCard>, Exception> callback) {
            var list = new BindingList<GameCard>();
            list.Add(GameCardFactory.Produce(1, 1, ""));
            list.Add(GameCardFactory.Produce(1, 1, ""));
            list.Add(GameCardFactory.Produce(1, 1, ""));
            list.Add(GameCardFactory.Produce(1, 1, ""));
            callback(list, null);
        }
    }
}