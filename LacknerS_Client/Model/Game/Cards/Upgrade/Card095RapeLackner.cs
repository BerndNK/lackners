﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card095RapeLackner : UpgradeCard
    {
        public Card095RapeLackner(int id, string owner) : base(95, id, owner)
        {
            Rarity = -2;
            Coolness = 2;
        }
    }
}
