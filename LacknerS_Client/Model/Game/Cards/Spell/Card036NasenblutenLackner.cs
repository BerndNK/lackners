﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card036NasenblutenLackner : SpellCard
    {
        public Card036NasenblutenLackner(int id, string owner) : base(36, id, owner)
        {
            Power = SpellCardPower.Middle;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            var selectionTarget = new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.Enemy);
            // make sure there are at least 2 upgrades on the playfield
            selectionTarget.SpecialGameCondition =
                game => game.Opponents.Sum(x => x.PlayedRareLackner.Sum(y => y.Upgrades.Count)) >= 2;
            return new List<SelectionTarget>
            {
                selectionTarget,
                new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.Enemy)
            };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var upgradeCard = GetCardById(selectionTargets.First().SelectedCard) as UpgradeCard;
            var upgradeCard2 = GetCardById(selectionTargets.Last().SelectedCard) as UpgradeCard;
            if (upgradeCard == null || upgradeCard2 == null) throw new Exception("Id of card could not be found, or parsing failed.");

            var rareLackner = RareLacknerCard.GetContainingRareLackner(upgradeCard);
            rareLackner.Upgrades.Remove(upgradeCard);
            forPlayer.PlayedCards.Remove(upgradeCard);
            activeGame.Graveyard.Add(upgradeCard);

            rareLackner = RareLacknerCard.GetContainingRareLackner(upgradeCard2);
            rareLackner.Upgrades.Remove(upgradeCard2);
            forPlayer.PlayedCards.Remove(upgradeCard2);
            activeGame.Graveyard.Add(upgradeCard2);


            GameLogger.GetInstance().Log($"{Name} entfernte {upgradeCard.Name} und {upgradeCard2.Name}.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
