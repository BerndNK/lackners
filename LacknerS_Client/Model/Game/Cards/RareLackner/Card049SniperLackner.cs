﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card049SniperLackner : RareLacknerCard
    {
        public Card049SniperLackner(int id, string owner) : base(49, id, owner)
        {
            Rarity = 6;
            Coolness = 5;
            MaxUpgradeCount = 1;
        }
    }
}
