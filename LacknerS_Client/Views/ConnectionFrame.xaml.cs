﻿using System.Windows.Controls;
using LacknerS_Client.Core.Logging;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    public partial class ConnectionFrame : UserControl {
        public ConnectionFrame() {
            InitializeComponent();
#if DEBUG
            LogDisplay.Messages = DebugLogger.GetInstance().GetLogs();
#else
            LogDisplay.Messages = GameLogger.GetInstance().GetLogs();
#endif

        }
    }
}