﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game
{
    /// <summary>
    /// Describes a target that needs to be selected, which can be a card or a player, in order for a played card to be activated. (E.g. a spell card which targets an upgrade. So the selection target for this spell would be an upgrade card)
    /// </summary>
    [Serializable]
    public class SelectionTarget {
        public int SelectedCard { get; set; }
        public string SelectedPlayer { get; set; }

        public SelectionPlayedType SelectionPlayedType { get; private set; }

        public CardType SelectionCardType { get; private set; }
        public OwnerType OwnerType { get; private set; }

        public SelectionType SelectionType { get; private set; }

        [IgnoreDataMember]
        public SpecialPlayerConditionFulfiller SpecialPlayerCondition { get; set; }
        [IgnoreDataMember]
        public SpecialCardConditionFulfiller SpecialCardCondition { get; set; }
        [IgnoreDataMember]
        public SpecialGameConditionFulfiller SpecialGameCondition { get; set; }

        public delegate bool SpecialCardConditionFulfiller(GameCard card);
        public delegate bool SpecialPlayerConditionFulfiller(Player player);

        public delegate bool SpecialGameConditionFulfiller(ActiveGameViewModel game);

        public SelectionTarget(SelectionType selectionType, OwnerType ownerType, SpecialPlayerConditionFulfiller specialPlayerCondition = null) {
            SelectionType = selectionType;
            OwnerType = ownerType;
            SpecialPlayerCondition = specialPlayerCondition;
        }

        

        public SelectionTarget(SelectionPlayedType selectionPlayedType, CardType cardType, OwnerType ownerType, SpecialCardConditionFulfiller specialCardCondition = null) {
            SelectionType = SelectionType.Card;
            SelectionPlayedType = selectionPlayedType;
            SelectionCardType = cardType;
            OwnerType = ownerType;
            SpecialCardCondition = specialCardCondition;
        }

        

        public bool Exists(ActiveGameViewModel activeGame)
        {
            // first, check if the special game condition is fulfilled. If not, return false (IF there is a special condition given)
            if (SpecialGameCondition != null && !SpecialGameCondition(activeGame)) return false;

            if (SelectionType == SelectionType.Player)
            {
                // If there is no special condition return true, otherwise if there is one, check if there exists at least one player, which fulfills it. 
                return SpecialPlayerCondition == null || activeGame.AllPlayer.Exists(x => SpecialPlayerCondition(x));
            }
            var cards = GetCards(activeGame);
            return cards.Count > 0;
        }

        public List<GameCard> GetCards(ActiveGameViewModel activeGame) {
            if (SelectionType == SelectionType.Player) throw new Exception("GetCards may only be called, when the SelectionType is for Cards!");

            // get all relevant players
            var players = new List<Player>();
            if ((OwnerType & OwnerType.Player) == OwnerType.Player) players.Add(activeGame.UserPlayer);
            if ((OwnerType & OwnerType.Enemy) == OwnerType.Enemy) {
                players.AddRange(activeGame.Opponents);
            }

            // get all relevant cards
            var cards = new List<GameCard>();
            foreach (var player in players) {
                if ((SelectionPlayedType & SelectionPlayedType.PlayedCard) == SelectionPlayedType.PlayedCard) cards.AddRange(player.PlayedCards);
                if ((SelectionPlayedType & SelectionPlayedType.OwnedCard) == SelectionPlayedType.OwnedCard) cards.AddRange(player.Cards);
            }
            if ((SelectionPlayedType & SelectionPlayedType.GraveyardCard) == SelectionPlayedType.GraveyardCard) cards.AddRange(activeGame.Graveyard);

            // return all cards which have the right type
            var toReturn = cards.Where(gameCard => (gameCard.CardType & SelectionCardType) == gameCard.CardType).ToList();

            // and fulfill the special condition (if there is one)
            if (SpecialCardCondition != null) toReturn = toReturn.Where(x => SpecialCardCondition(x)).ToList();
            return toReturn;
        }

        public List<Player> GetPlayer(ActiveGameViewModel activeGame)
        {
            if (SelectionType == SelectionType.Card) throw new Exception("GetPlayer may only be called, when the SelectionType is for Player!");

            // get all relevant players
            var players = new List<Player>();
            if ((OwnerType & OwnerType.Player) == OwnerType.Player) players.Add(activeGame.UserPlayer);
            if ((OwnerType & OwnerType.Enemy) == OwnerType.Enemy)
            {
                players.AddRange(activeGame.Opponents);
            }

            // get all relevant player, which fulfill the specialPlayerCondition (if there is any)
            return players.Where(x => SpecialPlayerCondition == null || SpecialPlayerCondition(x)).ToList();
        }
    }
}
