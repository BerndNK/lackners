﻿using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Model
{
    public struct TradeOffer
    {
        public bool NotAccepted => !Accepted;
        public bool Accepted { get; set; }
        public string Username { get; set; }
        public GameCard OfferedCard { get; set; }

        public TradeOffer(string user)
        {
            Accepted = false;
            Username = user;
            OfferedCard = null;
        }

        public TradeOffer(string user, GameCard card)
        {
            Username = user;
            OfferedCard = card;
            Accepted = true;
        }
    }
}