﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card090SexyLackner : RareLacknerCard
    {
        public Card090SexyLackner(int id, string owner) : base(90, id, owner)
        {
            Rarity = 5;
            Coolness = 5;
            EveryoneCanRemoveUpgrades = true;
        }
    }
}
