﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card075DaemonLackner : UpgradeCard
    {
        public Card075DaemonLackner(int id, string owner) : base(75, id, owner)
        {
            Rarity = 0;
            Coolness = 2;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
