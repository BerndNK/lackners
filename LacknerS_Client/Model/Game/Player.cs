﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Model.Game.Cards;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game {
    public class Player : INotifyPropertyChanged{
        private string _name;
        private ObservableCollection<GameCard> _cards;

        public string Name {
            get { return _name; }
            set {
                _name = value; 
                OnPropertyChanged();
            }
        }

        public ObservableCollection<GameCard> Cards {
            get { return _cards; }
            set {
                _cards = value; 
                OnPropertyChanged();
            }
        }

        public ObservableCollection<GameCard> CardsPlayedLastRound { get; set; }

        public bool HasTurn
        {
            get { return _hasTurn; }
            set
            {
                _hasTurn = value; 
                OnPropertyChanged();
            }
        }

        public ObservableCollection<GameCard> PlayedCards {
            get { return _playedCards; }
            set {
                _playedCards = value; 
                OnPropertyChanged();
            }
        }

        public int TurnSkips
        {
            get { return _turnSkips; }
            set
            {
                _turnSkips = value; 
                OnPropertyChanged();
            }
        }

        public bool IsClickable
        {
            get { return _isClickable; }
            set
            {
                _isClickable = value; 
                OnPropertyChanged();
            }
        }

        public int SumCoolness => _playedCards.Sum(x => x.Coolness);

        public int SumRarity => _playedCards.Sum(x => x.Rarity);

        public bool IsSuperCharged => PlayPoints >= 2;

        public string EndTurnIcon => TurnPhase == TurnPhase.UpgradePhase ? "../Resources/icons8-Right 2 Filled-50.png" : "../Resources/icons8-Checked-96.png";
        public int PlayPoints
        {
            get { return _playPoints; }
            set
            {
                _playPoints = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsSuperCharged));
            }
        }

        public ICommand OnClickCommand
        {
            get { return _onClickCommand; }
            set
            {
                _onClickCommand = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<GameCard> _playedCards;
        private ObservableCollection<RareLacknerCard> _playedRareLackner;
        private bool _hasTurn;
        private int _turnSkips;
        private bool _isClickable;
        private ICommand _onClickCommand;
        private TurnPhase _turnPhase = TurnPhase.TurnOver;
        private bool _isSuperCharged;
        private int _playPoints;
        private int _coolnessNeededModifier;
        private bool _canPlayUpgradesInSpellPhase;
        private int _pointsNeededForWin = 15;
        private bool _canRemoveOtherPlayersUpgrades;
        private int _freeOvercharges;
        private bool _canPlayRareLacknerInSpellPhase;
        private int _maxRareLackner = Int32.MaxValue;
        private bool _canSacrifice;
        private bool _canRemoveUpgrades = true;
        private int _rarityLeadNeededForExtraCard = 2;
        private bool _hasEnoughRarityToDrawExtraCard;

        public ObservableCollection<RareLacknerCard> PlayedRareLackner
        {
            get { return _playedRareLackner; }
            private set
            {
                _playedRareLackner = value; 
                OnPropertyChanged();
            }
        }

        public TurnPhase TurnPhase
        {
            get { return _turnPhase; }
            set
            {
                _turnPhase = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(EndTurnIcon));
                OnPropertyChanged(nameof(EndTurnText));
                OnPropertyChanged(nameof(InUpgradePhase));
            }
        }

        public int Turn { get; set; } = 1;

        public bool CanRemoveOtherPlayersUpgrades
        {
            get { return _canRemoveOtherPlayersUpgrades; }
            set
            {
                _canRemoveOtherPlayersUpgrades = value; 
                OnPropertyChanged();
            }
        }

        public int PointsNeededForWin
        {
            get { return _pointsNeededForWin; }
            set
            {
                _pointsNeededForWin = value; 
                OnPropertyChanged();
            }
        }

        public bool CanPlayUpgradesInSpellPhase
        {
            get { return _canPlayUpgradesInSpellPhase; }
            set
            {
                _canPlayUpgradesInSpellPhase = value; 
                OnPropertyChanged();
            }
        }

        public int CoolnessNeededModifier
        {
            get { return _coolnessNeededModifier; }
            set
            {
                _coolnessNeededModifier = value; 
                OnPropertyChanged();
            }
        }

        public int MaxRareLackner
        {
            get { return _maxRareLackner; }
            set
            {
                _maxRareLackner = value; 
                OnPropertyChanged();
            }
        }

        public bool CanPlayRareLacknerInSpellPhase
        {
            get { return _canPlayRareLacknerInSpellPhase; }
            set
            {
                _canPlayRareLacknerInSpellPhase = value;
                OnPropertyChanged();
            }
        }

        public int FreeOvercharges
        {
            get { return _freeOvercharges; }
            set
            {
                _freeOvercharges = value; 
                OnPropertyChanged();
            }
        }

        public string EndTurnText => TurnPhase == TurnPhase.UpgradePhase ? "Skip Upgrade phase" : "End turn";

        public bool InUpgradePhase => TurnPhase == TurnPhase.UpgradePhase;
        public bool DrawGreenCardNextTurn { get; set; }

        public bool HasEnoughRarityToWin => PlayedRareLackner != null && PlayedRareLackner.Sum(x => x.CombinedRarity) >= PointsNeededForWin;

        public bool HasEnoughRarityToDrawExtraCard
        {
            get { return _hasEnoughRarityToDrawExtraCard; }
            set
            {
                _hasEnoughRarityToDrawExtraCard = value; 
                OnPropertyChanged();
            }
        }

        public int RarityLeadNeededForExtraCard
        {
            get { return _rarityLeadNeededForExtraCard; }
            set
            {
                _rarityLeadNeededForExtraCard = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance can sacrifice a lackner. This is true when the player has played a KillerLackner.
        /// </summary>
        public bool CanSacrifice
        {
            get { return _canSacrifice; }
            set
            {
                _canSacrifice = value; 
                OnPropertyChanged();
            }
        }

        public bool CanRemoveUpgrades
        {
            get { return _canRemoveUpgrades; }
            set
            {
                _canRemoveUpgrades = value;
                OnPropertyChanged();
            }
        }

        public Player(string name, IDataService dataService = null) {
            Name = name;
            Cards = new ObservableCollection<GameCard>();
            PlayedCards = new ObservableCollection<GameCard>();
            PlayedCards.CollectionChanged += PlayedCardsOnCollectionChanged;
            PlayedRareLackner = new ObservableCollection<RareLacknerCard>();
            
            CardsPlayedLastRound = new ObservableCollection<GameCard>();
        }

        
        private void PlayedCardsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs eventArgs)
        {
            OnPropertyChanged(nameof(HasEnoughRarityToWin));
            if (eventArgs.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var eventArgsOldItem in eventArgs.OldItems)
                {
                    ((GameCard)eventArgsOldItem).CardRemoved(ActiveGameViewModel.Current, this);
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void UpdateHasEnoughRarityToDrawExtraCard(ActiveGameViewModel activeGame)
        {
            // all player must have at least one RareLackner played for this feature to apply

            if (activeGame.AllPlayer.Any(x => x.PlayedRareLackner.Count == 0))
            {
                HasEnoughRarityToDrawExtraCard = false;
                return;
            }

            var allPlayerExceptThis = activeGame.AllPlayer;
            allPlayerExceptThis.Remove(allPlayerExceptThis.First(x => x.Name == Name));

            var mostRarityOfOpponents = allPlayerExceptThis.Max(x => x.PlayedRareLackner.Max(y => y.CombinedRarity));
            HasEnoughRarityToDrawExtraCard = PlayedRareLackner.Any(x => x.CombinedRarity >= mostRarityOfOpponents + RarityLeadNeededForExtraCard);
        }

        

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void AddCard(GameCard gameCard) {
            Cards.Add(gameCard);
            OnPropertyChanged(nameof(Cards));
        }
    }
}
