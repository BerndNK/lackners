﻿using System;
using System.Collections.Generic;
using System.Linq;
using LacknerS_Client.Model.Game;

namespace LacknerS_CardCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            var reader = new GameCardReader();
            var writer = new GameCardWriter();
            Console.WriteLine("Reading excel file:");
            var gameCards = new List<GameCard>();
            foreach (var gameCard in reader.GetAllCardsThroughExcel(@"C:\Users\Bernd\OneDrive\Projects\LacknerS\LacknerSCardsList.xlsx"))
            {
                gameCards.Add(gameCard);
                SetCount(gameCards.Count);
            }

            Console.WriteLine();
            //Console.WriteLine($"Located {gameCards.Count} cards. Creating png files.");
            writer.CreateCards(gameCards, @"C:\Users\Bernd\Desktop\LacknerSTest");
        }

        private static void SetCount(int number)
        {
            
            Console.CursorLeft = 0;
            Console.Write("Located cards: ");
            Console.CursorLeft = 22;
            Console.Write("{0} ", number); 
        }
    }
}
