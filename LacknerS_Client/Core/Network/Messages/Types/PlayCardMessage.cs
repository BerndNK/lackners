﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Views;

namespace LacknerS_Client.Core.Network.Messages.Types {
    [Serializable]
    public class PlayCardMessage: Message {

        

        public PlayCardMessage(int playedCardId, List<SelectionTarget> onCards , string forPlayer, bool forFree = false)
        {
            if (forPlayer == null) forPlayer = ConnectionPanel.Username;

            PlayedCardId = playedCardId;
            OnCards = onCards;
            ForPlayer = forPlayer;
            ForFree = forFree;


            foreach (var selectionTarget in OnCards)
            {
                selectionTarget.SpecialCardCondition = null;
                selectionTarget.SpecialGameCondition = null;
                selectionTarget.SpecialPlayerCondition = null;
            }
        }

        public bool ForFree { get; set; }

        public string ForPlayer { get; set; }

        public List<SelectionTarget> OnCards { get; set; }

        public int PlayedCardId { get; set; }
    }
}
