﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network.Encryption;
using LacknerS_Client.Core.Network.Messages;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Views;

namespace LacknerS_Client.Core.Network {
    public class SocketConnector : INotifyPropertyChanged, IServerConnector {
        #region Constants
        public const int DefaultBfflen = 16384;
        #endregion

        #region Fields
        private byte[] _bytes = new byte[DefaultBfflen];
        private System.Net.Sockets.Socket _senderSock;
        private string _statusText = "Disconnected";
        private ConnectionHandler.ConnectionStatusEnum _connectionStatus;
        private bool _startedListener;
        #endregion

        #region Helper Functions
        private static byte[] AppendArray(byte[] existing, byte[] toAppend, int appendLength) {
            // append new readed _bytes to crnt array
            var newBuffer = new byte[existing.Length + appendLength];
            // copy crnt buffer into newer larger array
            Array.Copy(existing, newBuffer, existing.Length);
            // append the new _bytes
            Array.Copy(toAppend, 0, newBuffer, existing.Length, appendLength);

            return newBuffer;
        }

        public static void ByteTest() {
         /*   var m = new MemoryStream();
            var formatter = new BinaryFormatter();
            var data = Encoding.Unicode.GetBytes("asdasdasdasd");
            formatter.Serialize(m, new TextMessage("allah akbar"));
            var buffer = m.ToArray();
            var toAppend = Encoding.Unicode.GetBytes("ALLAH AKBAR");
            buffer = AppendArray(buffer, toAppend, toAppend.Length);

            var formatter2 = new BinaryFormatter();
            Stream stream = new MemoryStream(buffer);
            var header = formatter2.Deserialize(stream) as Message;
            if (header == null) return;
            if (header.GetType() != typeof(TextMessage)) return;
            var asTxtMsg = header as TextMessage;
            if (asTxtMsg != null) {
                Console.WriteLine(asTxtMsg.Text);
            }*/
        }

        #endregion

        #region Data Sending
        private void Send(Message data, MessageContainer.MessageType type) {
            var message = MessageContainer.GetBytes(new MessageContainer(type, Message.GetBytes(data)));

            // append a int32 to the beginning of the array which indicates the length of the entire message (excluding the 4 for the count)
            var toSend = new byte[message.Length + 4];
            Array.Copy(message, 0, toSend, 4, message.Length);

            toSend[0] = (byte)(message.Length & 0xff);
            toSend[1] = (byte)((message.Length >> 8) & 0xff);
            toSend[2] = (byte)((message.Length >> 16) & 0xff);
            toSend[3] = (byte)((message.Length >> 24) & 0xff);
            

            // Sends data to a connected Socket. 
            try {
                DebugLogger.GetInstance().Log("bytes sent: "+toSend.Length, LogType.Network);
                _senderSock.Send(toSend);
            } catch (Exception e) {
                DebugLogger.GetInstance().LogError("Failed to send message. Error: " + e.Message);
                ConnectionStatus = ConnectionHandler.ConnectionStatusEnum.Disconnected;
                StatusText = "Disconnected (Server closed connection)";
            }
        }
        #endregion

        #region INotifyPropertyChangedImplementation
        private void CallPropertyChanged(string on) {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region IServerConnectorImplementation
        public string StatusText {
            get { return _statusText; }
            private set {
                _statusText = value;
                CallPropertyChanged("StatusText");
            }
        }

        public string ProxyAddress { get; set; }
        public string ProxyUsername { get; set; }
        public string ProxyPassword { get; set; }
        public string ProxyDomain { get; set; }

        public event ConnectionHandler.BytesReceivedHandler BytesReceived;

        public ConnectionHandler.ConnectionStatusEnum ConnectionStatus {
            get { return _connectionStatus; }
            private set {
                _connectionStatus = value;
                CallPropertyChanged("ConnectionStatus");
            }
        }

        #region Connection
        public async void Connect(string address, int port) {
            if (string.IsNullOrWhiteSpace(address)) throw new ArgumentException("Address may not be whitespace");
            if (ConnectionStatus == ConnectionHandler.ConnectionStatusEnum.Connected) {
                ConnectionStatus = ConnectionHandler.ConnectionStatusEnum.Disconnected;
                _senderSock.Disconnect(false);
            }

            StatusText = "Connecting";
            ConnectionStatus = ConnectionHandler.ConnectionStatusEnum.Connecting;
            await Task.Run(() => {
                try {
                    // Create one SocketPermission for socket access restrictions 
                    var permission = new SocketPermission(
                        NetworkAccess.Connect, // Connection permission 
                        TransportType.Tcp, // Defines transport types 
                        address, // Gets the IP addresses 
                        SocketPermission.AllPorts // All ports 
                        );

                    // Ensures the code to have permission to access a Socket 
                    permission.Demand();

                    // Resolves a host name to an IPHostEntry instance        
                    IPAddress ipAddr;
                    if (address[0] >= '0' && address[0] <= '9') ipAddr = IPAddress.Parse(address);
                    else ipAddr = Dns.GetHostEntry(address).AddressList[0];

                    var ipEndPoint = new IPEndPoint(ipAddr, port);

                    // Create one Socket object to setup Tcp connection 
                    _senderSock = new System.Net.Sockets.Socket(
                        ipAddr.AddressFamily, // Specifies the addressing scheme 
                        SocketType.Stream, // The type of socket  
                        ProtocolType.Tcp // Specifies the protocols  
                        );

                    _senderSock.ReceiveBufferSize = DefaultBfflen;
                    _senderSock.SendBufferSize = DefaultBfflen;
                    //_senderSock.NoDelay = false; //Using the Nagle algorithm 

                    // Establishes a connection to a remote host 
                    _senderSock.Connect(ipEndPoint);
                    DebugLogger.GetInstance().LogError("Connected to: " + _senderSock.RemoteEndPoint);
                    System.Windows.Application.Current.Dispatcher.Invoke(() => {
                        try {
                            StatusText = "Connected";
                            ConnectionStatus = ConnectionHandler.ConnectionStatusEnum.Connected;
                        } catch (Exception) {
                            // ignored
                        }
                    });
                } catch (Exception e) {
                    DebugLogger.GetInstance().LogError("Failed to connect. Message: " + e.Message);
                    System.Windows.Application.Current.Dispatcher.Invoke(() => {
                        try {
                            StatusText = "Connect failed";
                            ConnectionStatus = ConnectionHandler.ConnectionStatusEnum.Disconnected;
                        } catch (Exception) {
                            // ignored
                        }
                    });
                }
            });
        }
        #endregion

        #region Data Sending
        public void Send(string message) {
            message = Crypto.Encrypt(message, SettingsWindow.GetSetting("cryptopassword").ToString());
            Send(new TextMessage(message), MessageContainer.MessageType.Text);
        }

        public void Send(GameState state) {
            Send(new GameStateChangedMessage(state), MessageContainer.MessageType.GameState);
        }

        public void Send(BitmapSource image) {
            if (image == null) return;
            //var base64String = BitmapSerializer.GetBytes(image);
          // Send(new ImageMessage(BitmapSerializer.GetBytes(image)), MessageContainer.MessageType.Picture);
        }

        public void SendFile(string path) {
            try {
                Send("Sending file: " + path.Split('\\').Last());
         //       Send(new FileMessage(path), MessageContainer.MessageType.File);
            } catch (Exception ex) {
                DebugLogger.GetInstance().LogError("File transfer failed. Message: " + ex.Message);
            }
        }

        public void SendCard(int type, string forPlayer, int id) {
            Send(new DealCardMessage(type, forPlayer, id), MessageContainer.MessageType.DealCard);
        }

        public void SendPlayCard(int id, List<SelectionTarget> onIds, string forPlayer, bool forFree = false) {
            Send(new PlayCardMessage(id, onIds, forPlayer, forFree), MessageContainer.MessageType.PlayCard);
        }

        public void SendTrade(TradeMessageType type, int forCardId)
        {
            Send(new TradeMessage(type, forCardId), MessageContainer.MessageType.Trade);
        }

        public void SendSwap(int firstCardId, int secondCardId)
        {
            Send(new SwapCardMessage(firstCardId, secondCardId), MessageContainer.MessageType.SwapCard);
        }

        public void Send(OrganizationType type, string user) {
            Send(new OrganizationMessage(type, user), MessageContainer.MessageType.Organization);
        }
        #endregion

        #region Data Receiving
        public void StartListener() {
            if (_startedListener) return;
            _startedListener = true;
            Task.Run(() => {
                var crntBuffer = new byte[0];
                var expectedBytes = -1;
                while (true) {
                    try {
                        // Receives data till data isn't available anymore
                        while (_senderSock.Available > 0) {
                            _bytes = new byte[DefaultBfflen];
                            var receivedBytes = _senderSock.Receive(_bytes);
                            crntBuffer = AppendArray(crntBuffer, _bytes, receivedBytes);
                        }

                        if (expectedBytes == -1) {
                            if (crntBuffer.Length < 4) continue;
                            // read the first 4 _bytes as an int32
                            var firstBytes = new byte[4];
                            Array.Copy(crntBuffer, firstBytes, 4);
                            if (!BitConverter.IsLittleEndian)
                                firstBytes = firstBytes.Reverse().ToArray();
                            expectedBytes = BitConverter.ToInt32(firstBytes, 0);
                        }

                        if (crntBuffer.Length < expectedBytes + 4) continue;
                        // read the message (which is expected _bytes long and starts 4 _bytes into the array)
                        var messageBuffer = new byte[expectedBytes];
                        Array.Copy(crntBuffer, 4, messageBuffer, 0, expectedBytes);

                        // set the currentBuffer to the remaining items
                        // allocate new temp array and copy the remaining _bytes into it
                        var newBuffer = new byte[crntBuffer.Length - (expectedBytes + 4)];
                        Array.Copy(crntBuffer, expectedBytes + 4, newBuffer, 0, newBuffer.Length);

                        // then set the current buffer to the temp one
                        crntBuffer = newBuffer;

                        System.Windows.Application.Current.Dispatcher.Invoke(() => {
                            if (BytesReceived != null)
                                BytesReceived(this, new BytesReceivedEventArgs(messageBuffer));
                        });

                        expectedBytes = -1;
                    } catch (Exception e) {
                        if (e.GetType() == typeof(SocketException)) {
                            var socketEx = e as SocketException;
                            if (socketEx != null && socketEx.ErrorCode == 10054) {
                                // server closed connection
                                System.Windows.Application.Current.Dispatcher.Invoke(() => {
                                    ConnectionStatus = ConnectionHandler.ConnectionStatusEnum.Disconnected;
                                    StatusText = "Disconnected (Server closed connection)";
                                });
                                return;
                            }
                        }
                        DebugLogger.GetInstance().LogError("Failed to receive data. Message: " + e.Message);
                        Thread.Sleep(1000);
                    }
                }
            });
        }


        #endregion
        #endregion
    }
}
