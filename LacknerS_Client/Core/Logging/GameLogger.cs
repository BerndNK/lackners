﻿using System;
using System.ComponentModel;
using System.Windows;

namespace LacknerS_Client.Core.Logging {
    public class GameLogger : ILogger{
        /// <summary>
        /// The logged messages
        /// </summary>
        private readonly BindingList<LogMessage> _messages;

        #region Singleton pattern
        /// <summary>
        /// The singleton instance
        /// </summary>
        private static GameLogger _instance;

        /// <summary>
        /// Gets the instance of the DebugLogger.
        /// </summary>
        /// <returns>DebugLogger instance</returns>
        public static GameLogger GetInstance() {
            return _instance ?? (_instance = new GameLogger());
        }

        #endregion

        /// <summary>
        /// Occurs when a new [message is logged].
        /// </summary>
        public event EventHandler<MessageLoggedEventArgs> MessageLogged;

        /// <summary>
        /// Prevents a default instance of the <see cref="DebugLogger"/> class from being created.
        /// </summary>
        private GameLogger() {
            _messages = new BindingList<LogMessage>();
        }


        /// <summary>
        /// Logs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logType">Type of the log message.</param>
        public void Log(string message, LogType logType = LogType.Info) {
            var logMessage = new LogMessage(message, logType);
            Application.Current.Dispatcher.Invoke(() => {
                _messages.Add(logMessage);
                MessageLogged?.Invoke(this, new MessageLoggedEventArgs(logMessage));
            });

        }

        /// <summary>
        /// Gets the logged messages.
        /// </summary>
        /// <returns>
        /// The logged messages
        /// </returns>
        public BindingList<LogMessage> GetLogs() => _messages;
    }
}
