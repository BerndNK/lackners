﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card079HotLackner : UpgradeCard
    {
        public Card079HotLackner(int id, string owner) : base(79, id, owner)
        {
            Rarity = 2;
            Coolness = 0;
        }
    }
}
