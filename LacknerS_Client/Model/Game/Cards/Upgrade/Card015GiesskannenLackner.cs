﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card015GiesskannenLackner : UpgradeCard{
        public Card015GiesskannenLackner(int id, string owner) : base(15, id, owner)
        {
            Rarity = 0;
            Coolness = -2;
        }
    }
}
