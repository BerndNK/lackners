﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card085VorlesungLackner : UpgradeCard
    {
        public Card085VorlesungLackner(int id, string owner) : base(85, id, owner)
        {
            Rarity = 0;
            Coolness = -2;
        }
    }
}
