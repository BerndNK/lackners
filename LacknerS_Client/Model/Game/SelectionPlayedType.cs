﻿using System;

namespace LacknerS_Client.Model.Game
{
    [Flags]
    public enum SelectionPlayedType
    {
        PlayedCard = 1,
        OwnedCard = 2,
        GraveyardCard = 4,
        All = PlayedCard | OwnedCard
    }
}