﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using LacknerS_Client.Core.Logging;

namespace LacknerS_Client.Converter {
    public class LogTypeToBrushConverter : IValueConverter{
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var color = new Color();
            if (value is LogType) {
                switch ((LogType)value) {
                    case LogType.Info:
                        color = Color.FromRgb(0, 96, 255);
                        break;
                    case LogType.Error:
                        color = Color.FromRgb(255, 0, 0);
                        break;
                        case LogType.Network:
                        color = Color.FromRgb(11, 183, 0);
                        break;
                    case LogType.All:
                        color = Colors.Black;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }
            }
            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return null; // not supported
        }
    }
}
