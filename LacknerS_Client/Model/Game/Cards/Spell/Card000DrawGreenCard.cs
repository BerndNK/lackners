﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;
using LacknerS_Client.Views;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    [Serializable]
    public class Card000DrawGreenCard : SpellCard
    {
        private static Card000DrawGreenCard _instance;
        public const int Id = -3;

        public static Card000DrawGreenCard GetInstance()
        {
            if (_instance != null) return _instance;
            return _instance = new Card000DrawGreenCard();
        }

        private Card000DrawGreenCard() : base(-3, Id, null) {
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.PlayPoints = 0;
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
            }

            GameLogger.GetInstance().Log($"{forPlayer.Name} nutzte seinen Overcharged - Lackner um eine grüne Karte zu ziehen.");
        }
    }
}
