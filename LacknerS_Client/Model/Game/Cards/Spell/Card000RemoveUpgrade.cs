﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.ViewModel;
using LacknerS_Client.Views;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    [Serializable]
    public class Card000RemoveUpgrade : SpellCard
    {
        private static Card000RemoveUpgrade _instance;
        public const int Id = -2;

        public static Card000RemoveUpgrade GetInstance()
        {
            if (_instance != null) return _instance;
            return _instance = new Card000RemoveUpgrade();
        }

        private Card000RemoveUpgrade() : base(0, Id, null) {
        }
        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            var selectionTarget = new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.All);

            selectionTarget.SpecialCardCondition = card =>
            {
                var asUpgrade = card as UpgradeCard;
                if (asUpgrade == null) return false;
                var container = GetContainingRareLackner(asUpgrade);
                if(container == null) return false;
                var everyoneCanRemove = container.EveryoneCanRemoveUpgrades;
                var canBeRemoved = asUpgrade.CanBeRegularlyRemoved;
                var owner = GetOwner(asUpgrade, forGame);
                var isOwnedByPlayer = owner == forGame.UserPlayer;

                if (!canBeRemoved) return false;
                if (isOwnedByPlayer || everyoneCanRemove) return true;

                return false;
            };


            return new List<SelectionTarget> { selectionTarget };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var upgradeCard = GetCardById(selectionTargets.First().SelectedCard) as UpgradeCard;
            if (upgradeCard == null) throw new Exception("Id of card could not be found, or parsing failed.");

            var rareLackner = RareLacknerCard.GetContainingRareLackner(upgradeCard);
            rareLackner.Upgrades.Remove(upgradeCard);
            forPlayer.PlayedCards.Remove(upgradeCard);
            activeGame.Graveyard.Add(upgradeCard);


            GameLogger.GetInstance().Log($"{forPlayer.Name} entfernte Upgrade {upgradeCard.Name}");
        }
    }
}
