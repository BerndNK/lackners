﻿using System;
using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Core.Network.Messages.Types {
    /// <summary>
    /// Message sent from a client to inform the host that it's game state has changed.
    /// </summary>
     [Serializable]
    public class GameStateChangedMessage : Message{
        public GameState GameState { get; set; }

        public GameStateChangedMessage(GameState to) {
            GameState = to;
        }

        
    }
}
