﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml;
using LacknerS_Client.Core.Logging;
using MahApps.Metro;
using MahApps.Metro.Controls;

namespace LacknerS_Client.Views
{
    /// <summary>
    /// Description for SettingsWindow.
    /// </summary>
    public partial class SettingsWindow : MetroWindow
    {
        private static SettingsWindow _current;
        private static readonly string SourcePath = Directory.GetCurrentDirectory() + "\\";
        private const string SettingsFileName = "Settings.xml";
        private static readonly string LocalCopyPath = $@"{Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)}\{AppDomain.CurrentDomain.FriendlyName}\";
        private static bool SettingFileChecked = false;
        /// <summary>
        /// Initializes a new instance of the SettingsWindow class.
        /// </summary>
        public SettingsWindow()
        {
            InitializeComponent();
            CheckAndCopySettingsFile();


            settingsXMLDataProvider.Source = new Uri(LocalCopyPath + SettingsFileName);
            _current = this;
        }

        private static void CheckAndCopySettingsFile()
        {
            SettingFileChecked = true;
            // check if source Settings exists
            if (!File.Exists(SourcePath + SettingsFileName))
            {
                DebugLogger.GetInstance().LogError("Settings:Settings - Could not find source \"" + SettingsFileName + "\" in path: \"" + SourcePath + "\"");
                return;
            }

            // if the local copy does not exist, copy the source settings into the local copy path
            if (!File.Exists(LocalCopyPath + SettingsFileName))
            {
                // create folder and copy file
                try
                {
                    Directory.CreateDirectory(LocalCopyPath);
                    File.Copy(SourcePath + SettingsFileName, LocalCopyPath + SettingsFileName);
                }
                catch (Exception e)
                {
                    DebugLogger.GetInstance().LogError("Settings:Settings - Failed to copy Settings file to Application Data. Error: " + e.Message);
                    return;
                }
            }
            else
            {
                // if it DOES exist, check the versions of both the copy and the source
                // load both files
                var sourceDoc = new XmlDocument();
                var copyDoc = new XmlDocument();
                sourceDoc.Load(SourcePath + SettingsFileName);
                copyDoc.Load(LocalCopyPath + SettingsFileName);

                // get root elements of both files
                var sourceRoot = sourceDoc.DocumentElement;
                var copyRoot = copyDoc.DocumentElement;
                if (copyRoot == null || sourceRoot == null || // if one doc failed to load
                    !copyRoot.HasAttribute("Version") || // the copy has no version attribute
                    Convert.ToInt32(sourceRoot.GetAttribute("Version")) > // or the source version is higher than the copy version
                    Convert.ToInt32(copyRoot.GetAttribute("Version")))
                {

                    // create folder and copy file
                    try
                    {
                        Directory.CreateDirectory(LocalCopyPath);
                        File.Delete(LocalCopyPath + SettingsFileName); // delete the file first in order to copy it
                        File.Copy(SourcePath + SettingsFileName, LocalCopyPath + SettingsFileName);
                    }
                    catch (Exception e)
                    {
                        DebugLogger.GetInstance().LogError("Settings:Settings - Failed to copy Settings file to Application Data. Error: " + e.Message);
                        return;
                    }
                }
            }

        }

        public static object GetSetting(string name)
        {
            try
            {
                if (!SettingFileChecked) CheckAndCopySettingsFile();

                XmlDocument doc = new XmlDocument();
                var path = LocalCopyPath + SettingsFileName;
                if (!File.Exists(path)) CheckAndCopySettingsFile();
                doc.Load(path);
                if (doc.DocumentElement == null) return null;
                foreach (var node in from XmlElement node in doc.DocumentElement.ChildNodes
                    where node.Attributes["XName"] != null
                    where string.Equals(node.Attributes["XName"].Value, name, StringComparison.CurrentCultureIgnoreCase)
                    select node)
                {
                    if (node.Attributes["Type"] != null)
                    {
                        if (node.Attributes["Type"].Value == "combo")
                        {
                            return node.Attributes["Values"] == null
                                ? ""
                                : node.Attributes["Values"].Value.Split('|')[
                                    Convert.ToInt32(node.Attributes["Value"].Value)];
                        }
                    }

                    if (node.Attributes["Value"] != null)
                        return node.Attributes["Value"].Value;
                }
            }
            catch (Exception)
            {
                // ignored
                return null;
            }
            return null;
        }

        public void Save()
        {
            string source = settingsXMLDataProvider.Source.LocalPath;
            settingsXMLDataProvider.Document.Save(source);

            try
            {
                ThemeManager.ChangeAppStyle(Application.Current,
                    ThemeManager.GetAccent(SettingsWindow.GetSetting("applicationAccent").ToString()),
                    ThemeManager.GetAppTheme(SettingsWindow.GetSetting("applicationTheme").ToString()));
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void AbortBTN_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ApplyBTN_OnClick(object sender, RoutedEventArgs e)
        {
            Save();
            Close();
        }
    }
}