﻿using System;

namespace LacknerS_Client.Core.Network {
    public class BytesReceivedEventArgs : EventArgs {
        public byte[] Bytes { get; private set; }

        public BytesReceivedEventArgs(byte[] bytes) {
            Bytes = bytes;
        }
    }
}