﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade {
    public class Card017DatenzentrumLackner : UpgradeCard{
        public Card017DatenzentrumLackner(int id, string owner) : base(17, id, owner)
        {
            Rarity = 1;
            Coolness = 2;
        }
    }
}
