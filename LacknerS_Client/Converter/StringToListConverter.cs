﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;
using System.Xml;

namespace LacknerS_Client.Converter {
    public class StringToListStringConverter : IValueConverter {
        public object Convert(object value, Type targetType,
            object parameter, CultureInfo culture) {
            Console.WriteLine(value.GetType().ToString());
            var asCollection = value as ICollection;
            string format = null;
            if (asCollection == null) return null;
            foreach (XmlAttribute crnt in asCollection) {
                format = crnt.Value;
            }

            if (string.IsNullOrEmpty(format)) {
                return new List<string>();
            } else {
                var splitted = format.Split('|');
                var asList = new List<string>(splitted);
                return asList;
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
