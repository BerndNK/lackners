﻿using System.Collections.Generic;
using System.Linq;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell
{
    public class Card071VerlaufenLackner : SpellCard
    {
        public Card071VerlaufenLackner(int id, string owner) : base(71, id, owner)
        {
            Power = SpellCardPower.High;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget>
            {
                new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.Enemy)
            };
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var card = (RareLacknerCard)GetCardById(selectionTargets.First().SelectedCard);
            var owner = GetOwner(card, activeGame);
            

            // destroy all upgrades
            foreach (var upgrade in card.Upgrades)
            {
                activeGame.Graveyard.Add(upgrade);
            }
            card.Upgrades.Clear();

            owner.PlayedRareLackner.Remove(card);
            owner.PlayedCards.Remove(card);

            forPlayer.PlayedCards.Add(card);
            forPlayer.PlayedRareLackner.Add(card);
            
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}