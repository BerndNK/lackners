﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card040SchadenfreudeLackner : UpgradeCard
    {
        public Card040SchadenfreudeLackner(int id, string owner) : base(40, id, owner)
        {
            Rarity = -1;
            Coolness = -1;
            CanBeRegularlyRemoved = false;
        }
    }
}
