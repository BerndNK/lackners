#ifndef _SOCKET_HANDLER_WINDOWS_H_
#define _SOCKET_HANDLER_WINDOWS_H_

#define WIN32_LEAN_AND_MEAN

#include "SocketHandler.h"

#include <winsock2.h>
#include <stdlib.h>
#include <ws2tcpip.h>
#include <stdio.h>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

namespace network {
	class SocketHandlerWindows : public SocketHandler {
	public:
		SocketHandlerWindows(std::string  port = DEFAULT_PORT);
		SocketHandlerWindows(const SocketHandlerWindows& socketHandler) = delete;
		~SocketHandlerWindows();

		int Initialize() override;

	private:
		WSADATA m_wsaData;
		SOCKET m_listenSocket = INVALID_SOCKET;

	protected:
		void Listener(bool* waitingForClientBool, Client* thisClientPtr) override;
		void Sender(Client* thisClientPtr, void* socket, bool* forceQuitBool) override;
		void ListenerHttp(bool* waitingForClientBool, Client* thisClientPtr) override;
	};
}
#endif
