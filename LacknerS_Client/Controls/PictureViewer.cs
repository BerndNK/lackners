﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;

namespace LacknerS_Client.Controls {

    [TemplatePart(Name = "PART_ImageBorder", Type = typeof(Border))]
    [TemplatePart(Name = "PART_BigViewPopup", Type = typeof(Popup))]
    [TemplatePart(Name = "PART_BigViewImage", Type = typeof(Image))]
    [TemplatePart(Name = "PART_Button", Type = typeof(Button))]
    public class PictureViewer : Control {
        #region DependencyProperties

        public static readonly DependencyProperty ImageSourceProperty = DependencyProperty.Register(
            "ImageSource", typeof(ImageSource), typeof(PictureViewer), new PropertyMetadata(default(ImageSource)));

        public ImageSource ImageSource {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public static readonly DependencyProperty IsClickableProperty = DependencyProperty.Register(
            "IsClickable", typeof(bool), typeof(PictureViewer), new PropertyMetadata(default(bool), PropertyChangedCallback));

        private static void PropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs) {
            var asPictureViewer = dependencyObject as PictureViewer;
            if (asPictureViewer == null) return;
            switch (dependencyPropertyChangedEventArgs.Property.Name) {
                case nameof(IsClickable):
                    if (asPictureViewer._imageBorder != null) {
                        if ((bool)dependencyPropertyChangedEventArgs.NewValue) {
                            asPictureViewer._imageBorder.BorderThickness = new Thickness(3);
                            asPictureViewer._imageBorder.BorderBrush = new SolidColorBrush(Colors.White);
                        } else asPictureViewer._imageBorder.BorderThickness = new Thickness(0);
                    }
                    break;
                default:
                    throw new Exception();
            }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(
            "Command", typeof(ICommand), typeof(PictureViewer), new PropertyMetadata(default(ICommand)));

        public ICommand Command {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public bool IsClickable {
            get { return (bool)GetValue(IsClickableProperty); }
            set { SetValue(IsClickableProperty, value); }
        }

        #endregion
        #region Fields
        private Border _imageBorder;
        private Popup _bigViewPopup;
        private Button _button;
        #endregion
        #region Properties

        #endregion

        static PictureViewer() {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PictureViewer), new FrameworkPropertyMetadata(typeof(PictureViewer)));
        }

        public override void OnApplyTemplate() {
            _imageBorder = GetTemplateChild("PART_ImageBorder") as Border;
            /*
            Debug.Assert(_imageBorder != null);
            Debug.Assert(_bigViewPopup != null);
            Debug.Assert(_bigViewImage != null);
            Debug.Assert(_button != null);*/
            if (_imageBorder != null) {
                _imageBorder.MouseEnter += ImageBorderOnMouseEnter;
                _imageBorder.MouseLeave += ImageBorderOnMouseLeave;
                _imageBorder.MouseMove += ImageBorderOnMouseMove;
                _imageBorder.MouseDown += ImageBorderOnMouseDown;
            }

            base.OnApplyTemplate();

            if (_imageBorder != null && IsClickable) {
                _imageBorder.BorderBrush = new SolidColorBrush(Colors.White);
                _imageBorder.BorderThickness = new Thickness(3);
            }
        }

        private void ImageBorderOnMouseDown(object sender, MouseButtonEventArgs mouseButtonEventArgs) {
            if (IsClickable) Command?.Execute(this);
        }


        private void ImageBorderOnMouseMove(object sender, MouseEventArgs mouseEventArgs) {
            if (_bigViewPopup == null) return;
            return;
            var mousePos = System.Windows.Forms.Control.MousePosition;
            _bigViewPopup.HorizontalOffset = -20;
            _bigViewPopup.VerticalOffset = -20;
            _bigViewPopup.Placement = PlacementMode.Mouse;
            _bigViewPopup.IsOpen = true;
            _bigViewPopup.HorizontalOffset = -50;
            _bigViewPopup.VerticalOffset = 50;
            _bigViewPopup.VerticalOffset += 1;
            _bigViewPopup.VerticalOffset -= 1;
            _bigViewPopup.Placement = PlacementMode.Mouse;
            mouseEventArgs.Handled = false;
        }

        private void ImageBorderOnMouseLeave(object sender, MouseEventArgs mouseEventArgs)
        {
            DeSpawnPopUp();
            mouseEventArgs.Handled = false;
            _imageBorder.BorderBrush = new SolidColorBrush(Colors.White);
            if (!IsClickable) _imageBorder.BorderThickness = new Thickness(0);
        }

        private void SpawnPopUp() {
            if (_bigViewPopup != null) return;
            _bigViewPopup = new Popup();

            var img = new Image();
            img.Source = ImageSource;
            img.HorizontalAlignment = HorizontalAlignment.Center;
            img.VerticalAlignment = VerticalAlignment.Top;
            img.Width = 450;
            img.IsHitTestVisible = false;

            _bigViewPopup.Placement = PlacementMode.Left;
            _bigViewPopup.HorizontalOffset = -20;
            _bigViewPopup.Child = img;
            _bigViewPopup.PlacementTarget = _imageBorder;
            _bigViewPopup.IsOpen = true;
            _bigViewPopup.IsHitTestVisible = false;
            img.IsHitTestVisible = false;
            if (IsClickable) {
                _imageBorder.BorderThickness = new Thickness(3);
                _imageBorder.BorderBrush = new SolidColorBrush(Colors.Red);
            }
        }

        private void ImageBorderOnMouseEnter(object sender, MouseEventArgs mouseEventArgs) {
            SpawnPopUp();
            mouseEventArgs.Handled = false;
        }

        private void DeSpawnPopUp() {
            if (_bigViewPopup == null) return;
            _bigViewPopup.IsOpen = false;
            _bigViewPopup = null;
        }
    }
}
