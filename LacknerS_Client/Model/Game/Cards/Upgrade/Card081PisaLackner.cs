﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card081PisaLackner : UpgradeCard
    {
        public Card081PisaLackner(int id, string owner) : base(81, id, owner)
        {
            Rarity = 1;
            Coolness = 1;
        }
    }
}
