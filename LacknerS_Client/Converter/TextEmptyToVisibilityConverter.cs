﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace LacknerS_Client.Converter {
    public class TextEmptyToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var str = value.ToString();
            var whiteSpace = String.IsNullOrWhiteSpace(str);
            return whiteSpace ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
