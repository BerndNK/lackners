﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Core.Network.Messages.Types {
    [Serializable]
    public class SwapCardMessage : Message{
        public SwapCardMessage(int firstCardId, int secondCardId)
        {
            FirstCard = firstCardId;
            SecondCard = secondCardId;
        }

        public int FirstCard { get; set; }

        public int SecondCard { get; set; }
    }
}
