﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Interaction logic for SelectionPanel.xaml
    /// </summary>
    public partial class SelectionPanel : UserControl, INotifyPropertyChanged {
        private bool _inHostMode;
        private bool _inGameMode;

        public bool InHostMode {
            get { return _inHostMode; }
            set {
                _inHostMode = value;
                OnPropertyChanged();
            }
        }

        public bool InGameMode {
            get { return _inGameMode; }
            set {
                _inGameMode = value;
                OnPropertyChanged();
            }
        }

        public static ClientMode SelectedMode {
            get; private set;
        }

        public SelectionPanel() {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnPlayerBtn_Click(object sender, RoutedEventArgs e) {

            InGameMode = true;
            SelectedMode = ClientMode.Game;
        }

        private void OnHostBtn_Click(object sender, RoutedEventArgs e) {
            InHostMode = true;
            SelectedMode = ClientMode.Host;
        }
    }

    public enum ClientMode {
        Waiting = 0,
        Game,
        Host
    }
}
