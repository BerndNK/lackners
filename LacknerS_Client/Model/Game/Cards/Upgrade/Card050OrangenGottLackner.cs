﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card050OrangenGottLackner : UpgradeCard
    {
        public Card050OrangenGottLackner(int id, string owner) : base(50, id, owner)
        {
            Rarity = 2;
            Coolness = 2;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.TurnSkips++;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
