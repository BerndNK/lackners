﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card068KlempnerLackner : RareLacknerCard
    {
        public Card068KlempnerLackner(int id, string owner) : base(68, id, owner)
        {
            Rarity = 4;
            Coolness = 4;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.CoolnessNeededModifier -= 1;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            forPlayer.CoolnessNeededModifier += 1;
            GameLogger.GetInstance().Log($"Klempner - Lackner zerstört! {forPlayer.Name} braucht wieder 1 Coolness mehr für Zauber.");
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
