﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card027DealerLackner : RareLacknerCard
    {
        public Card027DealerLackner(int id, string owner) : base(27, id, owner)
        {
            Rarity = 4;
            Coolness = 5;
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawGreenCard, forPlayer.Name);
                // also let every player draw a yellor card
                foreach (var player in activeGame.AllPlayer)
                {
                    ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, player.Name);
                }
            }

            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
