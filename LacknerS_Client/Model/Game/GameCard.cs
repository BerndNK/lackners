﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows.Input;
using LacknerS_Client.Model.Game.Cards;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game {
    public abstract class GameCard : INotifyPropertyChanged {
        private bool _isClickable;
        private ICommand _clickedCommmand;
        private CardType _cardType;
        private string _owner;
        private int _id;
        private int _rarity;
        private int _coolness;
        private int _neededPoints;
        private bool _isRevealed;
        private string _lastPlayedBy = "";

        /// <summary>
        /// Gets the number of the card.
        /// </summary>
        /// <value>
        /// The no.
        /// </value>
        public int No { get; private set; }

        public int Id {
            get { return _id; }
            set {
                _id = value;
                OnPropertyChanged();
            }
        }

        public string ImagePath => GetImagePath();

        public string Owner {
            get { return _owner; }
            set {
                _owner = value;
                OnPropertyChanged();
            }
        }

        public CardType CardType {
            get { return _cardType; }
            protected set {
                _cardType = value;
                OnPropertyChanged();
            }
        }

        public ICommand ClickedCommmand {
            get { return _clickedCommmand; }
            set {
                _clickedCommmand = value;
                OnPropertyChanged();
            }
        }

        public bool IsClickable {
            get { return _isClickable; }
            set {
                _isClickable = value;
                OnPropertyChanged();
            }
        }


        public int Rarity {
            get { return _rarity; }
            set {
                _rarity = value;
                OnPropertyChanged();
            }
        }

        public int Coolness {
            get { return _coolness; }
            set {
                _coolness = value;
                OnPropertyChanged();
            }
        }

        public int NeededPoints
        {
            get { return _neededPoints; }
            set
            {
                _neededPoints = value; 
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get
            {
                var derivedClass = GetType().FullName.Split('.').Last();
                return new Regex("(?<=\\d)([A-Za-z])+").Match(derivedClass).Value;
            }
        }

        public string SetName { get; set; }

        public bool IsRevealed
        {
            get { return _isRevealed; }
            set
            {
                _isRevealed = value; 
                OnPropertyChanged();
                OnPropertyChanged(nameof(ImagePath));
                // force updating of the width and height of this card
                OnPropertyChanged("Width");
                OnPropertyChanged("Height");
            }
        }

        public bool GetsRevealed { get; set; } = true;

        public string LastPlayedBy
        {
            get { return _lastPlayedBy; }
            set
            {
                _lastPlayedBy = value; 
                OnPropertyChanged();
            }
        }

        public string Description { get; set; }
        public bool IsUltraRare { get; set; }

        /// <summary>
        /// List of all cards every instanced
        /// </summary>
        private static List<GameCard> _allCards;
        protected GameCard(int no, CardType cardType, int id, string owner) {
            No = no;
            Id = id;
            Owner = owner;
            CardType = cardType;
            if(_allCards == null) _allCards = new List<GameCard>();
            _allCards.Add(this);
        }

        public static GameCard GetCardById(int id)
        {
            return _allCards.First(x => x.Id == id);
        }

        public static RareLacknerCard GetContainingRareLackner(UpgradeCard upgrade)
        {
            return _allCards.FirstOrDefault(x => (x as RareLacknerCard) != null && ((RareLacknerCard) x).Upgrades.Contains(upgrade)) as RareLacknerCard;
        }

        public static Player GetOwner(GameCard card, ActiveGameViewModel forGame)
        {
            var owner = forGame.AllPlayer.FirstOrDefault(x => x.Cards.Contains(card));
            if (owner != null) return owner;
            return forGame.AllPlayer.First(x => x.PlayedCards.Contains(card));
        }

        private string GetImagePath() {
            

            // check all pictures in the resources folder
            var filepath = AppDomain.CurrentDomain.BaseDirectory + "Resources\\";
            if (!IsRevealed)
            {
                return "../Resources/CardBack.png";
            }
            var d = new DirectoryInfo(filepath);

            foreach (var file in d.GetFiles("*.png")) {
                var name = file.Name;
                var number = name.Split('_').First();
                var noInString = No.ToString();
                // set the number to format XXX (so if it's a one, add two zeros)
                while (noInString.Length < 3) noInString = "0" + noInString;

                if (number == noInString) {
                    return file.FullName;
                }
            }
            return "";
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public abstract List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame);

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Activates the specified active game. (Plays it)
        /// </summary>
        /// <param name="activeGame">The active game.</param>
        /// <param name="selectionTargets"></param>
        /// <param name="forPlayer"></param>
        /// <exception cref="NotImplementedException"></exception>
        public abstract void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer);

        /// <summary>
        /// Determines whether [a special requirement is fulfilled] for [the specified active game] to play this card.
        /// </summary>
        /// <param name="activeGame">The active game.</param>
        /// <returns>
        ///   <c>true</c> if [special requirement is fulfilled] for [the specified active game]; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsSpecialRequirementFulfilled(ActiveGameViewModel activeGame)
        {
            return true;
        }

        /// <summary>
        /// Executes an action which shall reverse the effect of the Activate method, if applicable. Gets called whenever a card is removed from the field.
        /// </summary>
        /// <param name="activeGame">The active game.</param>
        /// <param name="forPlayer">For player.</param>
        public virtual void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            
        }
    }
}
