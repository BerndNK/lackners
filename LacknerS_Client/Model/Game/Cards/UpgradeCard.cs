﻿using System.Collections.Generic;
using System.Linq;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards {
    public  class UpgradeCard : GameCard{
        public UpgradeCard(int no, int id, string owner) : base(no, CardType.Upgrade, id, owner)
        {
            NeededPoints = 1;
        }

        public bool CanBeRegularlyRemoved { get; set; } = true;


        /// <summary>
        /// Activates the specified active game. (Plays it)
        /// </summary>
        /// <param name="activeGame">The active game.</param>
        /// <param name="selectionTargets"></param>
        /// <param name="forPlayer"></param>
        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var card = GameCard.GetCardById(selectionTargets.First().SelectedCard);
            (card as RareLacknerCard)?.Upgrades.Add(this);
            var owner = GetOwner(card, activeGame);
            owner.PlayedCards.Add(this);
            forPlayer.Cards.Remove(this);
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            // special condition: make sure the max. amount of upgrades is not reached
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.RareLackner, OwnerType.All,
                delegate(GameCard x)
                {
                    var maxUpgradesReached = ((RareLacknerCard) x).Upgrades.Count >= ((RareLacknerCard) x).MaxUpgradeCount;
                    var onlyOwnerCanGiveUpgrades = ((RareLacknerCard) x).OnlyOwnerCanGiveUpgrades;
                    var currentPlayerIsOwner = x.Owner == ActiveGameViewModel.Current.UserPlayer.Name;

                    if (maxUpgradesReached) return false;
                    if (onlyOwnerCanGiveUpgrades && !currentPlayerIsOwner) return false;

                    return true;
                })  };
        }

        public override string ToString()
        {
            return $"Rare: {Rarity} Cool: {Coolness}";
        }
    }
}
