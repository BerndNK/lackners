﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card039EremitLackner : RareLacknerCard
    {
        public Card039EremitLackner(int id, string owner) : base(39, id, owner)
        {
            Rarity = 6;
            Coolness = 1;
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.PointsNeededForWin -= 2;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }

        public override void CardRemoved(ActiveGameViewModel activeGame, Player forPlayer)
        {
            forPlayer.PointsNeededForWin += 2;
            GameLogger.GetInstance().Log($"Eremit - Lackner zerstört. {forPlayer.Name} braucht wieder 2 Rarität mehr zum Spielsieg.");
            base.CardRemoved(activeGame, forPlayer);
        }
    }
}
