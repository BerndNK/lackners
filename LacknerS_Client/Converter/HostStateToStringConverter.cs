﻿using System;
using System.Globalization;
using System.Windows.Data;
using LacknerS_Client.Model.Host;

namespace LacknerS_Client.Converter {
    public class HostStateToStringConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var asEnum = (HostState)value;
            switch (asEnum) {
                case HostState.PollingClients:
                    return "Sending requests for clients to register themselves...";
                case HostState.RegistrationComplete:
                    return "Registration complete.";
                default:
                    return "Error";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
