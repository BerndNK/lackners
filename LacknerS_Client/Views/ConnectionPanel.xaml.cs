﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.Core.Network;
using LacknerS_Client.ViewModel;
using MahApps.Metro;
using SimpleTalkClient.Shared.Utils;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    public partial class ConnectionPanel : INotifyPropertyChanged {
        #region DependencyProperties
        /// <summary>
        /// Global username. Set from the username textBox. Standard value is the username found in settings.xml
        /// </summary>
        public static string Username { get; private set; }
        public ConnectionHandler ConnectionHandler => ConnectionHandler.GetInstance();
    
        #endregion


        #region Fields
        private ImageSource _onlineStatusImageSource;
        private string _onlineStatusImagePath = "pack://application:,,,/LacknerS_Client;component/Resources/icons8-Online-96.png";

        private ImageSource _logIconImageSource;
        private string _logIconImagePath = "pack://application:,,,/LacknerS_Client;component/Resources/icons8-Bulleted List-96.png";

        private ImageSource _optionsIconImageSource;
        private string _optionsIconImagePath = "pack://application:,,,/LacknerS_Client;component/Resources/icons8-Settings-96.png";

        private ImageSource _retryIconImageSource;
        private string _retryIconImagePath = "pack://application:,,,/LacknerS_Client;component/Resources/icons8-Synchronize-96.png";

        private SettingsWindow _settings;
        private string _proxyAddress;
        private string _proxyDomain;
        private string _proxyPassword;
        private string _proxyUsername;
        private bool _logsVisible;

        #endregion

        #region Properties
        public ImageSource OnlineStatusImageSource {
            get { return _onlineStatusImageSource; }
            set {
                _onlineStatusImageSource = value;
                CallPropertyChanged("OnlineStatusImageSource");
            }
        }

        public string StatusText {
            get { return ConnectionHandler == null ? "" : ConnectionHandler.StatusText; }
        }

        public ImageSource LogIconImageSource {
            get { return _logIconImageSource; }
            set {
                _logIconImageSource = value;
                CallPropertyChanged("LogIconImageSource");
            }
        }

        public ImageSource RetryIconImageSource {
            get { return _retryIconImageSource; }
            set {
                _retryIconImageSource = value;
                CallPropertyChanged("RetryIconImageSource");
            }
        }

        public ImageSource OptionsIconImageSource {
            get { return _optionsIconImageSource; }
            set {
                _optionsIconImageSource = value;
                CallPropertyChanged("OptionsIconImageSource");
            }
        }

        public string ProxyAddress {
            get { return _proxyAddress; }
            set {
                _proxyAddress = value;
                if (ConnectionHandler != null) ConnectionHandler.ProxyAdddress = value;
                CallPropertyChanged("ProxyAddress");
            }
        }

        public string ProxyUsername {
            get { return _proxyUsername; }
            set {
                _proxyUsername = value;
                if (ConnectionHandler != null) ConnectionHandler.ProxyUsername = value;
                CallPropertyChanged("ProxyUsername");
            }
        }

        public string ProxyPassword {
            get { return _proxyPassword; }
            set {
                _proxyPassword = value;
                if (ConnectionHandler != null) ConnectionHandler.ProxyPassword = value;
                CallPropertyChanged("ProxyPassword");
            }
        }

        public string ProxyDomain {
            get { return _proxyDomain; }
            set {
                _proxyDomain = value;
                if (ConnectionHandler != null) ConnectionHandler.ProxyDomain = value;
                CallPropertyChanged("ProxyDomain");
            }
        }

        public bool LogsVisible {
            get { return _logsVisible; }
            set {
                _logsVisible = value; 
                CallPropertyChanged(nameof(LogsVisible));
            }
        }

        public bool IsConnected
            => ConnectionHandler.ConnectionStatus == ConnectionHandler.ConnectionStatusEnum.Connected;

        #endregion
        public ConnectionPanel() {
            try {
                OnlineStatusImageSource = new ImageSourceConverter().ConvertFromString(_onlineStatusImagePath) as ImageSource;
                OnlineStatusImageSource = ColorManipulator.ChangeColor(OnlineStatusImageSource, Color.FromRgb(200, 30, 30));
            } catch (Exception) {
                DebugLogger.GetInstance().LogError("Failed to create Image Source from path. Check \"OnlineStatusImagePath\"");
            }
            UpdateIcons();

            InitializeComponent();


            ThemeManager.IsThemeChanged += (sender, args) => UpdateIcons();

            Loaded += delegate (object sender, RoutedEventArgs args) {

                DnsTb.Text = SettingsWindow.GetSetting("standardaddress").ToString();
                PortTb.Text = SettingsWindow.GetSetting("standardport").ToString();
                UsernameTb.Text = SettingsWindow.GetSetting("username").ToString();
#if DEBUG
                UsernameTb.Text = new Random().Next().ToString();
#endif

                /* ProxyTb.Text = SettingsWindow.GetSetting("standardproxy").ToString();
                 ProxyNameTb.Text = SettingsWindow.GetSetting("standardproxyusername").ToString();
                 ProxyPasswordTb.Text = SettingsWindow.GetSetting("standardproxypassword").ToString();
                 ProxyDomainTb.Text = SettingsWindow.GetSetting("standardproxydomain").ToString();*/
                 
                 RetryBtn_OnClick(null, null);
            };
            ConnectionHandler.PropertyChanged += ConnectionHandlerOnPropertyChanged;
        }

        private void ConnectionHandlerOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "StatusText") CallPropertyChanged("StatusText");
            if (e.PropertyName == "ConnectionStatus") UpdateConnectionStatusBlip();
            CallPropertyChanged(nameof(IsConnected));
        }

        private void UpdateIcons() {
            try {
                LogIconImageSource = new ImageSourceConverter().ConvertFromString(_logIconImagePath) as ImageSource;
                var labelTextBrush = (FindResource("LabelTextBrush") as SolidColorBrush);
                if (labelTextBrush != null) LogIconImageSource = ColorManipulator.ChangeColor(LogIconImageSource, labelTextBrush.Color);
            } catch (Exception) {
                DebugLogger.GetInstance().LogError("Failed to create Image Source from path. Check \"LogIconImageSource\"");
            }

            try {
                OptionsIconImageSource = new ImageSourceConverter().ConvertFromString(_optionsIconImagePath) as ImageSource;
                var labelTextBrush = (FindResource("LabelTextBrush") as SolidColorBrush);
                if (labelTextBrush != null) OptionsIconImageSource = ColorManipulator.ChangeColor(OptionsIconImageSource, labelTextBrush.Color);
            } catch (Exception) {
                DebugLogger.GetInstance().LogError("Failed to create Image Source from path. Check \"OptionsIconImageSource\"");
            }

            try {
                RetryIconImageSource = new ImageSourceConverter().ConvertFromString(_retryIconImagePath) as ImageSource;
                var labelTextBrush = (FindResource("LabelTextBrush") as SolidColorBrush);
                if (labelTextBrush != null) RetryIconImageSource = ColorManipulator.ChangeColor(RetryIconImageSource, labelTextBrush.Color);
            } catch (Exception) {
                DebugLogger.GetInstance().LogError("Failed to create Image Source from path. Check \"RetryIconImageSource\"");
            }
        }

        private void UpdateConnectionStatusBlip() {
            if (ConnectionHandler == null) return;
            if (OnlineStatusImageSource == null) return;
            OnlineStatusImageSource = new ImageSourceConverter().ConvertFromString(_onlineStatusImagePath) as ImageSource;
            switch (ConnectionHandler.ConnectionStatus) {
                case ConnectionHandler.ConnectionStatusEnum.Disconnected:
                    OnlineStatusImageSource = ColorManipulator.ChangeColor(OnlineStatusImageSource, Color.FromRgb(200, 30, 30));
                    break;
                case ConnectionHandler.ConnectionStatusEnum.Connecting:
                    OnlineStatusImageSource = ColorManipulator.ChangeColor(OnlineStatusImageSource, Color.FromRgb(200, 200, 30));
                    break;
                case ConnectionHandler.ConnectionStatusEnum.Connected:
                    OnlineStatusImageSource = ColorManipulator.ChangeColor(OnlineStatusImageSource, Color.FromRgb(30, 200, 30));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #region INotifyPropertyChangedImplementation

        private void CallPropertyChanged(string on) {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        private void OptionsBtn_OnClick(object sender, RoutedEventArgs e) {
            if (_settings == null) {
                _settings = new SettingsWindow();
                _settings.Closed += (o, args) => _settings = null;
                _settings.Show();
            } else if (_settings.IsActive && _settings.IsLoaded) _settings.Activate();
        }

        private void LogBtn_OnClick(object sender, RoutedEventArgs e) {
            LogsVisible = !LogsVisible;
        }

        private void RetryBtn_OnClick(object sender, RoutedEventArgs e) {
            if (String.IsNullOrWhiteSpace(UsernameTb.Text)) return;
            Username = UsernameTb.Text;
            ActiveGameViewModel.Current.UserPlayer.Name = Username;
            try {
                 ConnectionHandler.Connect(DnsTb.Text, Convert.ToInt32(PortTb.Text));
            } catch (Exception ex) {
                DebugLogger.GetInstance().LogError("Failed to connect. Message: " + ex.Message);
            }
        }
    }
}
