﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card034BergsteigerLackner : RareLacknerCard
    {
        public Card034BergsteigerLackner(int id, string owner) : base(34, id, owner)
        {
            Rarity = 5;
            Coolness = 6;

        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            forPlayer.TurnSkips++;
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
