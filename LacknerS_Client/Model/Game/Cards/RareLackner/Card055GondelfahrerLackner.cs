﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Network;
using LacknerS_Client.Core.Network.Messages.Types;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.RareLackner
{
    public class Card055GondelfahrerLackner : RareLacknerCard
    {
        public Card055GondelfahrerLackner(int id, string owner) : base(55, id, owner)
        {
            Rarity = 4;
            Coolness = 5;
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            // draw a card if the forPlayer is the UserPlayer
            if (forPlayer.Name == activeGame.UserPlayer.Name)
            {
                ConnectionHandler.GetInstance().SendOrgaMessage(OrganizationType.DrawYellowCard, forPlayer.Name);
            }
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
