﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.Upgrade
{
    public class Card041TopDownLackner : UpgradeCard
    {
        public Card041TopDownLackner(int id, string owner) : base(41, id, owner)
        {
            Rarity = 2;
            Coolness = 0;
        }
    }
}
