﻿using System;
using System.Windows;
using System.Windows.Media;

namespace LacknerS_Client.Core.Network.Messages.Types {
    [Serializable]
    public class TextMessage : Message {
        #region Fields
        private string _text;
        private bool _isGreenText;

        #endregion

        #region Properties

        public string Text {
            get { return _text; }
            set {
                _text = value;
                if (!string.IsNullOrWhiteSpace(value)) IsGreenText = value[0] == '>';
                CallPropertyChanged("Text");
            }
        }

        public bool IsGreenText {
            get { return _isGreenText; }
            private set {
                _isGreenText = value;
                BaseOpacity = value ? 0 : 1;
                CallPropertyChanged("IsGreenText");
            }
        }

        public SolidColorBrush TextColorBrush {
            get {
                return IsGreenText ? new SolidColorBrush(Color.FromRgb(120, 153, 34)) : Application.Current.FindResource("LabelTextBrush") as SolidColorBrush;
            }
        }
        #endregion

        public TextMessage(string text) {
            Text = text;
        }
    }
}
