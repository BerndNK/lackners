﻿using System.Collections.Generic;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell {
    public class Card100ZerstörerDerWelten : SpellCard{
        public Card100ZerstörerDerWelten(int id, string owner) : base(100, id, owner)
        {
            Power = SpellCardPower.High;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            var selectionTarget = new SelectionTarget(SelectionPlayedType.OwnedCard, CardType.AllPlayable, OwnerType.Player);
            selectionTarget.SpecialGameCondition = game => game.UserPlayer.Cards.Count >= 1;
            return new List<SelectionTarget>
            {
                selectionTarget
                /*,
                new SelectionTarget(SelectionPlayedType.OwnedCard, CardType.All, OwnerType.Player)*/

            };
        }


        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            foreach (var player in activeGame.AllPlayer)
            {
                foreach (var rareLacknerCard in player.PlayedRareLackner)
                {
                    rareLacknerCard.Upgrades.Clear();
                }
            }
            foreach (var selectionTarget in selectionTargets)
            {
                var card = GetCardById(selectionTarget.SelectedCard);
                GetOwner(card, activeGame).Cards.Remove(card);
            }
            GameLogger.GetInstance().Log($"{Name} zerstörte die Upgrades aller RareLackner.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
