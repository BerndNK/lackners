﻿using System;
using System.Globalization;
using System.Windows.Data;
using LacknerS_Client.Model.Game;

namespace LacknerS_Client.Converter {
    public class GameStateToHostStringConverter :IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var asEnum = (GameState)value;
            switch (asEnum) {
                case GameState.WaitingForHostRequest:
                    return ""; // the server should never see this game state
                case GameState.SentResponse:
                    return "Waiting for confirmation.";
                case GameState.HostHasConfirmed:
                    return "Client is ready.";
                default:
                    return asEnum.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
