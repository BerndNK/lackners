﻿using System.Windows;
using System.Windows.Controls;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Description for GameView.
    /// </summary>
    public partial class GameView : UserControl {

        public static ActiveGame CurrentActiveGame { get; private set; }

        /// <summary>
        /// Initializes a new instance of the GameView class.
        /// </summary>
        public GameView() {
            InitializeComponent();
            CurrentActiveGame = ActiveGame;
        }
    }
}