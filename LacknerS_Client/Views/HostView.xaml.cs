﻿using System.Windows;
using System.Windows.Controls;

namespace LacknerS_Client.Views {
    /// <summary>
    /// Description for HostView.
    /// </summary>
    public partial class HostView : UserControl {
        /// <summary>
        /// Initializes a new instance of the HostView class.
        /// </summary>
        public HostView() {
            InitializeComponent();
        }
    }
}