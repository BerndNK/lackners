﻿namespace LacknerS_Client.Core.Network.Messages {
    public enum TradeMessageType {
        Offer,
        OfferResponse,
        AcceptTrade,
        RefuseTrade,
        CancelOffer
    }
}