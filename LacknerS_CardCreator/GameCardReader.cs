﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using LacknerS_Client.Model.Game;
using LacknerS_Client.Model.Game.Cards;
using Microsoft.Office.Interop.Excel;

namespace LacknerS_CardCreator
{
    class GameCardReader
    {
        class Columns
        {
            public const char No = 'B';
            public const char Type = 'F';
            public const char Name = 'G';
            public const char Rarity = 'I';
            public const char Coolness = 'J';
            public const char SubText = 'O';
            public const char MainText = 'P';
            public const char Description = 'Q';
            public const char SpellPower = 'M';
        }


        public IEnumerable<GameCard> GetAllCardsThroughExcel(string excelPath)
        {
            Range excelRange;
            var app = new Application();
            Workbook wb;
            Worksheet sheet;
            try
            {
                wb = app.Workbooks.Open(excelPath);

                sheet = (Worksheet)wb.Sheets["Sheet1"];
                excelRange = sheet.UsedRange;

            }
            catch (Exception)
            {
                app.Quit();
                yield break;
            }

            foreach (Range row in excelRange.Rows)
            {
                var card = GetCardThroughRow(row);
                if (card != null)
                    yield return card;
            }
            
            app.Quit();
            Marshal.ReleaseComObject(wb);
            Marshal.ReleaseComObject(excelRange);
            Marshal.ReleaseComObject(sheet);
            Marshal.ReleaseComObject(app);
            wb = null;
            excelRange = null;
            sheet = null;
            app = null;
            GC.Collect();
        }

        private GameCard GetCardThroughRow(Range row)
        {
            GameCard newCard;
            var type = TextOfCell(row, Columns.Type);
            var number = NumberOfCell(row, Columns.No);
            switch (type)
            {
                case "RareLackner":
                    newCard = new RareLacknerCard(number, number, null);
                    break;
                case "Upgrade":
                    newCard = new UpgradeCard(number, number, null);
                    break;
                case "Spell":
                    var spellCard = new SpellCard(number, number, null);
                    spellCard.Power = SpellPower(row);
                    newCard = spellCard;
                    break;
                case "Special":
                    newCard = new SpecialCard(number, number, null);
                    break;
                default:
                    return null;
            }

            newCard.Coolness = NumberOfCell(row, Columns.Coolness);
            newCard.Rarity = NumberOfCell(row, Columns.Rarity);
            newCard.SetName = TextOfCell(row, Columns.Name);
            newCard.Description = TextOfCell(row, Columns.Description);
            if (newCard.No == 1 || newCard.No % 25 == 0 && newCard.No != 0)
                newCard.IsUltraRare = true;
            //SplitDescription(row, newCard.Description);

            return newCard;
        }
        
        private List<string> _boldWords { get; } = new List<string> { "UPGRADE:", "CAST:", "EFFEKT:" };
        private void SplitDescription(Range row, string newCardDescription)
        {
            foreach (var boldWord in _boldWords)
            {
                var index = newCardDescription.IndexOf(boldWord, StringComparison.InvariantCultureIgnoreCase);
                if (index != -1)
                {
                    var mainText = newCardDescription.Substring(0, index);
                    var subText = newCardDescription.Substring(index);
                    SetTextOfCell(row, Columns.MainText, mainText);
                    SetTextOfCell(row, Columns.SubText, subText);
                    return;
                }
            }
            SetTextOfCell(row, Columns.MainText, newCardDescription);
            SetTextOfCell(row, Columns.SubText, "");
        }

        private SpellCardPower SpellPower(Range row)
        {
            var asInt = NumberOfCell(row, Columns.SpellPower);
            switch (asInt)
            {
                case 1:
                    return SpellCardPower.Low;
                case 2:
                    return SpellCardPower.Middle;
                default:
                    return SpellCardPower.High;
            }
        }

        private int NumberOfCell(Range row, char column)
        {
            var text = TextOfCell(row, column);
            if (Int32.TryParse(text, out var toReturn))
                return toReturn;
            return 0;
        }

        private string TextOfCell(Range row, char column)
        {
            Range cell = row.Cells[1, column - 'A' + 1];
            return cell.Text.ToString();
        }

        private void SetTextOfCell(Range row, char column, string text)
        {
            Range cell = row.Cells[1, column - 'A' + 1];
            cell[1,1] = text;
        }
    }
}
