﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell {
    public class Card019ZeitverschiebungsLackner : SpellCard{
        public Card019ZeitverschiebungsLackner(int id, string owner) : base(19, id, owner)
        {
            Power = SpellCardPower.High;
        }
        

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            foreach (var activeGameOpponent in activeGame.AllPlayer)
            {
                // dont return cards from this player
                if (activeGameOpponent.Name == forPlayer.Name) continue;

                foreach (var gameCard in activeGameOpponent.CardsPlayedLastRound)
                {
                    if (gameCard is UpgradeCard)
                    {
                        var container = GetContainingRareLackner(gameCard as UpgradeCard);
                        container.Upgrades.Remove(gameCard as UpgradeCard);
                    }
                    if (gameCard is RareLacknerCard)
                    {
                        activeGameOpponent.PlayedRareLackner.Remove(gameCard as RareLacknerCard);
                    }
                    activeGameOpponent.PlayedCards.Remove(gameCard);
                    activeGameOpponent.Cards.Add(gameCard);
                }
            }
            GameLogger.GetInstance().Log($"{Name} hat die Vergangenheit geändert.");
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
