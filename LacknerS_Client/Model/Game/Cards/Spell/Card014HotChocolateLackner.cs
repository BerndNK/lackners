﻿using System;
using System.Collections.Generic;
using System.Linq;
using LacknerS_Client.Core.Logging;
using LacknerS_Client.ViewModel;

namespace LacknerS_Client.Model.Game.Cards.Spell {
    public class Card014HotChocolateLackner : SpellCard{
        public Card014HotChocolateLackner(int id, string owner) : base(14, id, owner)
        {
            Power = SpellCardPower.Middle;
        }

        public override List<SelectionTarget> GetSelectionTargets(ActiveGameViewModel forGame)
        {
            return new List<SelectionTarget> { new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.Player), new SelectionTarget(SelectionPlayedType.PlayedCard, CardType.Upgrade, OwnerType.Enemy) };
        }

        public override void Activate(ActiveGameViewModel activeGame, List<SelectionTarget> selectionTargets, Player forPlayer)
        {
            var upgradeCardA = GetCardById(selectionTargets.First().SelectedCard) as UpgradeCard;
            var upgradeCardB = GetCardById(selectionTargets.Last().SelectedCard) as UpgradeCard;

            if(upgradeCardA == null || upgradeCardB == null) throw new Exception();

            RareLacknerCard rareLacknerA = null;
            RareLacknerCard rareLacknerB = null;

            // get the two rare lackner, which contain the upgrades
            foreach (var player in activeGame.AllPlayer)
            {
                foreach (var playerPlayedCard in player.PlayedCards)
                {
                    var asRareLackner = playerPlayedCard as RareLacknerCard;
                    if (asRareLackner == null) continue;
                    if (asRareLackner.Upgrades.Count(x => x.Id == upgradeCardA.Id) > 0) rareLacknerA = asRareLackner;
                    if (asRareLackner.Upgrades.Count(x => x.Id == upgradeCardB.Id) > 0) rareLacknerB = asRareLackner;
                }
            }

            if(rareLacknerA == null || rareLacknerB == null) throw new Exception();
            var ownerA = GetOwner(upgradeCardA,activeGame);
            var ownerB = GetOwner(upgradeCardB,activeGame);

            ownerA.PlayedCards.Remove(upgradeCardA);
            ownerB.PlayedCards.Remove(upgradeCardB);
            ownerA.PlayedCards.Add(upgradeCardA);
            ownerB.PlayedCards.Add(upgradeCardB);

            rareLacknerA.Upgrades.Remove(upgradeCardA);
            rareLacknerB.Upgrades.Remove(upgradeCardB);

            rareLacknerA.Upgrades.Add(upgradeCardB);
            rareLacknerB.Upgrades.Add(upgradeCardA);

            

            GameLogger.GetInstance().Log($"{Name} hat {upgradeCardA.Name} mit {upgradeCardB.Name} getauscht" );
            base.Activate(activeGame, selectionTargets, forPlayer);
        }
    }
}
