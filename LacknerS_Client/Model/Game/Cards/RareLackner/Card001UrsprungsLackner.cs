﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacknerS_Client.Model.Game.Cards.RareLackner {
    public class Card001UrsprungsLackner : RareLacknerCard {
        public Card001UrsprungsLackner(int id, string owner) : base(1, id, owner)
        {
            Rarity = 10;
            Coolness = 1;
            MaxUpgradeCount = 1;
        }

    }
}
